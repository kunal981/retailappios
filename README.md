# CrownCaliberRetailApp

# ERP External Quote API

The Crown and Caliber ERP system supports an external quote request submission API end-point for use by the website and iOS application. If you

## Technologies
- JSON
- REST
- SSL

## SSL Only
All communication with the backend will take place over SSL with Perfect Forward Secrecy enabled. Your Android or iOS client app should check for revoked SSL certificates and refuse to connect if the cert is invalid or expired.

**The server will not respond to plaintext HTTP communication to the API.** You must check for revoked certificates in order to prevent your app from being susceptible to man in the middle attacks against the security of  and the privacy of our users and requesters.

## Environments

You will need a unique API key to access each environment. The example one used in this document will not work.

### Sandbox Environment

- url: https://crown.sandbox.rietta.com

### Production Environment

- url: https://portal.crownandcaliber.com

## Endpoints

  - New Quote Request: /api/external/quote_requests.json (HTTPS POST)
  - Add Photo to a Quote Request: /api/external/quote_requests/###/attachments (HTTPS POST)

Your API client will need to supply the Quote Request ID for the ### spot in the add photo endpoint. This information will be provided to you in the JSON response body for the New Quote Request submission.

## Supported Fields

Field Name        |  Type   | Description
----------------- | ------- | ---------------------
name              | `String` | **Required.** The customer's full name, single string
phone             | `String` | **Required.** Customer's phone number
email             | `String` | **Required.**  Customer's Email address
brand             | `String` | **Required.** The brand
model_name        | `String` | **Required.** Model number
model_number      | `String` |
documents         | `String` | **Required.** What documents are included
extra_info        | `Text box` |
domestic          | `Boolean` |
no_model_number   | `Boolean` |
retailer          | `String` |
retailer_name     | `String` |
terms             | `Boolean` | **Required.** Must be set to true, this is the customer's electronic agreement to the terms and conditions.
attachments_attributes: [:data] | - | File attachments
originating_ip    | `String` | The original IP address that submitted the quote request.
originating_source  | `String` |  The source, ‘ios’ or ‘web’
originating_user_agent | `String` | The user agent that submitted this to you
originating_campaign  | `String` | The marketing campaign tracking ID that originating this Quote Request.  For example, 'web-form'.

## Submission with values alone as JSON

```
curl -v -H "Accept: application/json" -H "Content-type: application/json" -X POST -d '{"api_token":"fcd1fd644f4542f290893c60bdc85375:586e671e779c89537a2fb0fa584741abe2353f04dd431dc28fbaab094e75211d3ad7345197d6de08cb45d7d34ffb6df02f1efcd02f2bf7ce", "name":"George B. Tester","phone":"770-555-1212","email":"tester@rietta.com","brand":"Rolex","model_name":"Submariner","domestic":true,"retailer":false,"terms":true,"documents":"Box Only","originating_ip":"127.0.0.1","originating_source":"web","originating_user_agent":"Firefox / Test Case"}' https://crown.sandbox.rietta.com/api/external/quote_requests.json
```

On success, it will return an HTTP 201 Created status code and a JSON response with the quote ID and success as true. For example:

```
{
  "id": 26,
  "case_id": 31,
  "attachments_post_url": "https://crown.sandbox.rietta.com/api/external/quote_requests/26/attachments",
  "session_id": "164fb5ee4279ef7e8125b4e88c84b07e1a498d84b4e3362d73a0e8ba3305d93ca34daf0df5cfaf578968ceea7de700db8800fbf152518b9f",
  "success": true
}
```

On error, it will return an HTTP 422 Unprocessable Entity status and a list of error messages from the field validator, like this:

```
{
  "errors": {
    "name": [
      "can't be blank"
    ],
    "email": [
      "can't be blank"
    ],
    "phone": [
      "can't be blank"
    ],
    "brand": [
      "can't be blank"
    ],
    "model_name": [
      "can't be blank"
    ]
  },
  "success": false
}
```

## Submission with File Attachments

The API also accepts file attachments, photos, videos, text documents, etc.
You can submit these through the nested attachments_attributes parameter.  There is no limit to the number of files that you can submit.

```
curl -v -H "Accept: application/json" -X POST \
-F 'api_token=fcd1fd644f4542f290893c60bdc85375:586e671e779c89537a2fb0fa584741abe2353f04dd431dc28fbaab094e75211d3ad7345197d6de08cb45d7d34ffb6df02f1efcd02f2bf7ce;type=text' \
-F 'name=George P. Tester;type=text' \
-F 'phone=770-555-1212;type=text' \
-F 'email=tester@rietta.com;type=text' \
-F 'brand=Rolex;type=text' \
-F 'model_name=Submariner;type=text' \
-F 'model_number=31337;type=text' \
-F 'extra_info=Testing 123, this is a test.;type=text' \
-F 'terms=1;type=text' \
-F 'attachments_attributes[0][data]=@/Users/frank/Desktop/Watches Media/2014-04-11 00.47.29.jpg;type=image/jpg' \
 https://crown.sandbox.rietta.com/api/external/quote_requests.json
```

If successful, it will return an HTTP 201 Created status and a success message, just like the other call:

```
{
  "id": 26,
  "case_id": 31,
  "attachments_post_url": "https://crown.sandbox.rietta.com/api/external/quote_requests/26/attachments",
  "success": true
}
```

To add multiple files, add each as additional array entries, like this:

- -F 'attachments_attributes[1][data]=@/path/to/b.jpg;type=image/jpg'
- -F 'attachments_attributes[2][data]=@/path/to/c.jpg;type=image/jpg'
- -F 'attachments_attributes[3][data]=@/path/to/d.jpg;type=image/jpg'


## Add Files as Subsequent Posts

```
curl -v -H "Accept: application/json" -X POST \
-F 'api_token=fcd1fd644f4542f290893c60bdc85375:586e671e779c89537a2fb0fa584741abe2353f04dd431dc28fbaab094e75211d3ad7345197d6de08cb45d7d34ffb6df02f1efcd02f2bf7ce;type=text' \
-F 'session_id=9154d8e5cb0d6808b3b0296f8671b2522ebd743b23f4e3396018c94587e949f4ede4da6ebec132ef0e9bb0556d97ca82ccd80aaa79bef564;type=text' \
-F 'attachment[data]=@2014-04-11 00.47.38.jpg;type=image/jpg' https://crown.sandbox.rietta.com/api/external/quote_requests/22/attachments
```

Returns:

```
{"id":19,"url":"https://75123f502a606d83cd69-c3949df5626dc7d8a56b170ac96bdd5c.ssl.cf2.rackcdn.com/attachments/data/000/000/019/original/6d42637af550ddd7e2964cf8234c8e0823296bc05b5a80db43d2d94136581a9eb13069d57fdf472651830c3a5d4509c7.jpg","quote_request_id":22,"case_id":22,"success":true}
```

## Retail App API
The retail app uses the same basic API authentication as the quote form submissions.  The basic Structure looks like this.

```
https://portal.crownandcaliber.com/api/external/retail_stores/:retail_store_id.json GET
```

this will return:

```
{
    "options": {
        "decline_reason": [
            "Waiting For Quote",
            "Quote Received",
            "Customer Undecided"
        ],
        "brands": {
            "Brand": [
                "Model A",
                "Model B"
            ]
        },
        "model_location": {
            "Brand: "Model Number Location"
        }
    },
    "assests": {
        "logo": "URL",
        "screensaver": "URL",
        "appsize": "small",
        "quote_form_dl": boolean
        "terms": text
    },
    "reps": []
}
```

```
https://portal.crownandcaliber.com/api/external/retail_stores/:retail_store_id/retail_cases.json GET
```
this will return:

```
{
    "pending_cases": [
        {
          "created_at": "2014-11-14T21:32:21.308Z",
          "image": "URL",
          "name": "Full Name",
          "email": "email@domain.com",
          "phone": "1234567890",
          "id": 1,
          "brand": "Brand",
          "model_name": "Model Name",
          "model_number": "Model Number",
          "status": "Waiting For Quote",
          "timer_text": null
        }
    ],
    "quoted_cases": [
        {
          "created_at": "2014-11-16T16:23:19.113Z",
          "image": "URL",
          "name": "Full Name",
          "email": "email@domain.com",
          "phone": "1234567890",
          "id": 8673,
          "brand": "Brand",
          "model_name": "Model Name",
          "model_number": "Model Number",
          "accepted_at": null,
          "cash_price": 7500,
          "status": "Quote Received"
        }
    ],
    "to_be_shipped_cases": [
      {
        "created_at": "2014-12-01T15:16:52.130Z",
        "image": "URL",
        "name": "Full Name",
        "email": "email@domain.com",
        "phone": "1234567890",
        "id": 9311,
        "brand": "Brand",
        "model_name": "Model Name",
        "model_number": "Model Number",
        "accepted_at": "2014-12-01T20:22:45.423Z",
        "cash_price": 5000,
        "status": "To Be Shipped"
      }
    ]
}
```


```
https://portal.crownandcaliber.com/api/external/retail_stores/:retail_store_id/retail_cases/:retail_case_id.json GET
```
this will return


```
{
  case_id: 10852,
  image: "https://b064f7230b12103ff88d-be9a65929513ed21084ed456c65ec0f4.ssl.cf5.rackcdn.com/attachments/data/000/020/687/original/134b20632d01f1aea766607f5a9eebd02edb129baedba7f5c82ca81e8b0f309ed7c667c686f244372cd7006c93d66d07.jpg",
  seller: "Test Powell",
  phone: "1234567891",
  email: "test@test.com",
  brand: "OMEGA",
  model_name: "Planet Ocean",
  model_number: null,
  offer_price: "0",
  offer_text: "Thank you for your interest. Unfortunately, your watch falls below our minimum purchase value. We appreciate your time and look forward to assisting you in the future.",
  quote_request: 10254,
  notes: "Authentic band/buckle; band length",
  timer_text: null,
  watch_data: [
    {
      created_at: "2014-10-14T21:05:28.250Z",
      newness: "PO",
      auction_at: "08/03/2014",
      price: "2,025",
      bids: "35",
      seller_ranking: 589,
      box_and_papers: "b/o",
      band_type: "LB",
      dial_color: "Black",
      numeral_type: "Stick, arabic",
      bezel_type: "Orange",
      gender: null,
      note: "100% original, very good condtiion; shill at $2230",
      series_year: null,
      auction_url: "http://www.ebay.com/itm/OMEGA-SEAMASTER-WATCH-PLANET-OCEAN-CO-AXIAL-45-5-MM-CASE-STEEL-BLACK-2908-50-91-/181477020674?category=31387&nma=true&si=MJ6%252BzlW%252BWNie7pnWD13fwtgWfoI%253D&orig_cvip=true&rt=nc&_trksid=p2047675.l2557",
      brand: "Omega",
      comp: null,
      caliber: null,
      model_name: "Seamaster Planet Ocean",
      model_number: "2908.50.91",
      quote_at: "10/14/2014",
      quoter: "ehouser@crownandcaliber.com",
      mechanism: "Automatic",
      gold: null,
      case_material: "SS",
      source: "Online",
      updated_at: "2014-10-14T21:10:41.746Z",
      user_id: 26
    }
  ]
}
```
```
https://portal.crownandcaliber.com/api/external/retail_stores/:retail_store_id/retail_cases/:retail_case_id/accept.json PUT
```
you will need to pass in
```
{
  authentication: "token retreived at login"
}
```

```
https://portal.crownandcaliber.com/api/external/retail_stores/:retail_store_id/retail_cases/:retail_case_id/reject.json PUT
```
you will need to pass in
```
{
  authentication: "auth_token retreived at login",
  decline_reason: string
}
```

```
https://portal.crownandcaliber.com/api/external/retail_stores/:retail_store_id/retail_cases/:retail_case_id/ship.json PUT
```
you will need to pass in
```
{
  authentication: "token retreived at login",
  label: string
}
```

```
https://portal.crownandcaliber.com/api/external/sessions.json POST and DELETE
```
you will need to pass in on the POST
```
{"user":
  {
    "email":"email@domail.com",
    "passowrd":"password"
  }
}
```
and will recieve back
```
{
  "success": true,
  "info": "Logged in",
  "data": {
    "auth_token": "GyyWMRgv2HrvvvdDyx4rmd9ewcLzoa-s7g",
    "user_id": 21
  }
}
```

<a name="inbound_token"></a>
# Access API Token (Backend Only)

**This section is only relevant if you are working on the Ruby on Rails backend for the server.**

To access the API, the client must have a valid, unexpired Inbound Access Token.

To generate an Inbound Access Token for an environment, open the Rails console and execute:

```ruby
  ::External::InboundAccessToken.create.generated_token
```

Copy the resulting token, which will look like this:

`1fcd1fd644f4542f290893c60bdc85375:586e671e779c89537a2fb0fa584741abe2353f04dd431dc28fbaab094e75211d3ad7345197d6de08cb45d7d34ffb6df02f1efcd02f2bf7ce`

The generated token is securely handled by the ERP, meaning that it cannot be looked up again. If you loose control of the token, delete it and replace it with a new one.

This enforcement is implemented in `/controllers/api/external/base.rb`.
