//
//  CNCConfig.h
//  Crown&Caliber
//
//  Created by valerio8 on 09.07.14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#ifndef Crown_Caliber_CNCConfig_h
#define Crown_Caliber_CNCConfig_h

#define kCNCBlogURL			@"http://www.crownandcaliber.com/blog/"
#define kCNCPrivacyURL		@"http://www.crownandcaliber.com/privacy-policy/"
#define kCNCTermsURL		@"http://www.crownandcaliber.com/terms-of-service/"


#endif
