//
//  CNCTimerLabel.m
//  Crown&Caliber
//

#import "CNCTimerLabel.h"

@interface CNCTimerLabel()

@property (nonatomic, strong) NSTimer* timer;

@end

@implementation CNCTimerLabel

- (void) dealloc
{
    [_timer invalidate];
    _timer = nil;
}

- (void) setStartDate: (NSDate*) startDate
{
    _startDate = startDate;

    [self updateTimer];
}

- (void) setTimeout: (NSTimeInterval) timeout
{
    _timeout = timeout;

    [self updateTimer];
}

- (void) updateTimer
{
    [self.timer invalidate];

    if ((self.startDate != nil) && (self.timeout > FLT_EPSILON))
    {
        self.timer = [NSTimer scheduledTimerWithTimeInterval: 0.5f
                                                      target: self
                                                    selector: @selector(onUpdate:)
                                                    userInfo: nil
                                                     repeats: YES];
        [self onUpdate: self.timer];
    }
    else
    {
        self.timer = nil;
    }
}

- (void) onUpdate: (NSTimer*) timer
{
    NSTimeInterval currentInterval =
        [[self.startDate dateByAddingTimeInterval: self.timeout] timeIntervalSinceDate: [NSDate date]];
    if ((currentInterval < 0.0f) || (currentInterval > self.timeout))
    {
        currentInterval = 0.0f;
    }

    NSDate* date = [NSDate dateWithTimeIntervalSince1970: currentInterval];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"mm:ss"];
    [dateFormatter setTimeZone: [NSTimeZone timeZoneWithName: @"UTC"]];
    self.text = [dateFormatter stringFromDate: date];
}

@end
