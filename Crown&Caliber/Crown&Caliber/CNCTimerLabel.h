//
//  CNCTimerLabel.h
//  Crown&Caliber
//

#import <UIKit/UIKit.h>

@interface CNCTimerLabel: UILabel

@property (nonatomic, strong) NSDate* startDate;
@property (nonatomic, assign) NSTimeInterval timeout;

@end
