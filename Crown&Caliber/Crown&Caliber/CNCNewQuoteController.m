//
//  CNCNewQuoteController.m
//  Crown&Caliber
//
//  Created by Admin on 10.07.14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCNewQuoteController.h"
#import "CNCQuote.h"
#import "CNCApi.h"
#import "CNCAppOptions.h"
#import "CNCConfig.h"
#import "UIColor+CNCColor.h"
#import "CNCSettings.h"
#import "CNCStringPicker.h"
#import "CNCPaintImageView.h"
#import "CNCWebViewController.h"

#import <BlocksKit/UIActionSheet+BlocksKit.h>
#import <BlocksKit/UIImagePickerController+BlocksKit.h>
#import "CNCTextField.h"
#import <MBProgressHUD.h>
#import <JVFloatLabeledTextView.h>
#import <ActionSheetPicker.h>

#import "CNCAlertController.h"


//#import "NBPhoneMetaDataGenerator.h"
//
#import "NBPhoneNumberUtil.h"
#import "NBPhoneNumber.h"
//
//#import "NBPhoneNumberDesc.h"
//#import "NBNumberFormat.h"




#define ANIMATION_SPEED 0.5

__unused static const float UnderModelNormalOffset = 345;
__unused static const float UnderModelExpandedOffset = 432;

__unused static const float UnderNumberNormalOffset = 96;
__unused static const float UnderNumberExpandedOffset = 146;



@interface CNCNewQuoteController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate, UIGestureRecognizerDelegate, UIActionSheetDelegate, CNCPaintImageViewDelegate>
{
	NSTimer* updateLabelTimer;
	NSDate* submissionDate;
	NSDateFormatter* formatter;

	BOOL brandExpanded;
	BOOL modelExpanded;
    BOOL expandedForManager;
    BOOL quoteDetailsHidden;
    
    BOOL shrinked;
}

@property (weak, nonatomic) IBOutlet UIView* startStepView;

//@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *imageFront;
@property (weak, nonatomic) IBOutlet UIImageView *imageBack;

@property (weak, nonatomic) IBOutlet UIImageView *imageLicense;
@property (weak, nonatomic) IBOutlet UILabel *licenseLabel;
@property (weak, nonatomic) IBOutlet UIImageView* licensePic;
@property (weak, nonatomic) IBOutlet UIView *licenseBg;

@property (weak, nonatomic) IBOutlet UIScrollView* firstStepView;
@property (weak, nonatomic) IBOutlet UIButton* firstStepNextButton;

@property (weak, nonatomic) IBOutlet UIView* underModelPlaceholder;
@property (weak, nonatomic) IBOutlet UIView* underNumberPlaceholder;
@property (weak, nonatomic) IBOutlet UIView* quoteDetailsPlaceholder;
@property (weak, nonatomic) IBOutlet UIView* submitAndFooterPlaceholder;

@property (weak, nonatomic) IBOutlet UIScrollView* secondStepView;
@property (weak, nonatomic) IBOutlet UIView* thirdStepView;

@property (nonatomic, strong) IBOutlet UILabel* footerEmail1Label;
@property (nonatomic, strong) IBOutlet UILabel* footerEmail2Label;



@property (strong, nonatomic) CNCQuote* currentQuote;

@property (weak, nonatomic) IBOutlet CNCTextField *customerName;
@property (weak, nonatomic) IBOutlet CNCTextField *customerEmail;
@property (weak, nonatomic) IBOutlet UILabel* customerEmailLabel;
@property (weak, nonatomic) IBOutlet CNCPhoneTextField *customerPhone;
@property (weak, nonatomic) IBOutlet CNCTextField *watchBrand;
@property (weak, nonatomic) IBOutlet CNCTextField *otherWatchBrand;
@property (weak, nonatomic) IBOutlet CNCTextField *watchModel;
@property (weak, nonatomic) IBOutlet CNCTextField *otherWatchModel;
@property (weak, nonatomic) IBOutlet CNCTextField *watchModelNumber;


//@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentUSLocation;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentDocuments;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentModelFound;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentTradeIn;
@property (weak, nonatomic) IBOutlet UITextView* additionalInfo;

@property (weak, nonatomic) IBOutlet UIButton *brandPickerButton;
@property (weak, nonatomic) IBOutlet UIButton *modelPickerButton;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIView   *submitButtonInactiveView;

@property (weak, nonatomic) IBOutlet UISwitch *expediteSwitch;
@property (weak, nonatomic) IBOutlet UILabel *expediteLabel;
@property (weak, nonatomic) IBOutlet UISwitch *repTradeInSwitch;
@property (weak, nonatomic) IBOutlet UILabel *repTradeInLabel;

@property (weak, nonatomic) IBOutlet UILabel *otherBrandLabel;
@property (weak, nonatomic) IBOutlet UILabel *otherModelLabel;

//@property (weak, nonatomic) IBOutlet UILabel *termsLabel;
@property (weak, nonatomic) IBOutlet UILabel *tradeInLabel;
@property (weak, nonatomic) IBOutlet UILabel* watchModelNumberLabel;
//@property (strong, nonatomic) UITapGestureRecognizer *termsTap;

@property (weak, nonatomic) IBOutlet UILabel* quoteDetailsHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel* associateNameLabel;


//@property (weak, nonatomic) IBOutlet CNCTextField *retailerName;
//@property (weak, nonatomic) IBOutlet UISegmentedControl *referredByRetailerSegmentedControl;

@property (nonatomic) CGRect documentsSegementFrame;
//@property (nonatomic) CGRect sellLabelFrame;
//@property (nonatomic) CGRect sellSegmentFrame;
//@property (nonatomic) CGRect nextButtonFrame;
@property (nonatomic) CGRect submitButtonFrame;


@property (weak, nonatomic) IBOutlet UIButton *step1TermsButton;
@property (weak, nonatomic) IBOutlet UIView *step1TermsView;
@property (weak, nonatomic) IBOutlet UISwitch *step1AgeSwitch;
@property (weak, nonatomic) IBOutlet CNCPaintImageView *step1Signature;



@property (weak, nonatomic) IBOutlet CNCTextField *retailerIdTextField;
@property (weak, nonatomic) IBOutlet CNCTextField *otherAssociate;
@property (weak, nonatomic) IBOutlet UIButton *associateNamePicker;

@property (weak, nonatomic) IBOutlet UILabel *managersSignatureLabel;
@property (weak, nonatomic) IBOutlet CNCTextField *managersSignature;

@property (weak, nonatomic) IBOutlet UILabel* otherAssociateLabel;
@property (weak, nonatomic) IBOutlet UILabel* teamResearchingLabel;
@property (weak, nonatomic) IBOutlet UILabel* timerLabel;

@property (weak, nonatomic) IBOutlet UILabel* helpLabel;

@property (weak, nonatomic) IBOutlet UIButton* viewPendingButton;
@property (weak, nonatomic) IBOutlet UIButton* getAnotherButtonButton;


@property (weak, nonatomic) IBOutlet UILabel* nav1Label;
@property (weak, nonatomic) IBOutlet UILabel* nav2Label;
@property (weak, nonatomic) IBOutlet UILabel* nav3Label;

@property (strong, nonatomic) NSCharacterSet* phoneNumberSymbols;

@end

@implementation CNCNewQuoteController

typedef NS_ENUM(NSUInteger, CNCSecondStepField){
    CNCCustomerName = 1,
    CNCCustomerEmail,
    CNCCustomerPhone,
    CNCWhatchBrand,
    CNCWhatchModel,
    CNCWhatchModelNumber,
    CNCRetailerName,
	CNCStoreName,
	CNCRetailerId,
	CNCOtherAssociateName,
	CNCOtherBrand,
	CNCOtherModel,
    CNCManagersSignature
};


- (void) viewDidLoad
{
    [super viewDidLoad];

    self.phoneNumberSymbols = [NSCharacterSet characterSetWithCharactersInString: @"#*+"];
//    [self.customerPhone.formatter setDefaultOutputPattern: @"########"];


	formatter = [[NSDateFormatter alloc] init];
	formatter.timeStyle = NSDateFormatterShortStyle;

    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString: @"I am over 18 and agree to the terms & conditions"];
    
    [string addAttribute:NSForegroundColorAttributeName value: [UIColor blueColor] range:NSMakeRange(30  ,18)];
    [string  addAttribute:NSUnderlineStyleAttributeName value: [NSNumber numberWithInt: NSUnderlineStyleSingle] range: NSMakeRange(30, 18)];
    
	[self resetUserData];

    self.firstStepView.contentSize = CGSizeMake(CGRectGetWidth(self.firstStepView.bounds), CGRectGetMaxY(self.firstStepNextButton.frame)+40);
    
    [self.step1Signature setBrush: 2.0];
    [self.step1Signature setColor: [UIColor blackColor]];
    [self.step1Signature startDrawing];
    self.step1Signature.delegate = self;
    
    [self.step1Signature startDrawing];
    
	self.secondStepView.contentSize = CGSizeMake(CGRectGetWidth(self.secondStepView.bounds), CGRectGetMaxY(self.underModelPlaceholder.frame));
	self.additionalInfo.layer.borderColor = [UIColor lightGrayColor].CGColor;
	self.additionalInfo.layer.borderWidth = 1.0f;
    
    self.expediteSwitch.onTintColor = self.expediteSwitch.tintColor;
    self.repTradeInSwitch.onTintColor = self.repTradeInSwitch.tintColor;
    
    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(inactiveSubmitAction)];
    [self.submitButtonInactiveView addGestureRecognizer:gesture];
    
}

- (void) viewDidAppear: (BOOL) animated
{
//	NSDictionary* userInfo = [[NSUserDefaults standardUserDefaults] objectForKey: UserInfoKey];
//	if ([userInfo isKindOfClass: [NSDictionary class]])
//	{
//		self.currentQuote.name = _customerName.text =
//		[userInfo[UserNameKey] isKindOfClass: [NSString class]] ? userInfo[UserNameKey] : @"";
//		
//		self.currentQuote.email = _customerEmail.text =
//		[userInfo[UserEmailKey] isKindOfClass: [NSString class]] ? userInfo[UserEmailKey] : @"";
//		
//		self.currentQuote.phone = _customerPhone.text =
//		[userInfo[UserPhoneNumberKey] isKindOfClass: [NSString class]] ? userInfo[UserPhoneNumberKey] : @"";
//		
//	}
	[super viewDidAppear: animated];
//	[self resetUserData];
    
    [self askServerForReceivingTimeout];
}

- (void) viewWillAppear: (BOOL) animated
{
	[super viewWillAppear: animated];
    NSString *phoneMailString;
    if ([CNCAppOptions sharedInstance].appSize == AppSizeRep)
    {
        phoneMailString = @"Phone: (404) 812-9928";
    } else
    {
        phoneMailString = @"Phone: (800) 514-3750       |       Email: bnollner@crownandcaliber.com";
    }
    
    NSMutableAttributedString* phoneMailAttr = [[NSMutableAttributedString alloc] initWithString: phoneMailString];
    [phoneMailAttr addAttribute: NSFontAttributeName
                          value: [UIFont systemFontOfSize: 17.0f]
                          range: NSMakeRange(0, phoneMailString.length)];
    
    NSRange phoneRange = [phoneMailString rangeOfString: @"Phone:"];
    if (phoneRange.location != NSNotFound)
    {
        [phoneMailAttr addAttribute: NSFontAttributeName
                              value: [UIFont boldSystemFontOfSize: 17.0f]
                              range: phoneRange];
    }
    
    NSRange emailRange = [phoneMailString rangeOfString: @"Email:"];
    if (emailRange.location != NSNotFound)
    {
        [phoneMailAttr addAttribute: NSFontAttributeName
                              value: [UIFont boldSystemFontOfSize: 17.0f]
                              range: emailRange];
    }
    
    _footerEmail1Label.attributedText = phoneMailAttr;
    _footerEmail2Label.attributedText = phoneMailAttr;

}



- (void) dealloc
{
    [_step1Signature stopDrawing];
}



- (IBAction) showDialogForChosingSourceForView: (id) sender
{
	NSLog(@"Version %i", __IPHONE_OS_VERSION_MAX_ALLOWED);
	UIImageView* imageView =  (UIImageView*) ((UITapGestureRecognizer*)sender).view;
	[self pickImageForVIew: imageView completion:^(UIImage *resultImage) {
		if (imageView == self.imageFront)
		{
			self.currentQuote.imageFront = resultImage;
		}
		else if (imageView == self.imageBack)
		{
			self.currentQuote.imageBack = resultImage;
		}
        else if (imageView == self.imageLicense)
        {
            self.currentQuote.driverLicenseImage = resultImage;
        }
		
		imageView.image = resultImage;

		self.firstStepNextButton.enabled = (self.imageFront.image != nil &&
											self.imageBack.image != nil);
	}];
}


- (void) pickImage: (void (^)(UIImage *resultImage))success
{
	[self pickImageForVIew: nil completion: success];
}

- (void) pickImageForVIew: (UIImageView*) imaegView
			   completion: (void (^)(UIImage *resultImage))success
{
	if ([UIAlertAction class])
	{
		if (imaegView)
		{
			CNCAlertController *actionSheetController = [CNCAlertController alertControllerWithTitle: nil message: nil preferredStyle: UIAlertControllerStyleActionSheet];
			
			UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
									 {
										 if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
										 {
											 return;
										 }
										 UIImagePickerController* imagePickerController =  [[UIImagePickerController alloc] init];
										 [imagePickerController  setSourceType:UIImagePickerControllerSourceTypeCamera];
										 imagePickerController.delegate = self;
										 
										 [imagePickerController setBk_didFinishPickingMediaBlock:^(UIImagePickerController *picker, NSDictionary *info) {
											 [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
											 UIImage* resultImage = [info objectForKey:UIImagePickerControllerOriginalImage];
											 
											 [picker dismissViewControllerAnimated:YES completion:nil];
											 
											 if (success) {
												 success(resultImage);
											 }
										 }];
										 
										 [self presentViewController:imagePickerController animated:YES completion:NULL];
									 }];
			
			UIAlertAction *library = [UIAlertAction actionWithTitle:@"Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
									  {
										  UIImagePickerController* imagePickerController =  [[UIImagePickerController alloc] init];
										  [imagePickerController  setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
										  
										  imagePickerController.delegate = self;
										  [imagePickerController setBk_didFinishPickingMediaBlock:^(UIImagePickerController *picker, NSDictionary *info) {
											  [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
											  
											  UIImage* resultImage = [info objectForKey:UIImagePickerControllerOriginalImage];
											  [picker dismissViewControllerAnimated:YES completion:nil];
											  
											  if (success) {
												  success(resultImage);
											  }
										  }];
										  
										  [self presentViewController:imagePickerController animated:YES completion:NULL];
									  }];
			
			[actionSheetController addAction: camera];
			[actionSheetController addAction: library];
			
			actionSheetController.view.tintColor = [UIColor blackColor];
			
			[actionSheetController setModalPresentationStyle: UIModalPresentationPopover];
			
			UIPopoverPresentationController *popPresenter = [actionSheetController
															 popoverPresentationController];
			popPresenter.sourceView = imaegView;
			popPresenter.sourceRect = imaegView.bounds;
            [self presentViewController: actionSheetController animated:YES completion: nil];
		}
	}
	else
	{
		UIActionSheet *imagePickAction = [UIActionSheet bk_actionSheetWithTitle:nil];
		
		[imagePickAction bk_addButtonWithTitle:@"Camera" handler:^{
			
			if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
			{
				return;
			}
			
			UIImagePickerController* imagePickerController =  [[UIImagePickerController alloc] init];
			[imagePickerController  setSourceType:UIImagePickerControllerSourceTypeCamera];
			imagePickerController.delegate = self;
			
			[imagePickerController setBk_didFinishPickingMediaBlock:^(UIImagePickerController *picker, NSDictionary *info) {
				[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
				UIImage* resultImage = [info objectForKey:UIImagePickerControllerOriginalImage];
				
				[picker dismissViewControllerAnimated:YES completion:nil];
				
				if (success) {
					success(resultImage);
				}
			}];
			
			[self presentViewController:imagePickerController animated:YES completion:NULL];
		}];
		
		[imagePickAction bk_addButtonWithTitle:@"Library" handler:^{
			UIImagePickerController* imagePickerController =  [[UIImagePickerController alloc] init];
			[imagePickerController  setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
			
			imagePickerController.delegate = self;
			[imagePickerController setBk_didFinishPickingMediaBlock:^(UIImagePickerController *picker, NSDictionary *info) {
				[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
				
				UIImage* resultImage = [info objectForKey:UIImagePickerControllerOriginalImage];
				[picker dismissViewControllerAnimated:YES completion:nil];
				
				if (success) {
					success(resultImage);
				}
			}];
			
			[self presentViewController:imagePickerController animated:YES completion:NULL];
			
		}];
		
		[imagePickAction bk_setCancelButtonWithTitle:@"Cancel" handler:^{
			[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
		}];
		
		[imagePickAction showInView:self.view];
	}
}

- (void) askServerForReceivingTimeout {
    
    
    [[CNCApi sharedClient] pendingWatches: [CNCSettings userId]
                             successBlock: ^(NSArray *quotes, NSError *error, BOOL redirect)
     {
         
         if (redirect)
         {
             [[NSNotificationCenter defaultCenter] postNotificationName:@"requestSignout" object:self];
             return;
         }
         
     }];
}

#pragma mark - CNCPaintImageViewDelegate

- (void) paintImageViewDidChange: (CNCPaintImageView*) paintImageView
{
}

- (void) paintImageViewDidBegin
{
    // prevent scrolling while signing
    self.firstStepView.scrollEnabled = NO;
    [self allowSwipes:NO];
}

- (void) paintImageViewDidEnd
{
    self.currentQuote.signatureImage = [self.step1Signature drawedImage];
    self.firstStepView.scrollEnabled = YES;
    [self allowSwipes:YES];
}

- (IBAction) clearPressed: (UIButton*) sender
{
    [self.step1Signature resetImage];
}


- (IBAction) termsAndConditionsPressed: (UIButton*) sender
{
    [self presentWebVCWithHtml: [CNCAppOptions sharedInstance].terms];
}

- (IBAction) ageSwitchChanged: (id) sender
{
    // the age switch is a placebo, it does nothing since it is optional
}

- (IBAction) expediteSwitchChanged: (UISwitch *) sender
{
    if ([sender isOn]) {
        self.currentQuote.isExpedited = YES;
    } else {
        self.currentQuote.isExpedited = NO;
    }
}

- (IBAction) repTradeInSwitchChanged: (UISwitch *) sender
{
    if ([sender isOn]) {
        self.currentQuote.isTradeIn = YES;
        self.expediteSwitch.hidden = NO;   // show the expedite switch
        self.expediteLabel.hidden = NO;

    } else {
        self.currentQuote.isTradeIn = NO;
        self.expediteSwitch.hidden = YES;  // hide the expedite switch and reset value
        self.expediteLabel.hidden = YES;
        self.currentQuote.isExpedited = NO;
    }
}

- (void) presentWebVCWithHtml: (NSString*) html
{
    UINavigationController* webNavigationController = [self.storyboard instantiateViewControllerWithIdentifier: @"webNavigationController"];
    CNCWebViewController* webViewController = (CNCWebViewController*) webNavigationController.topViewController;
    [webViewController loadPageFromHtml: html];
    
    UIBarButtonItem* doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemDone
                                                                              target: self
                                                                              action: @selector(donePressed)];
    webViewController.navigationItem.rightBarButtonItem = doneItem;
    [self presentViewController: webNavigationController animated: YES completion: nil];
    
}

- (void) donePressed
{
    [self dismissViewControllerAnimated: YES completion: nil];
}



#pragma mark UIActionSheetDelegate protocol

- (void) actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
	
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (IBAction)modelFoundAction:(UISegmentedControl *)sender
{
    [self.view endEditing:YES];
    
    self.currentQuote.hasModelNumber = sender.selectedSegmentIndex == 0;
    
    if (sender.selectedSegmentIndex == 0) {
        self.watchModel.returnKeyType = UIReturnKeyNext;

        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            self.watchModelNumber.hidden = NO;
			self.watchModelNumberLabel.hidden = NO;
        }];

    }
    else {
		self.watchModel.returnKeyType = UIReturnKeyDone;

        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            self.watchModelNumber.hidden = YES;
			self.watchModelNumberLabel.hidden = YES;
        }];
    }
    [self checkSubmitButton:self.currentQuote];

}


-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    switch (textField.tag) {
        case CNCCustomerName:
            [self.customerEmail becomeFirstResponder];
            break;
        case CNCCustomerEmail:
            [self.customerPhone becomeFirstResponder];
            break;
        case CNCCustomerPhone:
            [self showBrandPicker: self.brandPickerButton];
            break;
        case CNCWhatchModel:
            if (self.segmentModelFound.selectedSegmentIndex == 0) {
                [self.watchModelNumber becomeFirstResponder];
            }
            else {
                [self.view endEditing:YES];
            }
            break;
        case CNCWhatchModelNumber:
            [self.view endEditing:YES];
            
            break;
        case CNCRetailerName:
            [self.view endEditing:YES];
            break;
			
		case CNCRetailerId:
		case CNCOtherAssociateName:
		case CNCOtherBrand:
		case CNCOtherModel:
        case CNCManagersSignature:
			[self.view endEditing:YES];
			break;
            break;
        default:
            break;
    }
    
    return YES;
}

- (IBAction) textChanged: (UITextField*)sender
{
	switch (sender.tag)
	{
	case CNCCustomerName:
	  self.currentQuote.name = sender.text;
	  break;
	case CNCCustomerEmail:
	  self.currentQuote.email = sender.text;
	  break;
	case CNCCustomerPhone:
      NSLog(@"CNCCustomerPhone %@", sender.text);
	  self.currentQuote.phone = sender.text;
	  break;
	case CNCWhatchBrand:
	case CNCOtherBrand:
	  self.currentQuote.brand = sender.text;
	  break;
	case CNCWhatchModel:
	case CNCOtherModel:
	  self.currentQuote.modelName = sender.text;
	  break;
	case CNCWhatchModelNumber:
	  self.currentQuote.modelNumber = sender.text;
	  break;
	case CNCRetailerName:
	  self.currentQuote.retailerName = sender.text;
	  break;
	case CNCStoreName:
	  self.currentQuote.storeName = sender.text;
	  break;
//	case CNCRetailerId:
	case CNCOtherAssociateName:
	  self.currentQuote.retailerId = sender.text;
	  break;
    case CNCManagersSignature:
      // there is no data field to set, this case here for consistency
      break;

	default:
	  break;
  }
  
  [self checkSubmitButton:self.currentQuote];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	[self textChanged: textField];
	return YES;
}

- (IBAction)textFieldDidEndEditing: (JVFloatLabeledTextField *)sender
{
    switch (sender.tag) {
        case CNCCustomerName:
            sender.layer.borderColor = [self validateName:sender.text]? [UIColor CNCGrayColor].CGColor : [UIColor redColor].CGColor;
            break;
        case CNCCustomerEmail:
            sender.layer.borderColor = [self validateEmail:sender.text]? [UIColor CNCGrayColor].CGColor : [UIColor redColor].CGColor;
            break;
//        case CNCCustomerPhone:
//            sender.layer.borderColor = [self validatePhone:sender.text]? [UIColor CNCGrayColor].CGColor : [UIColor redColor].CGColor;
//            break;
        case CNCWhatchModel:
            sender.layer.borderColor = [self validateModelName:sender.text]? [UIColor CNCGrayColor].CGColor : [UIColor redColor].CGColor;
            break;
        case CNCWhatchModelNumber:
            sender.layer.borderColor = [self validateModelNumber:sender.text]? [UIColor CNCGrayColor].CGColor : [UIColor redColor].CGColor;
            break;
		case CNCRetailerId:
			sender.layer.borderColor = [self validateModelName:sender.text]? [UIColor CNCGrayColor].CGColor : [UIColor redColor].CGColor;
			break;
        case CNCManagersSignature:
            sender.layer.borderColor = [self validateManagersSignature:sender.text]? [UIColor CNCGrayColor].CGColor : [UIColor redColor].CGColor;
            break;
        default:
            break;
    }
    
}


#pragma mark - Internal logic

- (IBAction) startQuoting: (id)sender
{
	[UIView animateWithDuration: 0.5 animations:^{
		self.startStepView.alpha = 0.0f;
	}];
}

- (IBAction) startSecondStep: (id)sender
{
	self.secondStepView.hidden = NO;
	[UIView animateWithDuration: 0.5 animations:^{
		[self.nav2Label setBackgroundColor: [UIColor CNCOrangeColor]];
		self.secondStepView.alpha = 1.0;
	}];
}


- (void) checkSubmitButton: (CNCQuote*) quote
{
    // note, any checks done here should also be reflected in the "inactiveSubmitAction" method.
    BOOL condition =
#ifdef Retail
	(self.retailerIdTextField.text &&
	 [self validateName: quote.name] &&
	 [self validateEmail: quote.email] &&
     (self.segmentDocuments.selectedSegmentIndex > -1) &&
	 quote.brand.length > 0 &&
	 [self validateModelName: quote.modelName]  &&
     ([CNCAppOptions sharedInstance].managersApproval? [self validateManagersSignature: self.managersSignature.text]:YES) &&
	 (quote.hasModelNumber? [self validateModelNumber: quote.modelNumber] : YES));
#else
	self.agreeSwitch.isOn &&
	((self.referredByRetailerSegmentedControl.selectedSegmentIndex == 0)? (quote.retailerName.length > 0):YES) &&
	([self validateName: quote.name] && [self validatePhone: quote.phone] &&
	 [self validateEmail: quote.email] && quote.brand.length > 0 &&
	 [self validateModelName: quote.modelName]  &&
	 (quote.hasModelNumber? [self validateModelNumber: quote.modelNumber] : YES));
#endif
	
    if (condition)
	{
        self.submitButton.enabled = YES;
    }
    else
    {
        self.submitButton.enabled = NO;
    }

}

- (IBAction)segmentDocumentsAcion:(UISegmentedControl *)sender
{
	NSString* documentsParam = nil;
	switch (sender.selectedSegmentIndex)
	{
		case 0:
			documentsParam = @"Box Only";
			break;
		case 1:
			documentsParam = @"Papers Only";
			break;
		case 2:
			documentsParam = @"Box & Papers";
			break;
		case 3:
			documentsParam = @"Neither";
			break;

		default:
			break;
	}
    self.currentQuote.documents = documentsParam;
    [self checkSubmitButton:self.currentQuote];
}

- (IBAction) segmentTradeInAcion:(UISegmentedControl*) sender
{
	if (sender.selectedSegmentIndex == 0)
	{
		self.currentQuote.isTradeIn = YES;
	}
	else
	{
		self.currentQuote.isTradeIn = NO;
	}
}


- (IBAction) nextAction: (id) sender
{
    [self.view endEditing:YES];
}


- (void)inactiveSubmitAction
{
    // this is a view containing the submit button on the second screen
    // when the button is inactive, the client wants the user to be able to select the 'disabled button'.
    // This view will catch the tap gesture and display a set of reasons why button still inactive

#ifdef Retail
    NSMutableString *message = [NSMutableString new];
    if (!self.retailerIdTextField.text) {
        [message appendString:@"Must have retailer id.\n"];
    }
    if (![self validateName: self.currentQuote.name]) {
        [message appendString:@"Must have valid Full Name.\n"];
    }
    if (![self validateEmail: self.currentQuote.email]) {
        [message appendString:@"Must have valid Email Address.\n"];
    }
    if (!(self.segmentDocuments.selectedSegmentIndex > -1)) {
        [message appendString:@"Must have answered 'What will be included with watch?'\n"];
    }
    if (!(self.currentQuote.brand.length > 0)) {
        [message appendString:@"Must have selected a Watch Brand.\n"];
    }
    if (![self validateModelName: self.currentQuote.modelName]) {
        [message appendString:@"Must have valid Watch Model.\n"];
    }
    if ([CNCAppOptions sharedInstance].managersApproval && ![self validateManagersSignature: self.managersSignature.text]) {
        [message appendString:@"Must have Managers Signature.\n"];
    }
    if (self.currentQuote.hasModelNumber && ![self validateModelNumber: self.currentQuote.modelNumber] ) {
        [message appendString:@"Must have valid Model Number, or mark as Not Found.\n"];
    }
    
    [self displayAlert:message withTitle:@"Error"];

#endif
    
    
}


- (IBAction) submitAction:(UIButton *)sender
{
	[self.secondStepView endEditing: YES];

    
    self.timerLabel.hidden = !self.currentQuote.isTradeIn;
    self.viewPendingButton.hidden = !self.currentQuote.isTradeIn;
    self.getAnotherButtonButton.hidden = self.currentQuote.isTradeIn;

    [MBProgressHUD showHUDAddedTo: self.parentViewController.view animated: YES];

    [[CNCApi sharedClient] createQuote: self.currentQuote withSuccess: ^(CNCQuote *quote, NSString* submissionText, BOOL retailTimer, NSError *error, BOOL redirect, NSString* message) {
        [MBProgressHUD hideAllHUDsForView: self.parentViewController.view animated:YES];
      
        if (redirect)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"requestSignout" object:self];
            return;
        }

        if (message)
        {
            [self displayAlert:message withTitle:@"Error"];
            return;
        } else if (error)
        {
            if ([self.menuDelegate respondsToSelector: @selector(didSelectFailItemWithQuote:)])
            {
                [self.menuDelegate didSelectFailItemWithQuote: self.currentQuote];
            } else {
                [self displayAlert:[error localizedDescription] withTitle:@"Error"];
            }
            return;
        }
        
        self.teamResearchingLabel.text = submissionText;
        if (!retailTimer) { // if after hours then no showing of the timer
            self.timerLabel.hidden = YES;
        }
		self.thirdStepView.hidden = NO;
		[UIView animateWithDuration: 0.5 animations:^{
			[self.nav3Label setBackgroundColor: [UIColor CNCOrangeColor]];
			self.thirdStepView.alpha = 1.0;
		}];

		submissionDate = [NSDate date];
		if (!updateLabelTimer)
		{
			updateLabelTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
																target: self
															  selector: @selector(updateTimer:)
															  userInfo: nil
															   repeats: YES];
		}

    }];
}

- (void)allowSwipes:(BOOL)allow {
    if ([self.menuDelegate respondsToSelector: @selector(allowSwipes:)])
    {
        [self.menuDelegate allowSwipes:allow];
    }
}

- (void) updateTimer: (NSTimer*) timer
{
	NSTimeInterval interval = 20 * 60 + [submissionDate timeIntervalSinceNow];
	int minutes = ((int)interval) / 60;
	int seconds = ((int)interval) % 60;
	self.timerLabel.text = [NSString stringWithFormat: @"%02d:%02d", minutes, seconds];
}


- (void)textViewDidChange:(UITextView *)textView
{
    self.currentQuote.extraInfo = textView.text;
}


- (IBAction)  showAssociatePicker: (UIButton*) sender
{
	NSArray* reps = [CNCAppOptions sharedInstance].reps;

	NSMutableArray* associates = [NSMutableArray arrayWithCapacity: reps.count + 1];
	for (NSDictionary* rep in reps)
	{
		[associates addObject: rep[@"full_name"]];
	}

	[associates addObject: @"Other"];


	CNCStringPicker* picker = [[CNCStringPicker alloc] initWithTitle: nil
                                                                rows: associates
                                                    initialSelection: 0
                                                           doneBlock: ^(CNCStringPicker *picker, NSInteger selectedIndex, id selectedValue) {

                                                               self.retailerIdTextField.text = associates[selectedIndex];

                                                               if ([self.retailerIdTextField.text isEqualToString: @"Other"])
                                                               {
                                                                   self.otherAssociate.hidden = NO;
                                                                   self.otherAssociateLabel.hidden = NO;
                                                               }
                                                               else
                                                               {
                                                                   NSDictionary* rep = reps[selectedIndex];
                                                                   self.currentQuote.salesAssociateId = rep[@"id"];
                                                               }
                                                               [self checkSubmitButton:self.currentQuote];
                                                           }
                                                         cancelBlock:^(CNCStringPicker *picker) {
                                                           }
                                                              origin: sender];
	[picker showActionSheetPicker];
}

- (void) updateScrollSize
{
	CGRect underModelFrame = self.underModelPlaceholder.frame;
	underModelFrame.size.height = CGRectGetMaxY(self.underNumberPlaceholder.frame);
	self.underModelPlaceholder.frame = underModelFrame;

	self.secondStepView.contentSize = CGSizeMake(CGRectGetWidth(self.secondStepView.bounds), CGRectGetMaxY(self.underModelPlaceholder.frame));
}

- (IBAction) showBrandPicker: (UIButton*) sender
{
    [self.view endEditing:YES];

	NSArray* watchBrands = [[CNCAppOptions sharedInstance].brands allKeys];
	watchBrands = [watchBrands sortedArrayUsingComparator: ^NSComparisonResult(NSString* obj1, NSString* obj2) {
		return [obj1 compare: obj2];
	}];


	if (!watchBrands)
		return;

	NSMutableArray* mutableWatchBrands = [NSMutableArray arrayWithArray: watchBrands];
	[mutableWatchBrands addObject: @"Other"];

    CNCStringPicker* picker = [[CNCStringPicker alloc] initWithTitle: nil
                                                                rows: mutableWatchBrands
                                                    initialSelection: 0
                                                           doneBlock: ^(CNCStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                                               self.currentQuote.brand = mutableWatchBrands[selectedIndex];
                                                               self.watchBrand.text = self.currentQuote.brand;
                                                               self.modelPickerButton.enabled = YES;
                                                               
															   // help label
															   NSString* helpLabelText = [CNCAppOptions sharedInstance].modelsDescription[self.currentQuote.brand];
															   float newUnderNumberOffset = (helpLabelText == nil) ? (CGRectGetMinY(self.helpLabel.frame) + 10.0f) : (CGRectGetMaxY(self.helpLabel.frame));
															   self.helpLabel.text = helpLabelText;

															   CGRect underNumberFrame = self.underNumberPlaceholder.frame;
															   underNumberFrame.origin.y = newUnderNumberOffset;

															   [UIView animateWithDuration: 0.5 animations:^{
																   self.underNumberPlaceholder.frame = underNumberFrame;
																   [self updateScrollSize];
															   }];


                                                               if  ([CNCAppOptions sharedInstance].appSize == AppSizeRep) {
                                                                   if ([self.currentQuote.brand isEqualToString:@"Jewelry"]) {
                                                                       // if jewelry is selected, reset the rep trade in and expedite values in case they are set.  Also hide the switches
                                                                       self.repTradeInSwitch.hidden = YES;
                                                                       self.repTradeInLabel.hidden = YES;
                                                                       [self.repTradeInSwitch setOn:NO];
                                                                       self.currentQuote.isTradeIn = NO;
                                                                       
                                                                       self.expediteSwitch.hidden = YES;
                                                                       self.expediteLabel.hidden = YES;
                                                                       [self.expediteSwitch setOn:NO];
                                                                       self.currentQuote.isExpedited = NO;
                                                                       
                                                                   } else {
                                                                       self.repTradeInSwitch.hidden = NO;
                                                                       self.repTradeInLabel.hidden = NO;
                                                                       // don't mess around with values, let the user do it if needed
                                                                   }
                                                               }


                                                               if ([self.currentQuote.brand isEqualToString: @"Other"])
                                                               {
                                                                   brandExpanded = YES;

                                                                   self.otherWatchBrand.hidden = NO;
                                                                   self.otherWatchBrand.userInteractionEnabled = YES;
                                                                   self.otherBrandLabel.hidden = NO;

                                                                   CGRect underModelFrame = self.underModelPlaceholder.frame;
                                                                   underModelFrame.origin.y = CGRectGetMaxY(self.otherWatchBrand.frame);

                                                                   [UIView animateWithDuration: 0.5 animations:^{
                                                                       self.underModelPlaceholder.frame = underModelFrame;
                                                                       [self updateScrollSize];
                                                                   }];

                                                                   [self.otherWatchBrand becomeFirstResponder];
                                                               }
                                                               else
                                                               {
                                                                   brandExpanded = NO;
                                                                   self.otherWatchBrand.hidden = YES;
                                                                   self.otherBrandLabel.hidden = YES;

                                                                   if (!modelExpanded)
                                                                   {
                                                                       CGRect underModelFrame = self.underModelPlaceholder.frame;
                                                                       underModelFrame.origin.y = CGRectGetMaxY(self.watchBrand.frame) + 8.0f;

                                                                       [UIView animateWithDuration: 0.5 animations:^{
                                                                           self.underModelPlaceholder.frame = underModelFrame;
                                                                           [self updateScrollSize];
                                                                       }];
                                                                   }
                                                                   [self showModelPicker: self.modelPickerButton];
                                                               }

                                                               [self checkSubmitButton:self.currentQuote];

    } cancelBlock:^(CNCStringPicker *picker) {
        //
    } origin: sender];
    
    [picker showActionSheetPicker];
}

- (IBAction) showModelPicker: (UIButton*) sender
{
	[self.view endEditing:YES];

	NSArray* watchModels = [CNCAppOptions sharedInstance].brands[self.currentQuote.brand];
	watchModels = [watchModels sortedArrayUsingComparator: ^NSComparisonResult(NSString* obj1, NSString* obj2) {
		return [obj1 compare: obj2];
	}];

//
//	if (!watchModels)
//		return;

	NSMutableArray* mutableWatchModels = [NSMutableArray arrayWithArray: watchModels];
	[mutableWatchModels addObject: @"Other"];


	CNCStringPicker* picker = [[CNCStringPicker alloc] initWithTitle: nil
                                                                rows: mutableWatchModels
                                                    initialSelection: 0
                                                           doneBlock: ^(CNCStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                                               self.currentQuote.modelName = mutableWatchModels[selectedIndex];
                                                               self.watchModel.text = self.currentQuote.modelName;


                                                               if ([self.currentQuote.modelName isEqualToString: @"Other"])
                                                               {
                                                                   modelExpanded = YES;

                                                                   self.otherWatchModel.hidden = NO;
                                                                   self.otherWatchModel.userInteractionEnabled = YES;
                                                                   self.otherModelLabel.hidden = NO;

                                                                   CGRect underModelFrame = self.underModelPlaceholder.frame;
                                                                   underModelFrame.origin.y = CGRectGetMaxY(self.otherWatchBrand.frame);

                                                                   [UIView animateWithDuration: 0.5 animations:^{
                                                                       self.underModelPlaceholder.frame = underModelFrame;
                                                                       [self updateScrollSize];
                                                                   }];

                                                                   [self.otherWatchModel becomeFirstResponder];
                                                               }
                                                               else
                                                               {
                                                                   modelExpanded = NO;
                                                                   self.otherWatchModel.hidden = YES;
                                                                   self.otherModelLabel.hidden = YES;

                                                                   if (!brandExpanded)
                                                                   {
                                                                       CGRect underModelFrame = self.underModelPlaceholder.frame;
                                                                       underModelFrame.origin.y = CGRectGetMaxY(self.watchBrand.frame) + 8.0f;

                                                                       [UIView animateWithDuration: 0.5 animations:^{
                                                                           self.underModelPlaceholder.frame = underModelFrame;
                                                                           [self updateScrollSize];
                                                                       }];
                                                                   }
                                                                   if (!self.watchModelNumber.hidden)
                                                                       [self.watchModelNumber becomeFirstResponder];
                                                               }

                                                               [self checkSubmitButton:self.currentQuote];

                                                           } cancelBlock:^(CNCStringPicker *picker) {
                                                               //
                                                           } origin: sender];

	[picker showActionSheetPicker];
}



-(BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];    //  return 0;
    return  ([emailTest evaluateWithObject:candidate] && candidate.length>3 && candidate.length<255);
}

#define PHONEACCEPTEDSYMBOLS @"1234567890+-()"
- (BOOL) validatePhone:(NSString*) phone
{
    NSCharacterSet *unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:PHONEACCEPTEDSYMBOLS] invertedSet];
    
    NSRange range = [phone rangeOfCharacterFromSet:unacceptedInput];
    
    return (phone.length >= 10 && phone.length <= 20 && range.location == NSNotFound);
}

- (BOOL) validateName:(NSString*) name
{
    return (name.length>0&& name.length<256);
}

- (BOOL) validateModelNumber:(NSString*) modelNumber
{
    return (modelNumber.length>2 && modelNumber.length<31);
}

- (BOOL) validateModelName:(NSString*) modelName
{
    return YES;
}

- (BOOL) validateManagersSignature:(NSString*) signature
{
    NSString *trimmedString = [signature stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    return (trimmedString.length>0);
}

- (void) resetUserData
{
    
	self.secondStepView.contentOffset = CGPointZero;

	self.otherWatchBrand.hidden = YES;
	self.otherWatchModel.hidden = YES;
	self.otherBrandLabel.hidden = YES;
	self.otherModelLabel.hidden = YES;

	self.otherAssociate.hidden = YES;
	self.otherAssociateLabel.hidden = YES;
    
    self.otherWatchBrand.text = @"";
    self.otherWatchModel.text = @"";
    self.otherAssociate.text = @"";
    self.managersSignature.text = @"";

	self.imageLicense.hidden = YES;
	self.licensePic.hidden = YES;
	self.licenseLabel.hidden = YES;
    self.licenseBg.hidden = YES;

	self.helpLabel.text = @"";
    
    [self.step1Signature resetImage];
    
    self.firstStepView.contentOffset = CGPointZero;
    if  ([CNCAppOptions sharedInstance].appSize == AppSizeRep)
    {
        self.step1TermsView.hidden = NO;
        self.imageLicense.hidden = NO;
        self.licensePic.hidden = NO;
        self.licenseLabel.hidden = NO;
        self.licenseBg.hidden = NO;
        
        CGRect firstStepButtonRect = self.firstStepNextButton.frame;
        firstStepButtonRect.origin.y = CGRectGetMaxY(self.step1TermsView.frame)+40;
        self.firstStepNextButton.frame = firstStepButtonRect;
        
        self.firstStepView.contentSize = CGSizeMake(CGRectGetWidth(self.firstStepView.bounds), CGRectGetMaxY(self.firstStepNextButton.frame)+30);
    } else
    {
        self.step1TermsView.hidden = YES;
        self.imageLicense.hidden = YES;
        self.licensePic.hidden = YES;
        self.licenseLabel.hidden = YES;
        self.licenseBg.hidden = YES;
        
        CGRect firstStepButtonRect = self.firstStepNextButton.frame;
        firstStepButtonRect.origin.y = CGRectGetMinY(self.step1TermsView.frame);
        self.firstStepNextButton.frame = firstStepButtonRect;
        
        self.firstStepView.contentSize = CGSizeMake(CGRectGetWidth(self.firstStepView.bounds), CGRectGetMaxY(self.firstStepNextButton.frame)+30 );
    }
    

    

    
    const CGFloat emailHeight = CGRectGetMaxY(self.customerEmail.frame) - CGRectGetMinY(self.customerEmailLabel.frame);
    const CGFloat tradeInHeight = CGRectGetMaxY(self.segmentTradeIn.frame) - CGRectGetMinY(self.tradeInLabel.frame);
    
    if  ([CNCAppOptions sharedInstance].appSize == AppSizeSmall)
    {
        if ((self.secondStepView != nil) && !shrinked)
        {
            shrinked = YES;

            for (UIView* subview in self.secondStepView.subviews)
            {
                if (CGRectGetMinY(subview.frame) > CGRectGetMaxY(self.customerEmail.frame))
                {
                    CGRect rect = subview.frame;
                    rect.origin.y -= emailHeight;
                    subview.frame = rect;
                }
            }
            
            for (UIView* subview in self.quoteDetailsPlaceholder.subviews)
            {
                if (CGRectGetMinY(subview.frame) > CGRectGetMaxY(self.segmentTradeIn.frame))
                {
                    CGRect rect = subview.frame;
                    rect.origin.y -= tradeInHeight;
                    subview.frame = rect;
                }
            }
            
            CGRect quoteDetailsPlaceholderRect = self.quoteDetailsPlaceholder.frame;
            quoteDetailsPlaceholderRect.size.height -= tradeInHeight;
            self.quoteDetailsPlaceholder.frame = quoteDetailsPlaceholderRect;

            CGRect underNumberPlaceholderRect = self.underNumberPlaceholder.frame;
            underNumberPlaceholderRect.size.height -= tradeInHeight;
            self.underNumberPlaceholder.frame = underNumberPlaceholderRect;
            
            CGRect underModelPlaceholderRect = self.underModelPlaceholder.frame;
            quoteDetailsPlaceholderRect.size.height -= tradeInHeight;
            underModelPlaceholderRect.size.height -= emailHeight;
            self.underModelPlaceholder.frame = underModelPlaceholderRect;
        }
    }
    else if (([CNCAppOptions sharedInstance].appSize == AppSizeBig) || ([CNCAppOptions sharedInstance].appSize == AppSizeRep))
    {
        if ((self.secondStepView != nil) && shrinked)
        {
            shrinked = NO;
            
            for (UIView* subview in self.secondStepView.subviews)
            {
                if (CGRectGetMinY(subview.frame) > CGRectGetMinY(self.customerEmail.frame))
                {
                    CGRect rect = subview.frame;
                    rect.origin.y += emailHeight;
                    subview.frame = rect;
                }
            }
            
            for (UIView* subview in self.quoteDetailsPlaceholder.subviews)
            {
                if (CGRectGetMinY(subview.frame) > CGRectGetMinY(self.segmentTradeIn.frame))
                {
                    CGRect rect = subview.frame;
                    rect.origin.y += tradeInHeight;
                    subview.frame = rect;
                }
            }
            
            CGRect quoteDetailsPlaceholderRect = self.quoteDetailsPlaceholder.frame;
            quoteDetailsPlaceholderRect.size.height += tradeInHeight;
            self.quoteDetailsPlaceholder.frame = quoteDetailsPlaceholderRect;
            
            CGRect underNumberPlaceholderRect = self.underNumberPlaceholder.frame;
            underNumberPlaceholderRect.size.height += tradeInHeight;
            self.underNumberPlaceholder.frame = underNumberPlaceholderRect;
            
            CGRect underModelPlaceholderRect = self.underModelPlaceholder.frame;
            underModelPlaceholderRect.size.height += emailHeight;
            quoteDetailsPlaceholderRect.size.height += tradeInHeight;
            self.underModelPlaceholder.frame = underModelPlaceholderRect;
        }
    }
    
    [self adjustForQuoteDetails];

    [self adjustForManagersApproval];
    
    
    
    self.secondStepView.contentSize = CGSizeMake(CGRectGetWidth(self.secondStepView.bounds), CGRectGetMaxY(self.underModelPlaceholder.frame));

    if ([CNCAppOptions sharedInstance].showDLOnQuote && !([CNCAppOptions sharedInstance].appSize == AppSizeSmall))
    {
        self.imageLicense.hidden = NO;
        self.licensePic.hidden = NO;
        self.licenseLabel.hidden = NO;
        self.licenseBg.hidden = NO;
    }
    
    
    [self.expediteSwitch setOn:NO];
    self.expediteSwitch.hidden = YES;
    self.expediteLabel.hidden = YES;
    [self.repTradeInSwitch setOn:NO];
    self.repTradeInSwitch.hidden = YES;
    self.repTradeInLabel.hidden = YES;


    if ([CNCAppOptions sharedInstance].appSize == AppSizeSmall) {
        self.customerEmail.hidden = YES;
        self.customerEmailLabel.hidden = YES;
        self.tradeInLabel.hidden = YES;
        self.segmentTradeIn.hidden = YES;
    } else if ([CNCAppOptions sharedInstance].appSize == AppSizeBig) {
        self.customerEmail.hidden = NO;
        self.customerEmailLabel.hidden = NO;
        self.tradeInLabel.hidden = NO;
        self.segmentTradeIn.hidden = NO;
    } else if ([CNCAppOptions sharedInstance].appSize == AppSizeRep) {
        self.customerEmail.hidden = NO;
        self.customerEmailLabel.hidden = NO;
        self.expediteSwitch.hidden = YES;
        self.expediteLabel.hidden = YES;
        self.repTradeInSwitch.hidden = NO;
        self.repTradeInLabel.hidden = NO;
    }

	[self.nav1Label setBackgroundColor: [UIColor CNCOrangeColor]];
	[self.nav2Label setBackgroundColor: [UIColor colorWithRed: 0.35 green: 0.15 blue: 0.00 alpha: 1.0]];
	[self.nav3Label setBackgroundColor: [UIColor colorWithRed: 0.35 green: 0.15 blue: 0.00 alpha: 1.0]];
	self.nav1Label.layer.cornerRadius = 25;
	self.nav2Label.layer.cornerRadius = 25;
	self.nav3Label.layer.cornerRadius = 25;
	self.nav1Label.clipsToBounds = YES;
	self.nav2Label.clipsToBounds = YES;
	self.nav3Label.clipsToBounds = YES;

	_customerEmail.text = @"";
	self.currentQuote.name = _customerName.text =
	self.currentQuote.phone = _customerPhone.text =
	self.currentQuote.retailerId = _retailerIdTextField.text = @"";
	
	self.imageFront.image = nil;
	self.imageBack.image = nil;
	self.imageLicense.image = nil;
    self.firstStepNextButton.enabled = NO;
    
    self.customerEmail.text = nil;
    self.customerName.text = nil;
    self.customerPhone.text = nil;
    self.watchBrand.text = nil;
    self.watchModel.text = nil;
    self.watchModelNumber.text = nil;

	self.modelPickerButton.enabled = YES;
	self.modelPickerButton.userInteractionEnabled = YES;

    self.additionalInfo.text = nil;

    self.submitButton.enabled = NO;
    
    self.segmentDocuments.selectedSegmentIndex = UISegmentedControlNoSegment;

	self.startStepView.alpha = 1.0;
	self.firstStepView.alpha = 1.0;
	self.secondStepView.alpha = 0.0;
	self.secondStepView.hidden = YES;
	self.thirdStepView.alpha = 0.0;
	self.thirdStepView.hidden = YES;

    for (CNCTextField *textField in self.view.subviews) {
        textField.layer.borderColor = [UIColor CNCGrayColor].CGColor;
    }
    
    if (self.segmentModelFound.selectedSegmentIndex == 1) {
        self.segmentModelFound.selectedSegmentIndex = 0;
        [self modelFoundAction:self.segmentModelFound];
    }
    
    
    if (self.currentQuote) {
        self.currentQuote = nil;
    }
    self.currentQuote = [[CNCQuote alloc] init];
//    self.currentQuote.documents = [self.segmentDocuments titleForSegmentAtIndex:0];
    self.currentQuote.extraInfo = @"";
    
    self.currentQuote.isFullQuote = YES;
    self.currentQuote.isDomestic = YES;
    self.currentQuote.hasModelNumber = YES;
    self.currentQuote.hasRetailer = YES;
    self.currentQuote.retailerName = nil;
    if ([CNCAppOptions sharedInstance].appSize == AppSizeRep)
    {
        self.currentQuote.isTradeIn = NO;
    }
    else
    {
        self.currentQuote.isTradeIn = YES;
        self.segmentTradeIn.selectedSegmentIndex = 0;
    }
        
	if ([CNCAppOptions sharedInstance].appSize == AppSizeSmall)
	{
		self.currentQuote.email = [CNCSettings userEmail];
	}
	else
	{
		self.currentQuote.email = @"";
	}

}

- (void)adjustForManagersApproval {
    
    // managersApprovalHeight = the height of the text box and label, plus the space between previous field and this one
    const CGFloat managersApprovalHeight = CGRectGetMaxY(self.managersSignature.frame) - CGRectGetMinY(self.managersSignatureLabel.frame)
    + CGRectGetMinY(self.managersSignatureLabel.frame) - CGRectGetMaxY(self.associateNamePicker.frame);
    
    if (CGRectGetHeight(self.submitAndFooterPlaceholder.frame)!=0) {
        // if this had bee equal to 0, this means this routine was called before view loaded and everything is 0 -- we can ignore
        
        //put things back to at original if moved
        if (expandedForManager) {
            
            expandedForManager = NO;
            self.managersSignature.hidden = YES;
            self.managersSignatureLabel.hidden = YES;
            
            CGRect submitAndFooterFrame = self.submitAndFooterPlaceholder.frame;
            submitAndFooterFrame.origin.y = submitAndFooterFrame.origin.y-managersApprovalHeight;
            self.submitAndFooterPlaceholder.frame = submitAndFooterFrame;

            //adjust page up the chain
            CGRect quoteDetailsPlaceholderRect = self.quoteDetailsPlaceholder.frame;
            quoteDetailsPlaceholderRect.size.height -= managersApprovalHeight;
            self.quoteDetailsPlaceholder.frame = quoteDetailsPlaceholderRect;
            
            CGRect underNumberPlaceholderRect = self.underNumberPlaceholder.frame;
            underNumberPlaceholderRect.size.height -= managersApprovalHeight;
            self.underNumberPlaceholder.frame = underNumberPlaceholderRect;

            CGRect underModelPlaceholderRect = self.underModelPlaceholder.frame;
            underModelPlaceholderRect.size.height -= managersApprovalHeight;
            self.underModelPlaceholder.frame = underModelPlaceholderRect;
            
            
        }
        
        if ([CNCAppOptions sharedInstance].appSize == AppSizeRep)
        {
            return;  // we don't adjust manager sig for rep, managers signature is hidden.
        }
        
        
        //now move to below managers signature if needed
        if ([CNCAppOptions sharedInstance].managersApproval) {
            expandedForManager = YES;
            self.managersSignature.hidden = NO;
            self.managersSignatureLabel.hidden = NO;

            CGRect submitAndFooterFrame = self.submitAndFooterPlaceholder.frame;
            submitAndFooterFrame.origin.y = submitAndFooterFrame.origin.y+managersApprovalHeight;
            self.submitAndFooterPlaceholder.frame = submitAndFooterFrame;
            
            //adjust page up the chain
            CGRect quoteDetailsPlaceholderRect = self.quoteDetailsPlaceholder.frame;
            quoteDetailsPlaceholderRect.size.height += managersApprovalHeight;
            self.quoteDetailsPlaceholder.frame = quoteDetailsPlaceholderRect;
            
            CGRect underModelPlaceholderRect = self.underModelPlaceholder.frame;
            underModelPlaceholderRect.size.height += managersApprovalHeight;
            self.underModelPlaceholder.frame = underModelPlaceholderRect;
            
            CGRect underNumberPlaceholderRect = self.underNumberPlaceholder.frame;
            underNumberPlaceholderRect.size.height += managersApprovalHeight;
            self.underNumberPlaceholder.frame = underNumberPlaceholderRect;
            
            
        }
        
    }

}


- (void)adjustForQuoteDetails {
    
    // if this is a rep user, then we want to remove the quote details section entirely
    
    // quoteDetailsHeight = the height of the items we are removing in the quote detials
    const CGFloat quoteDetailsHeight = CGRectGetMaxY(self.associateNamePicker.frame);  // maxy here is max relative to top of the details section
    
    // check to see if the view is really loaded by looking at the height of item, in this case footer.  If it's 0, then view not loaded
    if (CGRectGetHeight(self.submitAndFooterPlaceholder.frame)!=0) {
        
        // first off, let's put everything back in place if it was moved
        if (quoteDetailsHidden)
        {
            quoteDetailsHidden = NO;
            self.quoteDetailsHeaderLabel.hidden = NO;
            self.tradeInLabel.hidden = NO;
            self.segmentTradeIn.hidden = NO;
            self.associateNameLabel.hidden = NO;
            self.associateNamePicker.hidden = NO;
            self.retailerIdTextField.hidden = NO;
            self.otherAssociateLabel.hidden = NO;
            self.otherAssociate.hidden = NO;
            self.managersSignature.hidden = NO;
            self.managersSignatureLabel.hidden = NO;
            
            CGRect submitAndFooterFrame = self.submitAndFooterPlaceholder.frame;
//            submitAndFooterFrame.origin.y = CGRectGetMaxY(self.managersSignature.frame);   //???
            submitAndFooterFrame.origin.y = submitAndFooterFrame.origin.y+quoteDetailsHeight;
            self.submitAndFooterPlaceholder.frame = submitAndFooterFrame;
            
            //adjust page up the chain
            CGRect quoteDetailsPlaceholderRect = self.quoteDetailsPlaceholder.frame;
            quoteDetailsPlaceholderRect.size.height += quoteDetailsHeight;
            self.quoteDetailsPlaceholder.frame = quoteDetailsPlaceholderRect;

            CGRect underNumberPlaceholderRect = self.underNumberPlaceholder.frame;
            underNumberPlaceholderRect.size.height += quoteDetailsHeight;
            self.underNumberPlaceholder.frame = underNumberPlaceholderRect;
            
            CGRect underModelPlaceholderRect = self.underModelPlaceholder.frame;
            underModelPlaceholderRect.size.height += quoteDetailsHeight;
            self.underModelPlaceholder.frame = underModelPlaceholderRect;
            

            
        }
        
        // now, if this is rep, then we hide quote details
        if ([CNCAppOptions sharedInstance].appSize == AppSizeRep)
        {
            quoteDetailsHidden = YES;
            self.quoteDetailsHeaderLabel.hidden = YES;
            self.tradeInLabel.hidden = YES;
            self.segmentTradeIn.hidden = YES;
            self.associateNameLabel.hidden = YES;
            self.associateNamePicker.hidden = YES;
            self.retailerIdTextField.hidden = YES;
            self.otherAssociateLabel.hidden = YES;
            self.otherAssociate.hidden = YES;
            self.managersSignature.hidden = YES;
            self.managersSignatureLabel.hidden = YES;
            
            CGRect submitAndFooterFrame = self.submitAndFooterPlaceholder.frame;
            submitAndFooterFrame.origin.y = submitAndFooterFrame.origin.y-quoteDetailsHeight;
            self.submitAndFooterPlaceholder.frame = submitAndFooterFrame;
            
            CGRect quoteDetailsPlaceholderRect = self.quoteDetailsPlaceholder.frame;
            quoteDetailsPlaceholderRect.size.height -= quoteDetailsHeight;
            self.quoteDetailsPlaceholder.frame = quoteDetailsPlaceholderRect;
            
            CGRect underNumberPlaceholderRect = self.underNumberPlaceholder.frame;
            underNumberPlaceholderRect.size.height -= quoteDetailsHeight;
            self.underNumberPlaceholder.frame = underNumberPlaceholderRect;

            CGRect underModelPlaceholderRect = self.underModelPlaceholder.frame;
            underModelPlaceholderRect.size.height -= quoteDetailsHeight;
            self.underModelPlaceholder.frame = underModelPlaceholderRect;


        }
        
    }
    
}


- (IBAction) getAnotherQuote: (id)sender
{
	if (self.menuDelegate)
	{
		[self.menuDelegate didSelectNewQuoteItem];
	}
}

- (IBAction) viewAllPendingQuotes: (id)sender
{
	if (self.menuDelegate)
	{
		[self.menuDelegate didSelectPendingQuotesItem];
	}
}



@end
