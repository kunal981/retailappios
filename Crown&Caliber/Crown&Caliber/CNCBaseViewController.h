//
//  CNCBaseViewController.h
//  Crown&Caliber
//
//  Created by Oleg Lavrentyev on 12/3/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNCBaseViewController : UIViewController

- (void) displayAlert: (NSString*) message withTitle: (NSString*) title;

- (void) displayAlert: (NSString*) message withTitle: (NSString*) title completion: (void (^)(void))completion;

@end
