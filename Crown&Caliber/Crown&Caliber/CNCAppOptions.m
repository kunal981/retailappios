//
//  CNCAppOptions.m
//  Crown&Caliber
//
//  Created by Oleg Lavrentyev on 12/1/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCAppOptions.h"

@implementation CNCAppOptions

+ (CNCAppOptions*) sharedInstance
{
	static CNCAppOptions *sharedInstance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedInstance = [[CNCAppOptions alloc] init];
	});
	return sharedInstance;
}

@end
