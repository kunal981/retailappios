//
//  CNCBaseViewController.m
//  Crown&Caliber
//
//  Created by Oleg Lavrentyev on 12/3/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCBaseViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "CNCAppOptions.h"


@interface CNCBaseViewController () <UIAlertViewDelegate>

@property (nonatomic, strong) void (^alertCompletion)(void);

@end

@implementation CNCBaseViewController

- (void) viewDidLoad
{
    [super viewDidLoad];

	for (UIImageView* subview in self.navigationItem.titleView.subviews)
	{
		if ([subview isKindOfClass: [UIImageView class]])
		{
			if ([CNCAppOptions sharedInstance].logoURL.length > 0)
			{
				NSURL* logoURL = [NSURL URLWithString: [CNCAppOptions sharedInstance].logoURL];
				subview.frame = CGRectMake(0, -4, 400, 40);
				[subview setImageWithURL: logoURL];
			}
		}
	}
}

- (void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    self.navigationItem.titleView.frame = self.navigationItem.titleView.superview.bounds;

    for (UIImageView* subview in self.navigationItem.titleView.subviews)
    {
        if ([subview isKindOfClass: [UIImageView class]])
        {
            subview.center = CGPointMake(CGRectGetMidX(self.navigationItem.titleView.frame),
                                         CGRectGetMidY(self.navigationItem.titleView.frame));
            subview.contentMode = UIViewContentModeCenter;
        }
    }
}

- (void) displayAlert: (NSString*) message withTitle: (NSString*) title
{
    [self displayAlert: message withTitle: title completion: nil];
}

- (void) displayAlert: (NSString*) message withTitle: (NSString*) title completion: (void (^)(void))completion
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle: title
                                                    message: message
                                                   delegate: nil
                                          cancelButtonTitle: @"OK"
                                          otherButtonTitles: nil];
    if (completion != nil)
    {
        self.alertCompletion = completion;
        alert.delegate = self;
    }

    [alert show];
}

#pragma mark - UIAlertViewDelegate

- (void) alertView: (UIAlertView*) alertView clickedButtonAtIndex: (NSInteger) buttonIndex
{
    if (self.alertCompletion != nil)
    {
        self.alertCompletion();
    }
}

@end
