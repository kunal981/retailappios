//
//  CNCAppDelegate.h
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MessageUI/MessageUI.h>

@interface CNCAppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIWindow *window;

+ (MFMailComposeViewController*) sharedMailController;
+ (void) recreateMailVC;

@end
