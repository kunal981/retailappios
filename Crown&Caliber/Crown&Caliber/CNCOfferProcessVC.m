//
//  CNCOfferProcessVC.m
//  Crown&Caliber
//
//  Created by Valeriy on 9/23/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCOfferProcessVC.h"
#import "CNCQuote.h"
#import "CNCCase.h"
#import "CNCPaintImageView.h"

#import <MessageUI/MessageUI.h>
#import "CNCAppDelegate.h"

#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "CNCConfig.h"
#import "CNCWebViewController.h"
#import "CNCTextField.h"

#import "CNCShareSheetController.h"
#import "CNCAppOptions.h"
#import "CNCSettings.h"

#import "CNCPrintActivity.h"
#import "CNCPrintController.h"
#import "UIImageView+AFNetworking.h"
#import "MBProgressHUD.h"
#import "CNCApi.h"
#import "CNCPendingTableVC.h"
#import "CNCHistoryInfoViewController.h"

#import <BlocksKit/UIActionSheet+BlocksKit.h>
#import <BlocksKit/UIImagePickerController+BlocksKit.h>

@interface CNCOfferProcessVC () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, MFMailComposeViewControllerDelegate, UIAlertViewDelegate, UITextFieldDelegate, CNCPrintActivityDelegate, CNCPaintImageViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *mainInfoContainer;

@property (weak, nonatomic) IBOutlet UIImageView *driveLicenseView;

@property (weak, nonatomic) IBOutlet UILabel *descrLabel;

@property (weak, nonatomic) IBOutlet UILabel *signatureLabel;
@property (weak, nonatomic) IBOutlet UIButton *signatureClearButton;
@property (weak, nonatomic) IBOutlet CNCPaintImageView *signatureView;

@property (weak, nonatomic) IBOutlet UIButton *acceptButton;
@property (weak, nonatomic) IBOutlet UIButton *declineButton;

@property (weak, nonatomic) IBOutlet CNCTextField *offerAmountField;
@property (nonatomic, strong) UIImage* driverLicense;
@property (weak, nonatomic) IBOutlet UILabel *driverLicenseLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cameraIcon;


@property (nonatomic, strong) CNCShareSheetController* shareSheetController ;

@property (nonatomic, strong) UIWebView* webview ;

- (IBAction) accceptButtonPressed: (UIButton*) sender;

- (IBAction) declineButtonPressed: (UIButton*) sender;

- (IBAction) clearPressed: (UIButton*) sender;

- (IBAction) driverLicensePressed: (id) sender;

- (IBAction)termsAndConditionsPressed:(UIButton *)sender;
- (IBAction)privacyPolicyPressed:(UIButton *)sender;

- (IBAction)textFieldDidEndEdit:(CNCTextField *)sender;

@end

@implementation CNCOfferProcessVC

- (id) initWithNibName: (NSString*) nibNameOrNil bundle: (NSBundle*) nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) setQuote: (CNCQuote*) aQuote
{
    _quote = aQuote;

    [self updateQuote];
}

- (void) setCaseData: (CNCCase*) caseData
{
    _caseData = caseData;

    [self updateQuote];
}

- (void) updateQuote
{
    self.brandLabel.text = self.quote.brand;
    self.modelLabel.text = self.quote.modelName;
    self.modelNumLabel.text = self.quote.modelNumber;
    self.nameLabel.text = self.quote.name;
    self.phoneLabel.text = self.quote.phone;
    self.caseIdLabel.text = [self.quote.caseId description];
    self.associateNameLabel.text = self.quote.retailerId;

    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle: NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle: NSDateFormatterMediumStyle];

    self.receivedLabel.text = [dateFormatter stringFromDate: self.quote.createdAt];
    self.offerLabel.text = self.caseData.offerText;
    
    if (self.quote.imageURL.length > 0)
    {
        NSURL* watchURL = [NSURL URLWithString: self.quote.imageURL];
        [self.watchPhoto setImageWithURL: watchURL];
    }


    self.driveLicenseView.image = self.quote.driverLicenseImage;
    
    self.driverLicenseLabel.hidden = NO;
    self.cameraIcon.hidden = NO;
    self.driveLicenseView.hidden = NO;
    if (self.caseData.driversLicenseOnFile) {
        self.driverLicenseLabel.hidden = YES;
        self.cameraIcon.hidden = YES;
        self.driveLicenseView.hidden = YES;
    }
    
    self.signatureLabel.hidden = NO;
    self.signatureView.hidden = NO;
    self.descrLabel.hidden = NO;
    self.signatureClearButton.hidden = NO;
//  Temporarily commenting out, client changed mind about signature on file flow
//  Leaving because they might change mind back in upcoming releases
//    if (self.caseData.signatureOnFile) {
//        self.signatureLabel.hidden = YES;
//        self.signatureView.hidden = YES;
//        self.descrLabel.hidden = YES;
//        self.signatureClearButton.hidden = YES;
//    }
}

- (void) dealloc
{
	[self removeSignature];
	_menuDelegate = nil;
	[_signatureView stopDrawing];
}

- (void) viewDidLoad
{
    [super viewDidLoad];

    // Do any additional setup after loading the view.
	self.driveLicenseView.userInteractionEnabled = YES;

	[self.signatureView setBrush: 2.0];
	[self.signatureView setColor: [UIColor blackColor]];
	[self.signatureView startDrawing];
    self.signatureView.delegate = self;

	UIBarButtonItem* shareItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemAction
																			   target: self
																			   action: @selector(shareItemPressed:)];
	self.navigationItem.rightBarButtonItem = shareItem;
	
    [self updateQuote];
    [self updateAcceptButton];
}

- (void) updateAcceptButton
{
    UIImage* signature = [self.signatureView drawedImage];

    BOOL driversLicenseOK = self.caseData.driversLicenseOnFile || self.quote.driverLicenseImage;
    //  Temporarily commenting out, client changed mind about signature on file flow
    //  Leaving because they might change mind back in upcoming releases
    //BOOL signatureOK = self.caseData.signatureOnFile  || signature;  // must have one on file or signed
    //self.acceptButton.enabled = signatureOK && driversLicenseOK && self.ageSwitch.on;

    self.acceptButton.enabled = signature && driversLicenseOK && self.ageSwitch.on;
}

- (void) shareItemPressed: (UIBarButtonItem*) shareItem
{
	UIImage* signature = [self.signatureView drawedImage];
	if (!signature)
	{
		UIAlertView* alert = [[UIAlertView alloc] initWithTitle: nil
														message: @"Please draw signature for more options."
													   delegate: nil
											  cancelButtonTitle: @"Ok"
											  otherButtonTitles: nil];
		
		[alert show];
		return;
	}
	
	[self saveSignatureToDocuments];
	
	CNCPrintActivity* printActivity = [[CNCPrintActivity alloc] init];
	printActivity.delegate = self;
	
	
	NSArray* exludedActivityTypes = @[UIActivityTypePostToWeibo,
				   UIActivityTypePrint,
				   UIActivityTypeMessage,
				   UIActivityTypeCopyToPasteboard,
				   UIActivityTypeAssignToContact,
				   UIActivityTypeSaveToCameraRoll,
				   UIActivityTypeAddToReadingList];
	CNCShareSheetController* shareSheetController = [[CNCShareSheetController alloc] initWithActivityItems: @[ [self generateHTMLStringAcceptedForPrintAttachSignatureLink: NO], [NSData dataWithContentsOfFile: [self signaturePath]]]
																					 applicationActivities: @[printActivity]
																					  exludedActivityTypes: exludedActivityTypes];
	
	[shareSheetController startForViewController: self fromItem: shareItem];
	self.shareSheetController = shareSheetController;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction) accceptButtonPressed: (UIButton*) sender
{
    [MBProgressHUD showHUDAddedTo: self.parentViewController.view animated: YES];

    UIImage* signatureImage = [self.signatureView drawedImage];
    UIImage* driverLicenseImage = self.quote.driverLicenseImage;

    [[CNCApi sharedClient] uploadDriversLicenseImage: driverLicenseImage
                                            toCaseID: self.caseData.caseId
                                            forStoreID:[CNCSettings userId]
                                         withSuccess: ^(NSError* error)
     {
         if (error == nil)
         {
             [[CNCApi sharedClient] uploadSignatureImage: signatureImage
                                                     toCaseID: self.caseData.caseId
                                                     forStoreID:[CNCSettings userId]
                                                  withSuccess: ^(NSError* error)
              {
                  if (error == nil)
                  {
                      [[CNCApi sharedClient] acceptCase: self.quote.caseId
                                           successBlock: ^(NSString* thankYou, NSError* error, BOOL redirect)
                       {
                           [MBProgressHUD hideAllHUDsForView: self.parentViewController.view animated: YES];

                           if (redirect)
                           {
                               [[NSNotificationCenter defaultCenter] postNotificationName:@"requestSignout" object:self];
                               return;
                           }

                           if (error == nil)
                           {
                               [self displayAlert: thankYou
                                        withTitle: @"Thank You"
                                       completion: ^{
                                           for (UIViewController* viewController in self.navigationController.viewControllers)
                                           {
                                               if (([viewController isKindOfClass: [CNCPendingTableVC class]]) || ([viewController isKindOfClass: [CNCHistoryInfoViewController class]]))
                                               {
                                                   [self.navigationController popToViewController: viewController animated: YES];
                                                   break;
                                               }
                                           }
                                       }
                                ];
                           }
                           else
                           {
                               [self displayAlert: [error localizedDescription] withTitle: @"Error"];
                           }
                       }];
                  }
                  else
                  {
                      [MBProgressHUD hideAllHUDsForView: self.parentViewController.view animated: YES];

                      [self displayAlert: [error localizedDescription] withTitle: @"Error"];
                  }
              }];
         }
         else
         {
             [MBProgressHUD hideAllHUDsForView: self.parentViewController.view animated: YES];

             [self displayAlert: [error localizedDescription] withTitle: @"Error"];
         }
     }];
}

- (IBAction) declineButtonPressed: (UIButton* )sender
{
	[self startMessageVCStateAccepted: NO];
}

- (void) pickImageForVIew: (UIImageView*) imaegView
			   completion: (void (^)(UIImage *resultImage))success
{
	if ([UIAlertAction class])
	{
		if (imaegView)
		{
			UIAlertController *actionSheetController = [UIAlertController alertControllerWithTitle: nil message: nil preferredStyle: UIAlertControllerStyleActionSheet];

			UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
									 {
										 if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
										 {
											 return;
										 }
										 UIImagePickerController* imagePickerController =  [[UIImagePickerController alloc] init];
										 [imagePickerController  setSourceType:UIImagePickerControllerSourceTypeCamera];
										 imagePickerController.delegate = self;

										 [imagePickerController setBk_didFinishPickingMediaBlock:^(UIImagePickerController *picker, NSDictionary *info) {
											 [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
											 UIImage* resultImage = [info objectForKey:UIImagePickerControllerOriginalImage];

											 [picker dismissViewControllerAnimated:YES completion:nil];

											 if (success) {
												 success(resultImage);
											 }
										 }];

										 [self presentViewController:imagePickerController animated:YES completion:NULL];
									 }];

			UIAlertAction *library = [UIAlertAction actionWithTitle:@"Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
									  {
										  UIImagePickerController* imagePickerController =  [[UIImagePickerController alloc] init];
										  [imagePickerController  setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];

										  imagePickerController.delegate = self;
										  [imagePickerController setBk_didFinishPickingMediaBlock:^(UIImagePickerController *picker, NSDictionary *info) {
											  [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

											  UIImage* resultImage = [info objectForKey:UIImagePickerControllerOriginalImage];
											  [picker dismissViewControllerAnimated:YES completion:nil];

											  if (success) {
												  success(resultImage);
											  }
										  }];

										  [self presentViewController:imagePickerController animated:YES completion:NULL];
									  }];

			[actionSheetController addAction: camera];
			[actionSheetController addAction: library];

			actionSheetController.view.tintColor = [UIColor blackColor];

			[actionSheetController setModalPresentationStyle: UIModalPresentationPopover];

			UIPopoverPresentationController *popPresenter = [actionSheetController
															 popoverPresentationController];
			popPresenter.sourceView = imaegView;
			popPresenter.sourceRect = imaegView.bounds;
			[self presentViewController: actionSheetController animated:YES completion:nil];
		}
	}
	else
	{
		UIActionSheet *imagePickAction = [UIActionSheet bk_actionSheetWithTitle:nil];

		[imagePickAction bk_addButtonWithTitle:@"Camera" handler:^{

			if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
			{
				return;
			}

			UIImagePickerController* imagePickerController =  [[UIImagePickerController alloc] init];
			[imagePickerController  setSourceType:UIImagePickerControllerSourceTypeCamera];
			imagePickerController.delegate = self;

			[imagePickerController setBk_didFinishPickingMediaBlock:^(UIImagePickerController *picker, NSDictionary *info) {
				[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
				UIImage* resultImage = [info objectForKey:UIImagePickerControllerOriginalImage];

				[picker dismissViewControllerAnimated:YES completion:nil];

				if (success) {
					success(resultImage);
				}
			}];

			[self presentViewController:imagePickerController animated:YES completion:NULL];
		}];

		[imagePickAction bk_addButtonWithTitle:@"Library" handler:^{
			UIImagePickerController* imagePickerController =  [[UIImagePickerController alloc] init];
			[imagePickerController  setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];

			imagePickerController.delegate = self;
			[imagePickerController setBk_didFinishPickingMediaBlock:^(UIImagePickerController *picker, NSDictionary *info) {
				[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

				UIImage* resultImage = [info objectForKey:UIImagePickerControllerOriginalImage];
				[picker dismissViewControllerAnimated:YES completion:nil];

				if (success) {
					success(resultImage);
				}
			}];

			[self presentViewController:imagePickerController animated:YES completion:NULL];

		}];

		[imagePickAction bk_setCancelButtonWithTitle:@"Cancel" handler:^{
			[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
		}];

		[imagePickAction showInView:self.view];
	}
}


- (IBAction) driverLicensePressed: (id) sender
{
	[self pickImageForVIew: self.driveLicenseView
				completion: ^(UIImage *resultImage) {
					self.driveLicenseView.image = resultImage;
					self.quote.driverLicenseImage = resultImage;
					self.driverLicenseLabel.hidden = YES;
					self.cameraIcon.hidden = YES;
                    [self updateAcceptButton];
				}];
}

- (IBAction) termsAndConditionsPressed: (UIButton*) sender
{
	[self presentWebVCWithHtml: [CNCAppOptions sharedInstance].terms];
}

- (IBAction) privacyPolicyPressed: (UIButton*) sender
{
	[self presentWebVCWithLink: kCNCPrivacyURL];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
	return [textField resignFirstResponder];
}

- (IBAction) textFieldDidEndEdit: (CNCTextField*) sender
{
	
}

- (void) presentWebVCWithLink: (NSString*) link
{
	UINavigationController* webNavigationController = [self.storyboard instantiateViewControllerWithIdentifier: @"webNavigationController"];
	CNCWebViewController* webViewController = (CNCWebViewController*) webNavigationController.topViewController;
	[webViewController loadPageFromString: link];
	
	UIBarButtonItem* doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemDone
																			  target: self
																			  action: @selector(donePressed)];
	webViewController.navigationItem.rightBarButtonItem = doneItem;
	[self presentViewController: webNavigationController animated: YES completion: nil];
	
}

- (void) presentWebVCWithHtml: (NSString*) html
{
    UINavigationController* webNavigationController = [self.storyboard instantiateViewControllerWithIdentifier: @"webNavigationController"];
    CNCWebViewController* webViewController = (CNCWebViewController*) webNavigationController.topViewController;
    [webViewController loadPageFromHtml: html];

    UIBarButtonItem* doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemDone
                                                                              target: self
                                                                              action: @selector(donePressed)];
    webViewController.navigationItem.rightBarButtonItem = doneItem;
    [self presentViewController: webNavigationController animated: YES completion: nil];
    
}

- (void) donePressed
{
	[self dismissViewControllerAnimated: YES completion: nil];
}

- (IBAction) clearPressed: (UIButton*) sender
{
	[self.signatureView resetImage];
    [self updateAcceptButton];
}

- (void) showThankYouAlert
{
	UIAlertView* alert = [[UIAlertView alloc] initWithTitle: @"Thank You"
													message: nil
												   delegate: self
										  cancelButtonTitle: @"OK" otherButtonTitles: nil];
	[alert show];
}

#pragma mark - Events

- (IBAction) ageSwitchChanged: (id) sender
{
    [self updateAcceptButton];
}

#pragma mark - CNCPaintImageViewDelegate

- (void) paintImageViewDidChange: (CNCPaintImageView*) paintImageView
{
    [self updateAcceptButton];
}

#pragma mark UIImagePickerControllerDelegate protocol


- (void) imagePickerControllerDidCancel: (UIImagePickerController*) picker
{
	[picker dismissViewControllerAnimated: YES completion: nil];
}


- (void) startMessageVCStateAccepted: (BOOL) accepted
{
	MFMailComposeViewController* mailVC = [CNCAppDelegate sharedMailController];
	mailVC.mailComposeDelegate = self;
	
	NSString* status = (accepted) ? @"Accepted" : @"Declined";
	
	[mailVC setToRecipients: @[@"bnollner@crownandcaliber.com"]];
	[mailVC setSubject: [NSString stringWithFormat: @"[ERP] RTIP %@! Case # %@", status, self.quote.caseId]];
	[mailVC setMessageBody: [self generateHTMLStringAccepted: accepted] isHTML: YES];

	if (accepted)
	{
		[mailVC addAttachmentData: UIImageJPEGRepresentation(self.quote.driverLicenseImage, 1.0)
						 mimeType: (NSString*)kUTTypeImage
						 fileName: @"Driver License.png"];
		[mailVC addAttachmentData: UIImageJPEGRepresentation([self.signatureView drawedImage], 1.0)
						 mimeType: (NSString*)kUTTypeImage
						 fileName: @"Signature.png"];
	}
	
	[self presentViewController: mailVC animated: YES completion: nil];
}

- (NSString*) generateHTMLStringAccepted: (BOOL) accepted
{
	NSString* status = accepted ? @"Accepted" : @"Declined";
	
	NSString* htmlString =
	@"<html> \
	<body>\
	<head>\
	<title></title> </head>";
	
	NSString* endString =
	@"</body>\
	</html>";
	
	NSString* divider = @"</br>";
	
	htmlString = [htmlString stringByAppendingString: [NSString stringWithFormat: @"An RTIP has just arrived from %@ for a %@",
							self.quote.storeName,
							self.quote.brand]];
	htmlString = [htmlString stringByAppendingString: divider];
	htmlString = [htmlString stringByAppendingString: divider];
	
	htmlString = [htmlString stringByAppendingString: [NSString stringWithFormat: @"Email: %@</br>Phone: %@",
													   self.quote.email,
													   self.quote.phone]];
	
	htmlString = [htmlString stringByAppendingString: divider];
	htmlString = [htmlString stringByAppendingString: divider];
	
	htmlString = [htmlString stringByAppendingString: [NSString stringWithFormat: @"Case ID # %@</br>Case Satus: %@",
													   self.quote.caseId,
													   status]];
	
	htmlString = [htmlString stringByAppendingString: divider];
	htmlString = [htmlString stringByAppendingString: divider];
	
	htmlString = [htmlString stringByAppendingString: [NSString stringWithFormat: @"Open the case right now in the ERP at:\
													   </br><a>https://portal.crownandcaliber.com/#/cases/%@/edit</a>", self.quote.caseId]];
	
//	Source: iOS Retailer App    Thank you!
	htmlString = [htmlString stringByAppendingString: divider];
	htmlString = [htmlString stringByAppendingString: divider];
	
	if (!accepted)
	{
		htmlString = [htmlString stringByAppendingString: @"Feedback:"];
		
		htmlString = [htmlString stringByAppendingString: divider];
		htmlString = [htmlString stringByAppendingString: divider];
		htmlString = [htmlString stringByAppendingString: divider];
		htmlString = [htmlString stringByAppendingString: divider];
	}
	else if ([self validateOfferAmount: self.offerAmountField.text])
	{
		htmlString = [htmlString stringByAppendingString: [NSString stringWithFormat: @"Offer Amount: %@", self.offerAmountField.text]];
		
		htmlString = [htmlString stringByAppendingString: divider];
		htmlString = [htmlString stringByAppendingString: divider];
		htmlString = [htmlString stringByAppendingString: divider];
		htmlString = [htmlString stringByAppendingString: divider];
	}
	
	htmlString = [htmlString stringByAppendingString: @"Source: iOS Retailer App"];
	
	htmlString = [htmlString stringByAppendingString: divider];
	htmlString = [htmlString stringByAppendingString: divider];
	
	htmlString = [htmlString stringByAppendingString: @"Thank you!"];
	
	htmlString = [htmlString stringByAppendingString: endString];
	
	return htmlString;
}

- (NSString*) generateHTMLStringAcceptedForPrint
{
	return [self generateHTMLStringAcceptedForPrintAttachSignatureLink: YES];
}

- (NSString*) generateHTMLStringAcceptedForPrintAttachSignatureLink: (BOOL) attachLink
{
	
	NSString* htmlString =
	@"<html> \
	<body>\
	<head>\
	<title></title> </head> ";
	
	htmlString = [htmlString stringByAppendingString: @"<style>\
				  @media print {\
				  .more {\
				  page-break-before: always;\
				  }\
				  }\
				  </style>"];
	
	NSString* endString =
	@"</body>\
	</html>";
	
	NSString* divider = @"</br>";
	
	htmlString = [htmlString stringByAppendingString: [NSString stringWithFormat: @"Full name: %@",
													   self.quote.name]];
	htmlString = [htmlString stringByAppendingString: divider];
	htmlString = [htmlString stringByAppendingString: divider];
	
	htmlString = [htmlString stringByAppendingString: [NSString stringWithFormat: @"Email: %@</br>Phone Number: %@",
													   self.quote.email,
													   self.quote.phone]];
	
	htmlString = [htmlString stringByAppendingString: divider];
	htmlString = [htmlString stringByAppendingString: divider];
	
	htmlString = [htmlString stringByAppendingString: [NSString stringWithFormat: @"Case ID # %@",
													   self.quote.caseId]];
	
	htmlString = [htmlString stringByAppendingString: divider];
	htmlString = [htmlString stringByAppendingString: divider];
	//	Watch Brand
	
	htmlString = [htmlString stringByAppendingString: [NSString stringWithFormat: @"Watch Brand # %@",
													   self.quote.brand]];
	
	htmlString = [htmlString stringByAppendingString: divider];
	htmlString = [htmlString stringByAppendingString: divider];
	
	//	htmlString = [htmlString stringByAppendingString: [NSString stringWithFormat: @"Open the case right now in the ERP at:\
	//													   </br><a>https://portal.crownandcaliber.com/#/cases/%@/edit</a>", self.quote.caseId]];
	
	
	htmlString = [htmlString stringByAppendingString: @"By signing below, Customer hereby (i) acknowledges and agrees that Customer has read, and hereby agrees to be bound by, the Terms and Conditions, (ii) agrees to deliver to Retail Partner such Customer's Merchandise and Accessories, (iii) hereby sells such Customer's Merchandise in exchange for Retail Partner's store credit, in accordance with, and subject to, the Terms and Conditions, and (iv) acknowledges that Customer has read and understands Retail Partner's policies and procedures governing Retail Partner's store credit."];
	//	Source: iOS Retailer App    Thank you!
	htmlString = [htmlString stringByAppendingString: divider];
	htmlString = [htmlString stringByAppendingString: divider];
	
	if ([self validateOfferAmount: self.offerAmountField.text])
	{
		htmlString = [htmlString stringByAppendingString: [NSString stringWithFormat: @"Offer Amount: %@", self.offerAmountField.text]];
		
		htmlString = [htmlString stringByAppendingString: divider];
		htmlString = [htmlString stringByAppendingString: divider];
		htmlString = [htmlString stringByAppendingString: divider];
		htmlString = [htmlString stringByAppendingString: divider];
	}
	
	if (attachLink)
	{
		htmlString = [htmlString stringByAppendingString: [NSString stringWithFormat:@"<img src=\"%@\"/>", @"Signature.jpg"]];
		htmlString = [htmlString stringByAppendingString: divider];
		htmlString = [htmlString stringByAppendingString: divider];
	}
	
	//	TermsAndConditions.html
	NSString* path = [[NSBundle mainBundle] pathForResource: @"TermsAndConditions" ofType: @"html"];
	NSString* termsAndConditionsHtmlString = [NSString stringWithContentsOfFile: path
																	   encoding: NSUTF8StringEncoding
																		  error: nil];
	
	htmlString = [htmlString stringByAppendingString: termsAndConditionsHtmlString];
	
	htmlString = [htmlString stringByAppendingString: @"Thank you!"];
	
	htmlString = [htmlString stringByAppendingString: endString];
	
	return htmlString;
}

#define OFFERAMOUNTACCEPTEDSYMBOLS @"1234567890."
- (BOOL) validateOfferAmount: (NSString*) offerAmount
{
	return YES;

	NSCharacterSet *unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString: OFFERAMOUNTACCEPTEDSYMBOLS] invertedSet];
	
	NSRange range = [offerAmount rangeOfCharacterFromSet: unacceptedInput];
	
	return (offerAmount.length >= 1 && offerAmount.length <= 50 && range.location == NSNotFound);
}

#pragma mark MFMailComposeViewControllerDelegate protocol

- (void) mailComposeController: (MFMailComposeViewController*) controller
		   didFinishWithResult: (MFMailComposeResult) result
						 error: (NSError*) error
{
	[CNCAppDelegate recreateMailVC];
	if (result == MFMailComposeResultSent)
	{
		[self showThankYouAlert];
		[controller dismissViewControllerAnimated: YES completion: nil];
	}
	else if (result == MFMailComposeResultCancelled)
	{
		[controller dismissViewControllerAnimated: YES completion: nil];
	}
	else if (result == MFMailComposeResultFailed)
	{
		UIAlertView* alert = [[UIAlertView alloc] initWithTitle: nil
														message: @"Sending failed. Please try again."
													   delegate: nil
											  cancelButtonTitle: @"OK" otherButtonTitles: nil];
		[alert show];
	}
}

#pragma mark UIAlertViewDelegate protocol

- (void) alertView: (UIAlertView*) alertView willDismissWithButtonIndex: (NSInteger) buttonIndex
{
	if (_delegate)
	{
		[_delegate offerProcessViewControllerEnd: self];
	}
}



#pragma mark CNCPrintActivityDelegate protocol

- (void) printActivityPerfomed
{
	[self.shareSheetController dismiss];
	
//	UIImage* signature = [self.signatureView drawedImage];
//	if (!signature)
//	{
//		UIAlertView* alert = [[UIAlertView alloc] initWithTitle: nil
//														message: @"Signature is required."
//													   delegate: nil
//											  cancelButtonTitle: @"Ok"
//											  otherButtonTitles: nil];
//		
//		[alert show];
//		return;
//	}
	
	[self saveSignatureToDocuments];
	
	if (!self.webview)
	{
		self.webview = [[UIWebView alloc] initWithFrame: CGRectZero];
	}
	else
	{
		[self.webview reload];
	}
	NSString* hTMLStringAcceptedForPrint = [self generateHTMLStringAcceptedForPrint];
	NSURL* baseURL = [NSURL fileURLWithPath: [self signaturePath]];
	[self.webview loadHTMLString: hTMLStringAcceptedForPrint baseURL: baseURL];
	
	CNCPrintController* printController = [[CNCPrintController alloc] initWithPrintFormatter: [self.webview viewPrintFormatter]];
	
	[printController startPrintMenuforViewController: self
										 fromBarItem: self.navigationItem.rightBarButtonItem];
}

- (void) saveSignatureToDocuments
{
//	[self removeSignature];
	[UIImageJPEGRepresentation([self.signatureView drawedImage], 1.0)
	 writeToFile: [self signaturePath]
	 atomically: YES];
}

- (NSString*) signaturePath
{
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
														 NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];

	NSString *resourceDocPath = [[NSString alloc] initWithString: documentsDirectory];
	
	NSString *filePath = [resourceDocPath stringByAppendingPathComponent: @"Signature.jpg"];

	return filePath;
}

- (void) removeSignature
{
	NSString* signaturePath = [self signaturePath];
	if ([[NSFileManager defaultManager] fileExistsAtPath: signaturePath])
	{
		[[NSFileManager defaultManager] removeItemAtPath: signaturePath error: nil];
	}
}

@end
