//
//  CNCAppDelegate.m
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCAppDelegate.h"
#import "UIColor+CNCColor.h"
#import "CNCQuoteProcessor.h"
#import "CNCApi.h"
#import "MBProgressHUD.h"
#import "CNCAppOptions.h"
#import "CNCSettings.h"

@interface CNCAppDelegate ()

@property (nonatomic, strong) MFMailComposeViewController* mailController;

@end


@implementation CNCAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [UISegmentedControl appearance].tintColor = [UIColor CNCOrangeColor];
	[CNCAppDelegate recreateMailVC];
	
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [application.windows[0] setTintColor:[UIColor whiteColor]];
        application.statusBarStyle = UIStatusBarStyleLightContent;
        
        [[UINavigationBar appearance] setBarTintColor:[UIColor blackColor]];
    }

	if ([CNCSettings userId] != nil)
	{
		[self updateAppInfo];
	}

    return YES;
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	[self updateAppInfo];
}


- (void) updateAppInfo
{
	if ([CNCAppOptions sharedInstance].brands != nil)
		return;

	[MBProgressHUD showHUDAddedTo: self.window animated:YES];

	[[CNCApi sharedClient] upateAppInfoWithCompletionBlock: ^(NSError *error, BOOL redirect) {
		[MBProgressHUD hideAllHUDsForView: self.window animated: YES];
        if (redirect)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"requestSignout" object:self];
        }
		else if (error)
		{
			[[[UIAlertView alloc] initWithTitle: @"Network error"
										message: @"Cannot update application settings. Please retry later"
									   delegate: self
							  cancelButtonTitle: @"Retry" otherButtonTitles: nil] show];
        }
	}];

}

+ (CNCAppDelegate*) sharedDelegate
{
	return (CNCAppDelegate*) [UIApplication sharedApplication].delegate;
}

+ (MFMailComposeViewController*) sharedMailController
{
	return [CNCAppDelegate sharedDelegate].mailController;
}

+ (void) recreateMailVC
{
	[[CNCAppDelegate sharedDelegate] recreateMailVC];
}

- (void) recreateMailVC
{
	self.mailController = nil;
	self.mailController = [[MFMailComposeViewController alloc] init];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.


}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
