//
//  CNCImageViewerViewController.h
//  Crown&Caliber
//

#import <UIKit/UIKit.h>

@interface CNCImageViewerViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIButton *closeButton;

@property (strong, nonatomic) NSString *imageUrlString;


- (void)setImageUrl:(NSString *)imageUrlString;

@end