//
//  CNCCreateQuoteViewController.m
//  Crown&Caliber
//
//  Created by Igor Tomych on 08/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCCreateQuoteViewController.h"
#import "CNCFirstStepViewController.h"
#import "CNCSecondStepViewController.h"
#import "CNCThirdStepViewController.h"
#import "CNCQuote.h"
#import "CNCApi.h"
#import "CNCTextField.h"
#import <TPKeyboardAvoidingScrollView.h>
#import <MBProgressHUD.h>
#import "CNCWelcomeController.h"

@interface CNCCreateQuoteViewController () < CNCFirstStepDelegate, CNCSecondStepDelegate, CNCThirdStepDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *photoButton;
@property (weak, nonatomic) IBOutlet UIButton *informationButton;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;


@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *contentView;


@property (weak, nonatomic) IBOutlet UIView *buttonsView;
@property (strong, nonatomic) CNCFirstStepViewController *firstStepController;
@property (strong, nonatomic) CNCSecondStepViewController *secondStepController;
@property (strong, nonatomic) CNCThirdStepViewController *thirdStepController;

@property (strong, nonatomic) CNCQuote *currentQuote;



@end

@implementation CNCCreateQuoteViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.photoButton  setSelected:YES];
    [self.informationButton setEnabled:NO];
    [self.submitButton setEnabled:NO];
    
    
    self.firstStepController = [[CNCFirstStepViewController alloc] init];
    self.firstStepController.delegate = self;
    self.secondStepController = [[CNCSecondStepViewController alloc] init];
    self.secondStepController.delegate = self;
   
    self.thirdStepController = [[CNCThirdStepViewController alloc] init];
    self.thirdStepController.delegate = self;
    
    [self.contentView addSubview:self.secondStepController.view];
    [self.contentView addSubview:self.thirdStepController.view];
    [self.contentView addSubview:self.firstStepController.view];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)photoButtonPressed :(id)sender
{
    [self.firstStepController.view setHidden:NO];
    [self.secondStepController.view setHidden:YES];
    [self.thirdStepController.view setHidden:YES];
    
    [self deselectButtons];
    [(UIButton *)sender setSelected:YES];
    self.contentView.contentSize = self.firstStepController.view.frame.size;
    
}

- (IBAction)informationButtonPressed :(id)sender
{
    [self.firstStepController.view setHidden:YES];
    [self.secondStepController.view setHidden:NO];
    [self.thirdStepController.view setHidden:YES];
    
    [self deselectButtons];
    [(UIButton *)sender setSelected:YES];
    self.contentView.contentSize = self.secondStepController.view.frame.size;

}

- (IBAction)submitButtonPressed :(id)sender
{
    [self.firstStepController.view setHidden:YES];
    [self.secondStepController.view setHidden:YES];
    [self.thirdStepController.view setHidden:NO];
    
    [self deselectButtons];
    [(UIButton *)sender setSelected:YES];

}

- (void) deselectButtons
{
    [self.view endEditing:YES];
    for (UIButton *button in self.buttonsView.subviews) {
        [button setSelected:NO];
    }
}

- (void)goToSecondStepWithSuccess:(BOOL)success
{
    if (success) {
        [self.informationButton setEnabled:YES];
        [self.photoButton setSelected:NO];
        [self.informationButton setSelected:YES];
        [self.firstStepController.view setHidden:YES];
        [self.secondStepController.view setHidden:NO];
        [self.thirdStepController.view setHidden:YES];
        self.contentView.contentSize = self.secondStepController.view.frame.size;
        self.contentView.contentOffset = CGPointZero;
    }
}

-(void)goToThirdStep
{
    [self.submitButton setEnabled:YES];
    [self.informationButton setSelected:NO];
    [self.submitButton setSelected:YES];
    [self.secondStepController.view setHidden:YES];
    [self.thirdStepController.view setHidden:NO];
    self.contentView.contentSize = self.thirdStepController.view.frame.size;
    self.contentView.contentOffset = CGPointZero;


}

-(void) goToSuccessScreen
{

    [MBProgressHUD showHUDAddedTo:self.parentViewController.view animated:YES];

    
    [[CNCApi sharedClient] createQuote:self.currentQuote withSuccess:^(CNCQuote *quote, NSString* submissionText, BOOL retailTimer, NSError *error, BOOL redirect, NSString *message) {

        
        [MBProgressHUD hideAllHUDsForView:self.parentViewController.view animated:YES];
        
        if (redirect)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"requestSignout" object:self];
            return;
        }

        
        self.tabBarController.selectedIndex = 3;
        [self clearUserData];
    
//        [self.firstStepController.view setHidden:NO];
//        [self.thirdStepController.view setHidden:YES];
//        [self.photoButton setSelected:YES];
//        [self.informationButton setSelected:NO];
//        [self.submitButton setSelected:NO];
//        
//        [self.informationButton setEnabled:NO];
//        [self.submitButton setEnabled:NO];
//        [self.firstStepController deleteUserData];
//        [self.secondStepController deleteUserData];
//        [self.thirdStepController deleteUserData];
//        self.contentView.contentOffset = CGPointZero;
//        [self.view endEditing:YES];

        if (message)
        {
            [self displayAlert:message withTitle:@"Error"];
            CNCWelcomeController *controller = (CNCWelcomeController*) self.tabBarController.selectedViewController;
            controller.currentQuote = self.currentQuote;
            [controller loadFailScreen];
            return;
        } else if (error) {
            CNCWelcomeController *controller = (CNCWelcomeController*) self.tabBarController.selectedViewController;
            controller.currentQuote = self.currentQuote;
            [controller loadFailScreen];
        }

    }];
}

- (void) enableInformationButton
{
    self.informationButton.enabled = YES;
}
- (void) enableSubmitButton
{
    self.submitButton.enabled = YES;
}

- (void) disableSubmitButton
{
    self.submitButton.enabled = NO;
}

- (void) createNewQuote
{
    
    if (self.currentQuote) {
            self.currentQuote = nil;
    }
    self.currentQuote = [[CNCQuote alloc] init];
    self.currentQuote.documents = @"Box Only";
    self.currentQuote.extraInfo = @"";
    self.currentQuote.hasModelNumber = YES;
    self.currentQuote.isDomestic = YES;
    self.currentQuote.hasRetailer = NO;
	self.currentQuote.hasRetailer = YES;
	self.currentQuote.isFullQuote = YES;

    
    self.firstStepController.currentQuote = self.currentQuote;
    self.secondStepController.currentQuote = self.currentQuote;
    self.thirdStepController.currentQuote = self.currentQuote;
    [self clearUserData];
}

- (void) clearUserData
{
    [self.firstStepController.view setHidden:NO];
    [self.secondStepController.view setHidden:YES];
    [self.thirdStepController.view setHidden:YES];
    [self.photoButton setSelected:YES];
    [self.informationButton setSelected:NO];
    [self.submitButton setSelected:NO];
    
    [self.informationButton setEnabled:NO];
    [self.submitButton setEnabled:NO];
    [self.firstStepController deleteUserData];
    [self.secondStepController deleteUserData];
    [self.thirdStepController deleteUserData];
    self.contentView.contentOffset = CGPointZero;
    self.contentView.contentSize = self.firstStepController.view.frame.size;
    
    [self.view endEditing:YES];
}


@end
