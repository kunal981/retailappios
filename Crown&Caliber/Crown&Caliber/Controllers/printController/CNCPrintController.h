//
//  CNCPrintController.h
//  Crown&Caliber
//
//  Created by Valeriy on 10/7/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CNCPrintController : NSObject

- (id) initWithPrintFormatter: (UIViewPrintFormatter*) viewPrintFormatter;

- (void) startPrintMenuforViewController: (UIViewController*) viewController
							 fromBarItem: (UIBarButtonItem*) shareItem;

@end
