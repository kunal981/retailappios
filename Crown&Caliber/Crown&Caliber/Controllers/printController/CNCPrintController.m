//
//  CNCPrintController.m
//  Crown&Caliber
//
//  Created by Valeriy on 10/7/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCPrintController.h"

@interface CNCPrintController ()

@property (nonatomic, strong) UIViewPrintFormatter* viewPrintFormatter;

@end

@implementation CNCPrintController

- (id) initWithPrintFormatter: (UIViewPrintFormatter*) viewPrintFormatter
{
	if (self = [super init])
	{
		self.viewPrintFormatter = viewPrintFormatter;
	}
	
	return self;
}

- (void) startPrintMenuforViewController: (UIViewController*) viewController
							 fromBarItem: (UIBarButtonItem*) shareItem
{
	UIPrintInteractionController *controller = [UIPrintInteractionController sharedPrintController];
	if(!controller){
		NSLog(@"Couldn't get shared UIPrintInteractionController!");
		return;
	}
	
	UIPrintInteractionCompletionHandler completionHandler =
	^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
		if(!completed && error){
			NSLog(@"FAILED! due to error in domain %@ with error code %ld", error.domain, (long)error.code);
		}
	};
	
	// Obtain a printInfo so that we can set our printing defaults.
	UIPrintInfo *printInfo = [UIPrintInfo printInfo];
	// This application produces General content that contains color.
	printInfo.outputType = UIPrintInfoOutputGeneral;
	// We'll use the URL as the job name.
//	printInfo.jobName = [self.urlField text];
	// Set duplex so that it is available if the printer supports it. We are
	// performing portrait printing so we want to duplex along the long edge.
	printInfo.duplex = UIPrintInfoDuplexLongEdge;
	// Use this printInfo for this print job.
	controller.printInfo = printInfo;
	
	// Be sure the page range controls are present for documents of > 1 page.
	controller.showsPageRange = YES;
	
	// This code uses a custom UIPrintPageRenderer so that it can draw a header and footer.
	UIPrintPageRenderer *myRenderer = [[UIPrintPageRenderer alloc] init];
	
	[myRenderer addPrintFormatter: self.viewPrintFormatter
			startingAtPageAtIndex: 0];
	
	// Set our custom renderer as the printPageRenderer for the print job.
	controller.printPageRenderer = myRenderer;
	
	/*
	 The method we use to present the printing UI depends on the type of UI idiom that is currently executing. Once we invoke one of these methods to present the printing UI, our application's direct involvement in printing is complete. Our custom printPageRenderer will have its methods invoked at the appropriate time by UIKit.
	 */
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
	{
		[controller presentFromBarButtonItem: shareItem
									animated: YES
						   completionHandler:^(UIPrintInteractionController *printInteractionController, BOOL completed, NSError *error) {
			
		}];
//		[controller presentFromBarButtonItem: view animated: YES
//						   completionHandler: completionHandler];  // iPad
	}
	else {
		[controller presentAnimated:YES completionHandler:completionHandler];  // iPhone
	}
}

@end
