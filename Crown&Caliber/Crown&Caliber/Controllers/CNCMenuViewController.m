//
//  CNCMenuViewController.m
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCMenuViewController.h"
#import "CNCMenuTableViewCell.h"
#import "CNCAppOptions.h"

#ifdef Retail

    #define retailMenu @[  \
    @{@"title":@"Home", @"menuImage": @"icon_home_gray.png", @"menuImageSelected":@"icon_home_black.png"}, \
    @{@"title":@"Get a Quote", @"menuImage": @"get_quote_grey", @"menuImageSelected":@"get_quote"}, \
    @{@"title":@"Pending Quotes", @"menuImage": @"pending_quotes_grey", @"menuImageSelected":@"pending_quotes"}, \
    @{@"title":@"Watches to Ship", @"menuImage": @"watches_to_ship_grey", @"menuImageSelected":@"watches_to_ship"}, \
    @{@"title":@"History", @"menuImage": @"history_grey", @"menuImageSelected":@"history"}, \
    @{@"title":@"Terms & Conds. ", @"menuImage": @"ipad_icon_clipboard.png", @"menuImageSelected":@"ipad_icon_clipboard_black.png"}, \
    @{@"title":@"Contacts", @"menuImage": @"icon_book_gray", @"menuImageSelected":@"icon_contacts_black.png" }, \
    @{@"title":@"Sign Out", @"menuImage": @"sign_out_grey", @"menuImageSelected":@"sign_out" }]

    #define retailRepMenu @[  \
    @{@"title":@"Home", @"menuImage": @"icon_home_gray.png", @"menuImageSelected":@"icon_home_black.png"}, \
    @{@"title":@"Get a Quote", @"menuImage": @"get_quote_grey", @"menuImageSelected":@"get_quote"}, \
    @{@"title":@"Pending Quotes", @"menuImage": @"pending_quotes_grey", @"menuImageSelected":@"pending_quotes"}, \
    @{@"title":@"Terms & Conds. ", @"menuImage": @"ipad_icon_clipboard.png", @"menuImageSelected":@"ipad_icon_clipboard_black.png"}, \
    @{@"title":@"Contacts", @"menuImage": @"icon_book_gray", @"menuImageSelected":@"icon_contacts_black.png" }, \
    @{@"title":@"Sign Out", @"menuImage": @"sign_out_grey", @"menuImageSelected":@"sign_out" }]

#else

	#define nonretailMenu   @[  \
	@{@"title":@"Home", @"menuImage": @"icon_home_gray.png", @"menuImageSelected":@"icon_home_black.png"}, \
	@{@"title":@"Get a Quote", @"menuImage": @"", @"menuImageSelected":@""}, \
	@{@"title":@"Blog", @"menuImage": @"ipad_icon_book.png", @"menuImageSelected":@"ipad_icon_book_black.png"}, \
	@{@"title":@"How", @"menuImage": @"ipad_icon_video.png", @"menuImageSelected":@"ipad_icon_video_black.png"}, \
	@{@"title":@"Terms & Conds. ", @"menuImage": @"ipad_icon_clipboard.png", @"menuImageSelected":@"ipad_icon_clipboard_black.png"}, \
	@{@"title":@"Privacy Policy", @"menuImage": @"ipad_icon_eye.png", @"menuImageSelected":@"ipad_icon_eye_black.png"}, \
	@{@"title":@"Contacts", @"menuImage": @"icon_book_gray", @"menuImageSelected":@"icon_contacts_black.png" }]

#endif

@interface CNCMenuViewController ()

@property (strong, nonatomic) IBOutlet UIView *sectionHeader;
@property (nonatomic) NSInteger currentIndex;
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (strong, nonatomic) NSArray *menuItems;

@end

@implementation CNCMenuViewController

typedef NS_ENUM(NSUInteger, CNCMenuItem){
	
#ifdef Retail
	CNCMenuSectionHome,
	CNCMenuSectionNewQuote,
    CNCMenuSectionPendingQuotes,
    CNCMenuSectionWatchesToShip,
    CNCMenuSectionHistoryInfo,
	CNCMenuSectionTerms,
	CNCMenuSectionContacts,
    CNCMenuSectionSignOut
#else
	CNCMenuSectionHome,
	CNCMenuSectionNewQuote,
	CNCMenuSectionBlog,
	CNCMenuSectionHow,
	CNCMenuSectionTerms,
	CNCMenuSectionPrivacy,
	CNCMenuSectionContacts
#endif
	
};


- (void)viewDidLoad
{
  [super viewDidLoad];
  self.tableView.tableFooterView = self.footerView;
#ifdef Retail
    if ([CNCAppOptions sharedInstance].appSize == AppSizeRep)
    {
        [self.menuDelegate changeToRepRetailMenu];
    } else {
        [self.menuDelegate changeToNormalRetailMenu];
    }
//    self.menuItems = retailMenu;
#else
    self.menuItems = nonretailMenu;
#endif
    
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  
}

#ifdef Retail
- (void)changeToNormalRetailMenu
{
    self.menuItems = retailMenu;
    [self.tableView reloadData];
}

- (void)changeToRepRetailMenu
{
    self.menuItems = retailRepMenu;
    [self.tableView reloadData];
}
#endif

#pragma mark - Table view data source

- (NSInteger) tableView: (UITableView*) tableView numberOfRowsInSection: (NSInteger) section
{
	return [self.menuItems count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
  return self.sectionHeader;
}

- (void) tableView: (UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  self.currentIndex = indexPath.row;
  [self.tableView reloadData];
  
    if ([self.menuDelegate respondsToSelector:@selector(didSelectWelcomeItem)] && indexPath.row == CNCMenuSectionHome) {
        [self.menuDelegate didSelectWelcomeItem];
    }
#ifdef Retail
    if ([self.menuDelegate respondsToSelector:@selector(didSelectNewQuoteItem)] && indexPath.row == CNCMenuSectionNewQuote) {
        [self.menuDelegate didSelectNewQuoteItem];
    }
    else if ([self.menuDelegate respondsToSelector:@selector(didSelectPendingQuotesItem)] && indexPath.row == CNCMenuSectionPendingQuotes) {
        [self.menuDelegate didSelectPendingQuotesItem];
    }
    else if ([self.menuDelegate respondsToSelector:@selector(didSelectWatchesToShipItem)] && indexPath.row == CNCMenuSectionWatchesToShip
             && ([CNCAppOptions sharedInstance].appSize != AppSizeRep)) {
        [self.menuDelegate didSelectWatchesToShipItem];
    }
    else if ([self.menuDelegate respondsToSelector:@selector(didSelectTermsAndConditionsItem)] && indexPath.row == 3
             && ([CNCAppOptions sharedInstance].appSize == AppSizeRep)) {
        [self.menuDelegate didSelectTermsAndConditionsItem];
    }
    else if ([self.menuDelegate respondsToSelector:@selector(didSelectHistoryInfoItem)] && indexPath.row == CNCMenuSectionHistoryInfo
             && ([CNCAppOptions sharedInstance].appSize != AppSizeRep)) {
        [self.menuDelegate didSelectHistoryInfoItem];
    }
    else if ([self.menuDelegate respondsToSelector:@selector(didSelectContactsItem)] && indexPath.row == 4
             && ([CNCAppOptions sharedInstance].appSize == AppSizeRep)) {
        [self.menuDelegate didSelectContactsItem];
    }
    else if ([self.menuDelegate respondsToSelector:@selector(didSelectTermsAndConditionsItem)] && indexPath.row == CNCMenuSectionTerms
             && ([CNCAppOptions sharedInstance].appSize != AppSizeRep)) {
        [self.menuDelegate didSelectTermsAndConditionsItem];
    }
    else if ([self.menuDelegate respondsToSelector:@selector(didSelectSignOutItem)] && indexPath.row == 5
             && ([CNCAppOptions sharedInstance].appSize == AppSizeRep)) {
        [self.menuDelegate didSelectSignOutItem];
    }
    else if ([self.menuDelegate respondsToSelector:@selector(didSelectContactsItem)] && indexPath.row == CNCMenuSectionContacts
             && ([CNCAppOptions sharedInstance].appSize != AppSizeRep)) {
        [self.menuDelegate didSelectContactsItem];
    }
    else if ([self.menuDelegate respondsToSelector:@selector(didSelectSignOutItem)] && indexPath.row == CNCMenuSectionSignOut
             && ([CNCAppOptions sharedInstance].appSize != AppSizeRep)) {
        [self.menuDelegate didSelectSignOutItem];
    }

#else
    if ([self.menuDelegate respondsToSelector:@selector(didSelectNewQuoteItem)] && indexPath.row == CNCMenuSectionNewQuote) {
        [self.menuDelegate didSelectNewQuoteItem];
    }
    else if ([self.menuDelegate respondsToSelector:@selector(didSelectHowItem)] && indexPath.row == CNCMenuSectionHow) {
        [self.menuDelegate didSelectHowItem];
    }
    else if ([self.menuDelegate respondsToSelector:@selector(didSelectBlogItem)] && indexPath.row == CNCMenuSectionBlog) {
        [self.menuDelegate didSelectBlogItem];
    }
    else if ([self.menuDelegate respondsToSelector:@selector(didSelectPrivacyPolicyItem)] && indexPath.row == CNCMenuSectionPrivacy) {
        [self.menuDelegate didSelectPrivacyPolicyItem];
    }
    else if ([self.menuDelegate respondsToSelector:@selector(didSelectContactsItem)] && indexPath.row == CNCMenuSectionContacts) {
        [self.menuDelegate didSelectContactsItem];
    }
    
    else if ([self.menuDelegate respondsToSelector:@selector(didSelectTermsAndConditionsItem)] && indexPath.row == CNCMenuSectionTerms) {
        [self.menuDelegate didSelectTermsAndConditionsItem];
    }
#endif

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString* cellIdentifier  = @"customCell";
	CNCMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: cellIdentifier forIndexPath: indexPath];

	NSArray* sourceArray = _menuItems;

	[cell configureCellWithDictionary: sourceArray[indexPath.row]
						 isSelected: self.currentIndex == indexPath.row
						isFirstItem: indexPath.row == CNCMenuSectionNewQuote];
    
	return cell;
  
}

@end
