//
//  CNCFirstStepViewController.m
//  Crown&Caliber
//
//  Created by valerio8 on 09.07.14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//


#import "CNCFirstStepViewController.h"
#import <BlocksKit/UIActionSheet+BlocksKit.h>
#import <BlocksKit/UIImagePickerController+BlocksKit.h>
#import "CNCQuote.h"

@interface CNCFirstStepViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *frontImageView;
@property (weak, nonatomic) IBOutlet UIImageView *backImageView;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (nonatomic) BOOL isFirstTimeLoaded;



@end

@implementation CNCFirstStepViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.isFirstTimeLoaded = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)nextButtonPressed:(id)sender
{
    [self.delegate goToSecondStepWithSuccess:YES];
    [self.view endEditing:YES];
}

- (IBAction)imageFrontAction:(id)sender {
    [self pickImage:^(UIImage *resultImage) {
        self.currentQuote.imageFront = resultImage;
        self.frontImageView.image = resultImage;
        
        [self checkNextButtonAvailability:self.currentQuote];
    }];
}

- (IBAction)imageBackAction:(id)sender {
    [self pickImage:^(UIImage *resultImage) {
        self.currentQuote.imageBack = resultImage;
        self.backImageView.image = resultImage;
        [self checkNextButtonAvailability:self.currentQuote];
    }];
}

- (void)checkNextButtonAvailability:(CNCQuote*)quote {
    if (self.frontImageView.image && self.backImageView.image) {
        self.nextButton.enabled = YES;
        [self.delegate enableInformationButton ];
        if (self.isFirstTimeLoaded) {
            self.isFirstTimeLoaded = NO;
            [self.delegate goToSecondStepWithSuccess:YES];
        }
        
    }
}

- (void)pickImage:(void (^)(UIImage *resultImage))success {
    
    UIActionSheet *imagePickAction = [UIActionSheet bk_actionSheetWithTitle:nil];
    [imagePickAction bk_addButtonWithTitle:@"Camera" handler:^{
        
        UIImagePickerController* imagePickerController =  [[UIImagePickerController alloc] init];
        [imagePickerController  setSourceType:UIImagePickerControllerSourceTypeCamera];
        imagePickerController.delegate = self;
        
        [imagePickerController setBk_didFinishPickingMediaBlock:^(UIImagePickerController *picker, NSDictionary *info) {
            
            UIImage* resultImage = [info objectForKey:UIImagePickerControllerOriginalImage];
            [picker dismissViewControllerAnimated:YES completion:nil];
            
            if (success) {
                success(resultImage);
            }
            
        }];
        
        [[self.delegate tabBarController] presentViewController:imagePickerController animated:YES completion:NULL];
    }];
    
    [imagePickAction bk_addButtonWithTitle:@"Library" handler:^{
        UIImagePickerController* imagePickerController =  [[UIImagePickerController alloc] init];
        [imagePickerController  setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        
        imagePickerController.delegate = self;
        [imagePickerController setBk_didFinishPickingMediaBlock:^(UIImagePickerController *picker, NSDictionary *info) {
            //TODO: big images?
            
            UIImage* resultImage = [info objectForKey:UIImagePickerControllerOriginalImage];
            [picker dismissViewControllerAnimated:YES completion:nil];
            
            if (success) {
                success(resultImage);
            }
        }];
        
        [[self.delegate tabBarController] presentViewController:imagePickerController animated:YES completion:NULL];
        
    }];
    
    [imagePickAction bk_setCancelButtonWithTitle:@"Cancel" handler:nil];
    
    
    [imagePickAction showInView:[[self.delegate tabBarController] view]];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [[self.delegate tabBarController] dismissViewControllerAnimated:YES completion:nil];
}

-(void) deleteUserData
{
    self.frontImageView.image = nil;
    self.backImageView.image = nil;
    self.nextButton.enabled = NO;
    self.isFirstTimeLoaded = YES;
    
}

@end
