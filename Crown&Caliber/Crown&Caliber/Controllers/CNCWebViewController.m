//
//  CNCWebViewController.m
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCWebViewController.h"
#import <MBProgressHUD.h>


@interface CNCWebViewController () <UIWebViewDelegate>

//@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *goBackButton;
@property (weak, nonatomic) IBOutlet UIButton *goForwardButton;
@property (nonatomic) float webViewOriginY;
@property (nonatomic) float webViewHeight;
@property (nonatomic, strong) NSString *url;

@end

@implementation CNCWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    if (self.url != nil) {
        [self loadPageFromString:self.url];
    }
    
    
    self.webView.scalesPageToFit = YES;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadPageFromString:(NSString *)urlString {
    if (self.webView != nil) {
        [self.webView removeFromSuperview];
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.webViewOriginY = 110;
        self.webViewHeight = self.view.frame.size.height  - self.webViewOriginY;
    }
    else
    {
        self.webViewOriginY = 64;
        self.webViewHeight = self.view.frame.size.height  - self.webViewOriginY - 50;
    }
    
    
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, self.webViewOriginY, self.view.frame.size.width, self.webViewHeight)];
    self.webView.delegate = self;
    self.webView.scalesPageToFit = YES;
    [self.view addSubview:self.webView];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
    
    if (self.isViewLoaded && self.view.window) {
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
        self.url = nil;
    }
    else
    {
        self.url = urlString;
    }
    
    
}

-(void)loadPageFromHtml:(NSString *)htmlString {
    if (self.webView != nil) {
        [self.webView removeFromSuperview];
    }

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.webViewOriginY = 110;
        self.webViewHeight = self.view.frame.size.height  - self.webViewOriginY;
    }
    else
    {
        self.webViewOriginY = 64;
        self.webViewHeight = self.view.frame.size.height  - self.webViewOriginY - 50;
    }

    self.url = nil;
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, self.webViewOriginY, self.view.frame.size.width, self.webViewHeight)];
    self.webView.delegate = self;
    self.webView.scalesPageToFit = YES;
    [self.view addSubview:self.webView];
    [self.webView loadHTMLString:htmlString baseURL:nil];

    if (self.isViewLoaded && self.view.window) {
        [self.webView loadHTMLString:htmlString baseURL:nil];
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [MBProgressHUD showHUDAddedTo:self.webView animated:YES];
    
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideAllHUDsForView:self.webView animated:YES];
    [self checkButtonsAvailability];
}

- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.webView animated:YES];
    [self checkButtonsAvailability];
}

-(IBAction)goBack:(id)sender {
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    }
}

-(IBAction)goForward:(id)sender {
    if ([self.webView canGoForward]) {
        [self.webView goForward];
    }
}

-(void)checkButtonsAvailability
{
    if ([self.webView canGoBack]) {
        [self.goBackButton setEnabled:YES];
    }
    else
    {
        [self.goBackButton setEnabled:NO];
    }
    
    if ([self.webView canGoForward]) {
        [self.goForwardButton setEnabled:YES];
    }
    else
    {
        [self.goForwardButton setEnabled:NO];
    }
}

@end
