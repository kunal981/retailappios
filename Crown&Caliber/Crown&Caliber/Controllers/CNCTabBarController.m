//
//  CNCTabBarController.m
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCTabBarController.h"
#import "CNCWebViewController.h"
#import "CNCWelcomeController.h"
#import "CNCCreateQuoteViewController.h"
#import "CNCConfig.h"
#import "CNCAppOptions.h"
#import "UIColor+CNCColor.h"

typedef enum {
  CNCQuoteViewControllerItem = 0,
  CNCHowControllerItem,
  CNCWebControllerItem,
  CNCWelcomeViewControllerItem,
  CNCSplashViewControllerItem,
  CNCContactsViewControllerItem
} CNCViewControllerItems;

@interface CNCTabBarController () <UIGestureRecognizerDelegate>

{
    UITapGestureRecognizer *tap;
}

@property (nonatomic, strong) CNCWebViewController *webViewController;
@property (nonatomic, strong) CNCWelcomeController *welcomeController;
@property (strong, nonatomic) IBOutlet UIView *tabControlsFooter;

@property (nonatomic) BOOL moreButtonIsPressed;
@property (nonatomic) BOOL buttonsMoveFinished;
@property (nonatomic) NSInteger previousViewControllerIndex;

@end

@implementation CNCTabBarController

- (void)viewDidLoad
{
  [super viewDidLoad];
  self.tabBar.hidden = YES;
  
  self.customizableViewControllers = nil;
  
  self.webViewController = [self.viewControllers objectAtIndex:CNCWebControllerItem];
  self.welcomeController = [self.viewControllers objectAtIndex:CNCWelcomeViewControllerItem];
  
  self.buttonsMoveFinished = YES;
  self.moreButtonIsPressed = NO;
  [self.welcomeController loadWelcomeScreen];
  self.previousViewControllerIndex = self.selectedIndex;
  
  [self.view addSubview:self.tabControlsFooter];
  self.tabControlsFooter.frame = CGRectMake(0, self.view.frame.size.height - 49, self.view.frame.size.width, 98);
  self.view.backgroundColor = [UIColor CNCGrayColor];
  
  
  tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(transitionViewTap)];
  tap.numberOfTapsRequired = 1;
  tap.delegate = self;
  
  
  [self setSelectedViewController:[self.viewControllers objectAtIndex:CNCSplashViewControllerItem]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)blogButtonPressed:(id)sender
{
    [self deselectButtons];
    [(UIButton*)sender setSelected:YES];
    self.selectedIndex = CNCWebControllerItem;
    [self.webViewController loadPageFromString:kCNCBlogURL];
    if (self.moreButtonIsPressed) {
        [self hideAdditionalButtons];
    }
    
    self.previousViewControllerIndex = self.selectedIndex;
}

- (IBAction)howButtonPressed:(id)sender
{
    [self deselectButtons];
    self.selectedIndex = CNCHowControllerItem;
    [(UIButton*)sender setSelected:YES];
    if (self.moreButtonIsPressed) {
        [self hideAdditionalButtons];
    }
    self.previousViewControllerIndex = self.selectedIndex;

}

- (IBAction) moreButtonPressed:(id)sender
{
    [self deselectButtons];
    [(UIButton*)sender setSelected:!self.moreButtonIsPressed];
    if (self.moreButtonIsPressed) {
        [self hideAdditionalButtons];
    }
    else
    {
        [self showAdditionalButtons];
    }

}


- (IBAction) addNewQuoteButtonPressed:(id)sender
{
    [self deselectButtons];
    self.selectedIndex = CNCQuoteViewControllerItem;
    if (self.moreButtonIsPressed) {
        [self hideAdditionalButtons];
    }
    self.previousViewControllerIndex = self.selectedIndex;
    [self.welcomeController loadSuccessScreen];
    CNCCreateQuoteViewController* controller = (CNCCreateQuoteViewController*)self.selectedViewController;
    [controller createNewQuote];
}

- (IBAction)welcomeButtonPressed:(id)sender {
  [self.welcomeController loadWelcomeScreen];
  [self setSelectedViewController:self.welcomeController];

  [self hideAdditionalButtons];
  [self deselectButtons];
  self.previousViewControllerIndex = self.selectedIndex;

}

- (IBAction) privacyButtonPressed:(id)sender
{
    self.selectedIndex = CNCWebControllerItem;
    [self.webViewController loadPageFromString:kCNCPrivacyURL];
    [self hideAdditionalButtons];
    [self deselectButtons];
    self.previousViewControllerIndex = self.selectedIndex;
    
}

-(IBAction) termsButtonPressed:(id)sender
{
    self.selectedIndex = CNCWebControllerItem;
    [self.webViewController loadPageFromHtml: [CNCAppOptions sharedInstance].terms];
    [self hideAdditionalButtons];
    [self deselectButtons];
    self.previousViewControllerIndex = self.selectedIndex;
}

- (IBAction)contactsButtonPressed:(id)sender {
  self.selectedIndex = CNCContactsViewControllerItem;
  
  [self hideAdditionalButtons];
  [self deselectButtons];
  
  self.previousViewControllerIndex = self.selectedIndex;
}

- (void) hideAdditionalButtons
{
    
    UIView* transitionView = self.view.subviews[0];
    UIView *viewToResize = [[transitionView.subviews[0] subviews] objectAtIndex:0];
    
    CGRect newFrame = viewToResize.frame;
    
    if (self.selectedIndex == self.previousViewControllerIndex ) {
        newFrame.origin.y += self.tabControlsFooter.frame.size.height/2;
    }
    
    
    self.buttonsMoveFinished = NO;
    [UIView animateWithDuration:0.5 animations:^{
        viewToResize.frame = newFrame;
        self.tabControlsFooter.frame = CGRectMake(0, self.view.frame.size.height - self.tabControlsFooter.frame.size.height/2, self.view.frame.size.width, self.tabControlsFooter.frame.size.height) ;
    } completion:^(BOOL finished) {
        self.moreButtonIsPressed = NO;
        self.buttonsMoveFinished = YES;
    }];
    

    for (UIGestureRecognizer *recognizer in self.view.gestureRecognizers) {
        [self.view removeGestureRecognizer:recognizer];
    }
}

-(void) showAdditionalButtons
{
    UIView* transitionView = self.view.subviews[0];
    UIView *viewToResize = [[transitionView.subviews[0] subviews] objectAtIndex:0];
    
    CGRect newFrame = viewToResize.frame;
    
    
    
    if (self.selectedIndex == self.previousViewControllerIndex ) {
        newFrame.origin.y -= self.tabControlsFooter.frame.size.height/2;
    }
    
    
    
    self.buttonsMoveFinished = NO;
    [UIView animateWithDuration:0.5 animations:^{
        viewToResize.frame = newFrame;
        
        self.tabControlsFooter.frame = CGRectMake(0, self.view.frame.size.height - self.tabControlsFooter.frame.size.height, self.view.frame.size.width, self.tabControlsFooter.frame.size.height) ;
    } completion:^(BOOL finished) {
        self.moreButtonIsPressed = YES;
        self.buttonsMoveFinished = YES;
    }];
    
    [transitionView addGestureRecognizer:tap];
    
}

-(void) deselectButtons
{
    for (UIButton *button in self.tabControlsFooter.subviews) {
        [button setSelected:NO];
    }
    
}


- (void) transitionViewTap
{
    if (self.moreButtonIsPressed && self.buttonsMoveFinished)
    {
        [self hideAdditionalButtons];
    }
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

-(void) hideFooter:(BOOL)hide
{
    [self.tabControlsFooter setHidden:hide];
}

//-(void)setSelectedViewController:(UIViewController *)selectedViewController
//{
//  [super setSelectedViewController:selectedViewController];
//  
//  if([self.moreNavigationController.viewControllers count] > 1)
//  {
//    //Modify the view stack to remove the More view
//    self.moreNavigationController.viewControllers = [[NSArray alloc] initWithObjects:self.moreNavigationController.visibleViewController, nil];
//  }
//}

-(NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
@end
