//
//  CNCSplashViewController.m
//  Crown&Caliber
//
//  Created by valerio8 on 22.07.14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCSplashViewController.h"
#import "CNCWelcomeController.h"
#import "CNCTabBarController.h"
#import "CNCApi.h"
#import "CNCSettings.h"

#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)

@interface CNCSplashViewController ()

@property (nonatomic, strong) IBOutlet UIImageView *imageView;

@end

@implementation CNCSplashViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
  
    self.navigationController.navigationBarHidden = YES;
    self.imageView.image = [self backgroundImage];


//	[[CNCApi sharedClient] pendingWatches: [CNCSettings userId] successBlock:^(NSArray *watches, NSError *error) {
//
//	}];
//
//	[[CNCApi sharedClient] quotedWatches: [CNCSettings userId] successBlock:^(NSArray *watches, NSError *error) {
//
//	}];
//
//	[[CNCApi sharedClient] watchesToShip: [CNCSettings userId] successBlock:^(NSArray *watches, NSError *error) {
//
//	}];
//
//	[[CNCApi sharedClient] historyInfo: [CNCSettings userId] successBlock:^(NSArray *watches, NSError *error) {
//
//	}];
//
//	[[CNCApi sharedClient] watchCaseInfo: @"9311" successBlock:^(CNCCase* aCase, NSError *error) {
//
//	}];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CNCTabBarController *tabController = (CNCTabBarController*)self.tabBarController;
        CNCWelcomeController *controller = tabController.viewControllers[3];
        [tabController hideFooter:YES];
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.tabBarController.selectedIndex = 3;
            [tabController hideFooter:NO];
            [controller loadWelcomeScreen];
        });
    }
    else{
        [self.navigationController setNavigationBarHidden:YES];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController setNavigationBarHidden:NO];
            [self.menuDelegate loadWelcomeScreen];
        });
    }

}

- (UIImage *)backgroundImage
{
    NSString *resource;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        resource = IS_IPHONE5 ? @"LaunchImage-700-568h" : @"LaunchImage-700";
    } else {
        resource = @"LaunchImage-700-Portrait";
    }
    return [UIImage imageNamed:resource];
}

@end
