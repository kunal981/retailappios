//
//  CNCContactsController.m
//  Crown&Caliber
//
//  Created by Igor Tomych on 29/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCContactsController.h"
#import "CNCAppOptions.h"

@interface CNCContactsController ()

@property (nonatomic, strong) IBOutlet UITextView* emailLabel;
@property (nonatomic, strong) IBOutlet UITextView* phoneLabel;


@end

@implementation CNCContactsController

- (void)viewDidLoad
{
  [super viewDidLoad];
    self.title = @"Contacts";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([CNCAppOptions sharedInstance].appSize == AppSizeRep)
    {
        _emailLabel.text = @"";
        _phoneLabel.text = @"Phone: (404) 812-9928";
    } else{
        _emailLabel.text = @"Email:\nbnollner@crownandcaliber.com";
        _phoneLabel.text = @"Phone:\n(800) 514-3750";
    }

}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];

}


@end
