//
//  CNCSplashViewController.h
//  Crown&Caliber
//
//  Created by valerio8 on 22.07.14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNCMenuViewController.h"

@interface CNCSplashViewController : UIViewController

@property (nonatomic, strong) id <CNCMenuDelegate> menuDelegate;
@end
