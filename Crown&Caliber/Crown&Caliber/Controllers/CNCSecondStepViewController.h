//
//  CNCSecondStepViewController.h
//  Crown&Caliber
//
//  Created by valerio8 on 09.07.14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNCQuote.h"
@protocol CNCSecondStepDelegate;
@interface CNCSecondStepViewController : UIViewController
@property (nonatomic,strong) id<CNCSecondStepDelegate> delegate;
@property (strong,nonatomic) CNCQuote *currentQuote;

-(void) deleteUserData;

@end
@protocol CNCSecondStepDelegate <NSObject>
-(void) goToThirdStep;
-(void) disableSubmitButton;
-(void) enableSubmitButton;
-(UIView*)view;
@end
