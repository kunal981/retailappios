//
//  CNCWatchesViewController.m
//  Crown&Caliber
//

#import "CNCWatchesViewController.h"
#import "CNCWatch.h"
#import "CNCWatchesCell.h"
#import "MBProgressHUD.h"
#import "CNCApi.h"
#import "CNCSettings.h"
#import "CNCFooterView.h"
#import "UIImageView+AFNetworking.h"
#import "SHSPhoneNumberFormatter.h"
#import "SHSPhoneNumberFormatter+UserConfig.h"
#import "CNCImageViewerViewController.h"


@interface CNCWatchesViewController()

@property (nonatomic, strong) NSArray* watches;

@property (nonatomic, strong) NSDateFormatter* dateFormatter;
@property (nonatomic, strong) NSDateFormatter* timeFormatter;
@property (nonatomic, strong) SHSPhoneNumberFormatter* phoneFormatter;
@property (nonatomic, strong) UIRefreshControl* refreshControl;
@property (nonatomic, strong) IBOutlet UILabel* headerLabel;
@property (nonatomic) BOOL returningFromSecondaryView;

@property (nonatomic, strong) NSDictionary *systemDict;
@property (nonatomic, strong) NSDictionary *systemBoldDict;
@property (nonatomic, strong) NSDictionary *systemBoldItalicDict;


@end

@implementation CNCWatchesViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
	self = [super initWithCoder:coder];
	if (self) {
		// Custom initialization
		self.dateFormatter = [[NSDateFormatter alloc] init];
		[self.dateFormatter setDateStyle: NSDateFormatterShortStyle];

		self.timeFormatter = [[NSDateFormatter alloc] init];
		[self.timeFormatter setTimeStyle: NSDateFormatterShortStyle];
        
        self.phoneFormatter = [[SHSPhoneNumberFormatter alloc] init];
        [self.phoneFormatter setDefaultOutputPattern:@"+# (###) ###-####"];
	}
	return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.watchesTableView addSubview: self.refreshControl];
    [self.refreshControl addTarget: self action: @selector(refreshTable) forControlEvents: UIControlEventValueChanged];
    
    NSString *labelText1 = @"Below is a comprehensive list of all quote requests which have been accepted. Please ship the watches to Crown & Caliber within 72 hours. Once the watch has been sent by you, please check the “Shipped” box. Once we have received the watch, it will no longer appear on this section.";
    
    NSString *labelText2 = @"\n\nPlease ship the following watches to:\n\nC&C\n3565 Piedmont Rd, Building 2, Ste 720\nAtlanta, GA 30305";
    NSString *tableInstText = @"  Swipe down to automatically refresh screen.";

    UIFont *systemFontNormal = [UIFont systemFontOfSize:17.0];
    UIFont *systemFontBold = [UIFont boldSystemFontOfSize:17.0];
    UIFont *systemFontBoldItalic = [UIFont fontWithName:@"HelveticaNeue-BoldItalic" size:17.0];
    self.systemDict = [NSDictionary dictionaryWithObject: systemFontNormal forKey:NSFontAttributeName];
    self.systemBoldDict = [NSDictionary dictionaryWithObject:systemFontBold forKey:NSFontAttributeName];
    self.systemBoldItalicDict = [NSDictionary dictionaryWithObject:systemFontBoldItalic forKey:NSFontAttributeName];
    
    NSMutableAttributedString *lAttrString1 = [[NSMutableAttributedString alloc] initWithString:labelText1 attributes: self.systemDict];
    NSMutableAttributedString *lAttrString2 = [[NSMutableAttributedString alloc] initWithString:labelText2 attributes: self.systemDict];
    
    NSMutableAttributedString *iAttrString = [[NSMutableAttributedString alloc]initWithString: tableInstText attributes:self.systemBoldItalicDict];
    
    [lAttrString1 appendAttributedString:iAttrString];
    [lAttrString1 appendAttributedString:lAttrString2];
    self.headerLabel.attributedText = lAttrString1;
    
    self.returningFromSecondaryView = NO;
    
    [self.watchesTableView.layer setBorderWidth:1.0];
    [self.watchesTableView.layer setBorderColor:[[UIColor colorWithWhite: 0.8 alpha: 1.0] CGColor]];
    

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUserSignout:)
                                                 name:@"userSignout"
                                               object:nil];
}


#pragma mark - Notifications

- (void)handleUserSignout:(NSNotification *)note {
    self.watches = nil;
    [self.watchesTableView reloadData];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void) refreshTable
{
    [MBProgressHUD showHUDAddedTo:self.parentViewController.view animated:YES];

    [[CNCApi sharedClient] watchesToShip: [CNCSettings userId]
                            successBlock: ^(NSArray *watches, NSError *error, BOOL redirect)
     {
         [MBProgressHUD hideAllHUDsForView: self.parentViewController.view animated: YES];
         
         if (redirect)
         {
             [[NSNotificationCenter defaultCenter] postNotificationName:@"requestSignout" object:self];
             return;
         }
         
         self.watches = watches;

         [self.refreshControl endRefreshing];
         [self.watchesTableView reloadData];
         
         const CGFloat rowsHeight = self.watches.count * self.watchesTableView.rowHeight;
         
         if (rowsHeight > CGRectGetHeight(self.watchesTableView.frame) - StaticFooterHeight)
         {
             const CGFloat footerHeight = (rowsHeight > CGRectGetHeight(self.watchesTableView.frame)) ? 100.0f : CGRectGetHeight(self.watchesTableView.frame) - rowsHeight;
             CNCFooterView* footerView = [[CNCFooterView alloc] initWithFrame: CGRectMake(0.0f, 0.0f, 0.0f, footerHeight)];
             self.watchesTableView.tableFooterView = footerView;
             self.footerView.hidden = YES;
         }
         else
         {
             self.watchesTableView.tableFooterView = nil;
             self.footerView.hidden = NO;
         }
     }];
}

- (void) viewDidAppear: (BOOL) animated
{
    [super viewDidAppear: animated];

    if (self.returningFromSecondaryView)
    {
        self.returningFromSecondaryView = NO;
    } else {
        [self refreshTable];
    }
}

- (void) displayAlert: (NSString*) message withTitle: (NSString*) title
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle: title
                                                    message: message
                                                   delegate: nil
                                          cancelButtonTitle: @"OK"
                                          otherButtonTitles: nil];
    [alert show];
}

#pragma mark - Events

- (void) onShipPressed: (UIButton*) sender
{
    const NSInteger selectedRow = sender.tag - 1;
    if ((selectedRow >= 0) && (selectedRow < self.watches.count))
    {
        CNCQuote* watch = self.watches[selectedRow];

        [MBProgressHUD showHUDAddedTo:self.parentViewController.view animated:YES];

        [[CNCApi sharedClient] shipWatch: watch.caseId
                            successBlock: ^(NSError* error, BOOL redirect)
         {
             if (redirect)
             {
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"requestSignout" object:self];
                 return;
             }

             if (error == nil)
             {
                 [[CNCApi sharedClient] watchesToShip: [CNCSettings userId]
                                         successBlock: ^(NSArray *watches, NSError *error, BOOL redirect)
                  {
                      [MBProgressHUD hideAllHUDsForView: self.parentViewController.view animated: YES];

                      if (redirect)
                      {
                          [[NSNotificationCenter defaultCenter] postNotificationName:@"requestSignout" object:self];
                          return;
                      }

                      self.watches = watches;
                      [self.watchesTableView reloadData];
                  }];
             }
             else
             {
                 [MBProgressHUD hideAllHUDsForView: self.parentViewController.view animated: YES];

                 [self displayAlert: error.localizedDescription withTitle: @"Error"];
             }
         }];
    }
}

- (IBAction) onImagePressed: (UIButton*) sender
{
    const NSInteger selectedIndex = sender.tag;
    CNCQuote *watch = self.watches[selectedIndex];
    
    if (watch.imageURL.length > 0)
    {
        CNCImageViewerViewController *imageVC = [[CNCImageViewerViewController alloc] init];
        [imageVC setImageUrl:watch.imageURL];
        self.returningFromSecondaryView = YES;
        [self presentViewController:imageVC animated:YES completion:nil];
    }
}

- (IBAction) onImage2Pressed: (UIButton*) sender
{
    const NSInteger selectedIndex = sender.tag;
    CNCQuote *watch = self.watches[selectedIndex];
    
    if (watch.image2URL.length > 0)
    {
        CNCImageViewerViewController *imageVC = [[CNCImageViewerViewController alloc] init];
        [imageVC setImageUrl:watch.image2URL];
        self.returningFromSecondaryView = YES;
        [self presentViewController:imageVC animated:YES completion:nil];
    }
}


- (NSAttributedString *) makeAttributedStringWithLabel: (NSString *)labelString value:(NSString *)valueString {
    
    NSMutableAttributedString *lAttrString;
    
    lAttrString = [[NSMutableAttributedString alloc] initWithString:labelString attributes: self.systemBoldDict];
    if (valueString) {
        [lAttrString appendAttributedString: [[NSAttributedString alloc] initWithString:valueString attributes:self.systemDict]];
    }
    return lAttrString;
    
}


#pragma mark - UITableViewDataSource

- (NSInteger) tableView: (UITableView*) tableView numberOfRowsInSection: (NSInteger) section
{
    // Return 1 for empty list
    return MAX(1, self.watches.count);
}

- (UITableViewCell*) tableView: (UITableView*) tableView cellForRowAtIndexPath: (NSIndexPath*) indexPath
{
    const BOOL watchesExist = self.watches.count > 0;
    NSString* identifier = watchesExist ?  @"watch" : @"emptyCell";
    UITableViewCell* cellGeneral = [tableView dequeueReusableCellWithIdentifier: identifier];

    if (watchesExist)
    {
        CNCWatchesCell* cell = (CNCWatchesCell*)cellGeneral;
        CNCQuote* watch = self.watches[indexPath.row];

        if (watch.shipped) {    
            cell.markedShippedLabel.hidden = YES;
            cell.shippedButton.hidden = YES;
            cell.shippedDateLabel.hidden = NO;
            cell.trackingNumberLabel.hidden = NO;
            [cell.shippedButton setBackgroundImage: [UIImage imageNamed: @"checked-checkbox.png"] forState: UIControlStateNormal];
        } else {
            cell.markedShippedLabel.hidden = NO;
            cell.shippedButton.hidden = NO;
            cell.shippedDateLabel.hidden = YES;
            cell.trackingNumberLabel.hidden = YES;
            [cell.shippedButton setBackgroundImage: [UIImage imageNamed: @"unchecked-checkbox.png"] forState: UIControlStateNormal];
        }
        
        cell.nameLabel.attributedText = [self makeAttributedStringWithLabel:@"Name: " value:watch.name];
        cell.associateNameLabel.attributedText = [self makeAttributedStringWithLabel:@"Associate: " value:watch.retailerId];
        cell.brandLabel.attributedText = [self makeAttributedStringWithLabel:@"Brand: " value:watch.brand];
        cell.markedShippedLabel.attributedText = [self makeAttributedStringWithLabel:@"Mark Shipped: " value:nil];
        cell.statusLabel.attributedText = [self makeAttributedStringWithLabel:@"Status: " value:watch.status];
        cell.trackingNumberLabel.attributedText = [self makeAttributedStringWithLabel:@"Tracking Number: " value:watch.shippingLabel];
        if (watch.phone)
        {
            NSDictionary *phoneDict = [self.phoneFormatter valuesForString:watch.phone];
            cell.phoneLabel.attributedText = [self makeAttributedStringWithLabel:@"Phone: " value:phoneDict[@"text"]];
            
        } else {
            cell.phoneLabel.attributedText = [self makeAttributedStringWithLabel:@"Phone: " value:nil];
        }
        cell.caseIdLabel.attributedText = [self makeAttributedStringWithLabel:@"Case #: " value:watch.caseId];


        if (![watch.cashPrice isEqual: [NSNull null]])
        {
            cell.amountLabel.attributedText = [self makeAttributedStringWithLabel:@"Insured Amount: " value:[NSString stringWithFormat: @"$%@", [watch.cashPrice stringValue]]];
        }
        else
        {
            cell.amountLabel.attributedText = [self makeAttributedStringWithLabel:@"Insured Amount: " value:@"-"];
        }
        
        
        NSString* dateString;
        if (watch.createdAt) {
            dateString = [NSString stringWithFormat: @"%@ %@",
                          [self.dateFormatter stringFromDate: watch.createdAt],
                          [self.timeFormatter stringFromDate: watch.createdAt]];
        } else {
            dateString = nil;
        }
        cell.createdDateLabel.attributedText = [self makeAttributedStringWithLabel:@"Case Created: " value:dateString];
        
        if (watch.acceptedAt) {
            dateString = [NSString stringWithFormat: @"%@ %@",
                          [self.dateFormatter stringFromDate: watch.acceptedAt],
                          [self.timeFormatter stringFromDate: watch.acceptedAt]];
        } else {
            dateString = nil;
        }
        cell.acceptedDateLabel.attributedText = [self makeAttributedStringWithLabel:@"Offer Accepted: " value:dateString];
        
        if (watch.shippedAt) {
            dateString = [NSString stringWithFormat: @"%@ %@",
                          [self.dateFormatter stringFromDate: watch.shippedAt],
                          [self.timeFormatter stringFromDate: watch.shippedAt]];
        } else {
            dateString = nil;
        }
        cell.shippedDateLabel.attributedText = [self makeAttributedStringWithLabel:@"Shipped At: " value:dateString];

        cell.shippedButton.tag = indexPath.row + 1;
        [cell.shippedButton addTarget: self action: @selector(onShipPressed:) forControlEvents: UIControlEventTouchUpInside];
        
        
        cell.watchPhotoButton.hidden = YES;
        cell.watchPhoto.hidden = YES;
        if (watch.imageURL.length > 0)
        {
            NSURL* watchURL = [NSURL URLWithString: watch.imageURL];
            [cell.watchPhoto setImage:nil];
            [cell.watchPhoto setImageWithURL: watchURL];
            cell.watchPhotoButton.tag=indexPath.row;
            cell.watchPhotoButton.hidden = NO;
            cell.watchPhoto.hidden = NO;
        }
        
        cell.watchPhoto2Button.hidden = YES;
        cell.watchPhoto2.hidden = YES;
        if (watch.imageURL.length > 0)
        {
            NSURL* watchURL = [NSURL URLWithString: watch.image2URL];
            [cell.watchPhoto2 setImage:nil];
            [cell.watchPhoto2 setImageWithURL: watchURL];
            cell.watchPhoto2Button.tag=indexPath.row;
            cell.watchPhoto2Button.hidden = NO;
            cell.watchPhoto2.hidden = NO;
        }

    }

    return cellGeneral;
}

#pragma mark - UITableViewDelegate

@end
