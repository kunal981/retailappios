//
//  CNCWatchesViewController.h
//  Crown&Caliber
//

#import <UIKit/UIKit.h>
#import "CNCMenuViewController.h"
#import "CNCBaseViewController.h"

@interface CNCWatchesViewController: CNCBaseViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) id<CNCMenuDelegate> menuDelegate;
@property (nonatomic, strong) IBOutlet UITableView* watchesTableView;
@property (nonatomic, strong) IBOutlet UIView* footerView;

@end
