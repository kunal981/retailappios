//
//  CNCWelcomeController.m
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCWelcomeController.h"
#import "CNCApi.h"
#import <MBProgressHUD.h>
#import "CNCCreateQuoteViewController.h"
#import "CNCAppOptions.h"
#import "UIImageView+AFNetworking.h"

@interface CNCWelcomeController ()

@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UIImageView *logoImageView;
@property (nonatomic, weak) IBOutlet UIButton *button;
@property (nonatomic, weak) IBOutlet UILabel *textLabel;
@property (nonatomic, weak) IBOutlet UILabel *thankYouTextLabel;
@property (nonatomic) BOOL failScreenIsLoaded;
@property (nonatomic, weak) IBOutlet UIView* failLayerView;

@property (strong, nonatomic) IBOutlet UITextView *phoneTextView;
@property (strong, nonatomic) IBOutlet UITextView *emailTextView;
@property (strong, nonatomic) IBOutlet UITextView *webTextView;


@end

@implementation CNCWelcomeController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO];
	[self loadWelcomeScreen];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)loadWelcomeScreen
{
	if ([CNCAppOptions sharedInstance].screensaverURL.length > 0)
	{
		NSURL* screensaverURL = [NSURL URLWithString: [CNCAppOptions sharedInstance].screensaverURL];
		[self.imageView setImageWithURL: screensaverURL];
	}

    self.failScreenIsLoaded = NO;
    
    self.failLayerView.hidden = YES;

    for ( int tag = 100; tag < 105; tag++ ) { //FIXME: IPAD Dirty Hack. Outlets appear to be empty
        [[self.view viewWithTag: tag] setHidden:YES];
    }
}

- (void) loadSuccessScreen
{
    [self.logoImageView setHidden:YES];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
         self.imageView.image = [UIImage imageNamed:@"cover_NewQuote_ThankYou.png"];
    }
   
    self.textLabel.text = @" Our valuation team is now researching your quote request. We will be in touch with a quote momentarily.";
    [self.button setTitle:@"Get another Quote" forState:UIControlStateNormal];
    self.failScreenIsLoaded = NO;
    
    [self.thankYouTextLabel setHidden:NO];
    [self.phoneTextView setHidden:NO];
    [self.emailTextView setHidden:NO];
    [self.webTextView setHidden:NO];
    
    for ( int tag = 100; tag < 105; tag++ ) { //FIXME: IPAD Dirty Hack. Outlets appear to be empty
        [[self.view viewWithTag: tag] setHidden:NO];
    }
}

- (void) loadFailScreen
{
    [self.logoImageView setHidden:YES];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.imageView.image = [UIImage imageNamed:@"cover_NewQuote_ThankYou.png"];
    }
    self.failLayerView.hidden = NO;
    self.failScreenIsLoaded = YES;
}

-(IBAction)createNewQuoteButtonPressed:(id)sender
{
    if (self.failScreenIsLoaded) {
        __weak CNCWelcomeController *weakSelf = self;
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[CNCApi sharedClient] createQuote:self.currentQuote withSuccess:^(CNCQuote *quote, NSString* submissionText, BOOL retailTimer, NSError *error, BOOL redirect, NSString *message) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];

            if (redirect)
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"requestSignout" object:self];
                return;
            }

            if (message)
            {
                [self displayAlert:message withTitle:@"Error"];
                [weakSelf loadFailScreen];
            } else if (error) {
                [weakSelf loadFailScreen];
            }
            else
            {
				[quote saveQuoteToStorage];
                [weakSelf loadSuccessScreen];
            }
        }];
    }
    else
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            [self.tabBarController setSelectedIndex:0];
            [self loadSuccessScreen];
            CNCCreateQuoteViewController *controller = (CNCCreateQuoteViewController*)self.tabBarController.selectedViewController;
            [controller createNewQuote];
            
        }
        else {
            if ([self.menuDelegate respondsToSelector:@selector(didSelectNewQuoteItem)]) {
                [self.menuDelegate didSelectNewQuoteItem];
            }
        }
    }
}

- (IBAction) onTryAgain
{
    [self loadWelcomeScreen];
    
    if ([self.menuDelegate respondsToSelector:@selector(didSelectNewQuoteItem)]) {
        [self.menuDelegate didSelectNewQuoteItem];
    }
}


@end
