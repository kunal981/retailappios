//
//  CNCSidePanelController.m
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCSidePanelController.h"
#import "CNCMenuViewController.h"
#import "CNCConfig.h"
#import "CNCWelcomeController.h"
#import "CNCNewQuoteController.h"
#import "CNCWebViewController.h"
#import "CNCHowViewController.h"
#import "CNCSplashViewController.h"
#import "CNCContactsController.h"
#import "CNCPendingTableVC.h"
#import "CNCLoginViewController.h"
#import "CNCWatchesViewController.h"
#import "CNCHistoryInfoViewController.h"
#import "CNCSettings.h"
#import "CNCAppOptions.h"

@interface CNCSidePanelController () <CNCMenuDelegate>

@property (nonatomic, strong) UINavigationController *welcomeNavigationController;
@property (nonatomic, strong) UINavigationController *createNewQuoteNavigationController; //do not rename to newQuoteController!
@property (nonatomic, strong) UINavigationController *howNavigationController;
@property (nonatomic, strong) UINavigationController *webNavigationController;
@property (nonatomic, strong) UINavigationController *menuNavigationController;
@property (nonatomic, strong) UINavigationController *splashNavigationController;
@property (nonatomic, strong) UINavigationController *contactsNavigationController;

@property (nonatomic, strong) UINavigationController *pendingQuotesNavigationController;
@property (nonatomic, strong) UINavigationController *watchesToShipNavigationController;
@property (nonatomic, strong) UINavigationController *historyInfoNavigationController;

@property (nonatomic, strong) CNCWelcomeController* welcomeController;
@property (nonatomic, strong) CNCNewQuoteController* createNewQuoteController;
@property (nonatomic, strong) CNCWebViewController* webViewController;
@property (nonatomic, strong) CNCHowViewController* howViewController;
@property (nonatomic, strong) CNCSplashViewController *splashController;
@property (nonatomic, strong) CNCLoginViewController* loginController;
@property (nonatomic, strong) CNCWatchesViewController* watchesController;
@property (nonatomic, strong) CNCHistoryInfoViewController* historyInfoController;
@property (nonatomic, strong) CNCMenuViewController* menuController;

@property (strong, nonatomic) CNCContactsController *contactsController;

@property (strong, nonatomic) CNCPendingTableVC* pendingQuotesVC;

@end

@implementation CNCSidePanelController

-(void) awakeFromNib
{
    self.menuController = [self.storyboard instantiateViewControllerWithIdentifier: @"leftViewController"];
    self.menuController.menuDelegate = self;
    [self setLeftPanel:self.menuController];

    self.welcomeNavigationController = [self.storyboard instantiateViewControllerWithIdentifier: @"welcomeViewController"];
    self.welcomeController = (CNCWelcomeController*)self.welcomeNavigationController.topViewController;
    self.welcomeController.menuDelegate = self;
    
   

    self.createNewQuoteNavigationController = [self.storyboard instantiateViewControllerWithIdentifier: @"newQuoteController"];
    self.createNewQuoteController = (CNCNewQuoteController* ) self.createNewQuoteNavigationController.topViewController;
    self.createNewQuoteController.menuDelegate = self;
    
    self.webNavigationController = [self.storyboard instantiateViewControllerWithIdentifier: @"webNavigationController"];
    self.webViewController = (CNCWebViewController*) self.webNavigationController.topViewController;
    self.webViewController.menuDelegate = self;
	
#ifdef Retail
	self.howNavigationController = [self.storyboard instantiateViewControllerWithIdentifier: @"howNavigationController"];
	self.howViewController = (CNCHowViewController*)self.howNavigationController.topViewController;
	self.howViewController.menuDelegate = self;
	
	
	self.pendingQuotesNavigationController = [self.storyboard instantiateViewControllerWithIdentifier: @"pendingViewController"];
	self.pendingQuotesVC = (CNCPendingTableVC*) self.pendingQuotesNavigationController.topViewController;
	self.pendingQuotesVC.menuDelegate = self;

    self.watchesToShipNavigationController = [self.storyboard instantiateViewControllerWithIdentifier: @"watchesToShipViewController"];
    self.watchesController = (CNCWatchesViewController*) self.watchesToShipNavigationController.topViewController;
    self.watchesController.menuDelegate = self;
    
    self.historyInfoNavigationController = [self.storyboard instantiateViewControllerWithIdentifier: @"historyInfoViewController"];
    self.historyInfoController = (CNCHistoryInfoViewController*) self.historyInfoNavigationController.topViewController;
    self.historyInfoController.menuDelegate = self;
    
    self.loginController = [self.storyboard instantiateViewControllerWithIdentifier: @"loginController"];
    self.loginController.menuDelegate = self;
#endif
    
    self.splashNavigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"splashNavigationController"];
    self.splashController = (CNCSplashViewController*)self.splashNavigationController.topViewController;
    self.splashController.menuDelegate = self;
  
    self.contactsNavigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"contactsViewController"];
    
    [self setCenterPanel:self.splashNavigationController];
    
    self.leftFixedWidth = 150;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self allowSwipes:NO]; // default the swipe to NO until we are allowed to
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleSignoutNeeded:)
                                                 name:@"requestSignout"
                                               object:nil];
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)handleSignoutNeeded:(NSNotification *)note {
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle: @"Sign Out"
                                                    message: @"Your session has expired.\nPlease sign in again."
                                                   delegate: nil
                                          cancelButtonTitle: @"OK"
                                          otherButtonTitles: nil];
    [alert show];

    [self didSelectSignOutItem];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)stylePanel:(UIView *)panel {
    //removing rounded corners, do not remove this method!
}

#pragma mark - Menu actions

- (void)changeToNormalRetailMenu
{
    [self.menuController changeToNormalRetailMenu];
}
- (void)changeToRepRetailMenu
{
    [self.menuController changeToRepRetailMenu];
}


- (void) didSelectBlogItem
{
	[self setCenterPanel: self.webNavigationController];
	[self.webViewController loadPageFromString: kCNCBlogURL];
}

- (void) didSelectHowItem
{
    [self setCenterPanel: self.howNavigationController];
}

- (void)didSelectSignOutItem
{
    [CNCSettings setLoginToken: @""];
    [CNCSettings setUserId: nil];
    [CNCSettings setUserEmail: nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"userSignout" object:self];
    
    [self allowSwipes:NO];

    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }

    [self setCenterPanel: self.loginController];
}

-(void)didSelectNewQuoteItem {
    [self setCenterPanel:self.createNewQuoteNavigationController];
	[self.createNewQuoteController resetUserData];
}

- (void)didSelectPendingQuotesItem
{
    [self setCenterPanel: self.pendingQuotesNavigationController];
}

- (void)didSelectWatchesToShipItem
{
    [self setCenterPanel:self.watchesToShipNavigationController];
}

- (void)didSelectHistoryInfoItem
{
    [self setCenterPanel:self.historyInfoNavigationController];
}

- (void)didSelectTermsAndConditionsItem {
    [self setCenterPanel: self.webNavigationController];
    [self.webViewController loadPageFromHtml: [CNCAppOptions sharedInstance].terms];
}

- (void)didSelectPrivacyPolicyItem {
    [self setCenterPanel:self.webNavigationController];
    [self.webViewController loadPageFromString: kCNCPrivacyURL];

}

- (void)didSelectContactsItem {
    [self setCenterPanel:self.contactsNavigationController];
}

- (void)didSelectThankYouItem {
    [self setCenterPanel:self.welcomeNavigationController];
    [self.welcomeController loadSuccessScreen];
}

- (void)didSelectFailItemWithQuote:(CNCQuote*)quote {
    [self setCenterPanel:self.welcomeNavigationController];
    [self.welcomeController loadFailScreen];
    self.welcomeController.currentQuote = quote ;
}

- (void)didSelectWelcomeItem {
  [self loadWelcomeScreen];
}


- (void)loadWelcomeScreen{
#ifdef Retail
    if ([CNCSettings loginToken].length > 0)
    {
        [self setCenterPanel:self.welcomeNavigationController];
        [self.welcomeController loadWelcomeScreen];
        [self allowSwipes:YES];
    }
    else
    {
        [self setCenterPanel: self.loginController];
        [self allowSwipes:NO];
    }
#else
    [self setCenterPanel:self.welcomeNavigationController];
    [self.welcomeController loadWelcomeScreen];
#endif
}

- (void)allowSwipes:(BOOL) allow {
    self.allowLeftSwipe = allow;
    self.allowRightSwipe = allow;
}

-(NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

@end
