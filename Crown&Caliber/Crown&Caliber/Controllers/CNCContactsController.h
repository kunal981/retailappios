//
//  CNCContactsController.h
//  Crown&Caliber
//
//  Created by Igor Tomych on 29/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNCBaseViewController.h"

@interface CNCContactsController : CNCBaseViewController

@end
