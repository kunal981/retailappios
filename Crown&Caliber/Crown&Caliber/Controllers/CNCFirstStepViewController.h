//
//  CNCFirstStepViewController.h
//  Crown&Caliber
//
//  Created by valerio8 on 09.07.14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNCQuote.h"

@protocol CNCFirstStepDelegate <NSObject>

@required
-(void)goToSecondStepWithSuccess:(BOOL)success;
-(void) enableInformationButton;
@optional
-(BOOL)arePhotosUploaded;
-(UITabBarController*)tabBarController;
@end


@interface CNCFirstStepViewController : UIViewController
@property (strong,nonatomic) id<CNCFirstStepDelegate> delegate;
@property (strong,nonatomic) CNCQuote *currentQuote;

-(void) deleteUserData;


@end

