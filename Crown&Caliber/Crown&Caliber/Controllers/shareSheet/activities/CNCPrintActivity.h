//
//  CNCPrintActivity.h
//  Crown&Caliber
//
//  Created by Valeriy on 10/7/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CNCPrintActivityDelegate <NSObject>

- (void) printActivityPerfomed;

@end

@interface CNCPrintActivity : UIActivity

@property (nonatomic, assign) NSObject<CNCPrintActivityDelegate>* delegate;

@end
