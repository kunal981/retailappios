//
//  CNCPrintActivity.m
//  Crown&Caliber
//
//  Created by Valeriy on 10/7/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCPrintActivity.h"

@implementation CNCPrintActivity


- (NSString*) activityType
{
	return @"cnc.activity.print";
}

- (NSString*) activityTitle
{
	return NSLocalizedString(@"Print", @"");
}

- (UIImage*) activityImage
{
	return nil;
}

- (BOOL) canPerformWithActivityItems: (NSArray*) activityItems
{
	return YES;
}

- (void) prepareWithActivityItems: (NSArray*) activityItems
{
}

- (UIViewController*) activityViewController
{
	return nil;
}

- (void) performActivity
{
	if (self.delegate)
	{
		[self.delegate printActivityPerfomed];
	}
}

@end
