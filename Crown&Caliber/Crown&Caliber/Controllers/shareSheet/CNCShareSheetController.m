//
//  CNCShareSheetController.m
//  Crown&Caliber
//
//  Created by Valeriy on 10/6/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCShareSheetController.h"

@interface CNCShareSheetController ()

@property (nonatomic, strong) UIActivityViewController* activityVC;
@property (nonatomic, assign) id delegate;
@property (nonatomic, strong) UIPopoverController *popController;

@end

@implementation CNCShareSheetController

- (id) initWithCrownAndCaliberDefaultItemsAndDelegate: (id) delegate
{
	NSArray *activityItems = @[[NSURL URLWithString: @"http://google.com"]];
//	UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
//	activityVC.excludedActivityTypes = @[UIActivityTypeMessage, UIActivityTypePrint];
	
	NSMutableArray* exludedActivityTypes = [NSMutableArray arrayWithArray: @[UIActivityTypePrint, UIActivityTypeMail]];
	
	self = [self initWithActivityItems: activityItems
				 applicationActivities: nil
				  exludedActivityTypes: exludedActivityTypes];
	if (self)
	{
		
	}
	return self;
}

- (id) initWithActivityItems: (NSArray*) activityIyitems
	   applicationActivities: (NSArray*) applicationActivities
		exludedActivityTypes: (NSArray*) exludedActivityTypes
{
	if (self = [super init])
	{
		self.activityVC = [[UIActivityViewController alloc] initWithActivityItems: activityIyitems
															applicationActivities: applicationActivities];
		self.activityVC.excludedActivityTypes = exludedActivityTypes;
	}
	
	return self;
}

- (void) startForViewController: (UIViewController*) viewController
					   fromItem: (UIBarButtonItem*) item
{
	if ([[UIDevice currentDevice].systemVersion doubleValue] >= 8.0)
	{
		UIPopoverPresentationController *popPresenter = [self.activityVC popoverPresentationController];
		popPresenter.barButtonItem = item;
		
		[viewController presentViewController: self.activityVC
									 animated: YES
								   completion: nil];

	}
	else
	{
		self.popController = [[UIPopoverController alloc] initWithContentViewController: self.activityVC];
		[self.popController presentPopoverFromBarButtonItem: item
							  permittedArrowDirections: UIPopoverArrowDirectionAny
											  animated: YES];
	}
	
}

- (void) dismiss
{
	if ([[UIDevice currentDevice].systemVersion doubleValue] >= 8.0)
	{
		[self.activityVC dismissViewControllerAnimated: YES completion: nil];
	}
	else
	{
		[self.popController dismissPopoverAnimated: YES];
	}
}

@end
