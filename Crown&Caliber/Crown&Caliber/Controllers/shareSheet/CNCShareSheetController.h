//
//  CNCShareSheetController.h
//  Crown&Caliber
//
//  Created by Valeriy on 10/6/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CNCShareSheetController : NSObject

- (id) initWithCrownAndCaliberDefaultItemsAndDelegate: (id) delegate;

- (id) initWithActivityItems: (NSArray*) activityIyitems
	   applicationActivities: (NSArray*) applicationActivities
		exludedActivityTypes: (NSArray*) exludedActivityTypes;

- (void) startForViewController: (UIViewController*) viewController
					   fromItem: (UIBarButtonItem*) item;
- (void) dismiss;

@end
