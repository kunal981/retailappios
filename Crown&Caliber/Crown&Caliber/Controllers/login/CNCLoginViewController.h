//
//  CNCLoginViewController.h
//  Crown&Caliber
//

#import <UIKit/UIKit.h>
#import "CNCMenuViewController.h"

@interface CNCLoginViewController : UIViewController

@property (nonatomic, strong) id<CNCMenuDelegate> menuDelegate;
@property (nonatomic, strong) IBOutlet UITextField* emailField;
@property (nonatomic, strong) IBOutlet UITextField* passwordField;
@property (nonatomic, strong) IBOutlet UILabel* createAccountLabel;
@property (nonatomic, strong) IBOutlet UIButton* signInButton;

- (IBAction) onLogin;

@end
