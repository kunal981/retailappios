//
//  CNCLoginViewController.m
//  Crown&Caliber
//

#import "CNCLoginViewController.h"
#import "CNCApi.h"
#import "MBProgressHUD.h"
#import "UIColor+CNCColor.h"
#import "CNCSettings.h"
#import "CNCAppOptions.h"
#import <MediaPlayer/MediaPlayer.h>

@interface CNCLoginViewController ()

@property (nonatomic, strong) MPMoviePlayerController* moviePlayer;

@end

@implementation CNCLoginViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self.signInButton setBackgroundColor: [UIColor CNCOrangeColor]];
    
//    self.emailField.text = @"hamiltongardens@crownandcaliber.com";
//    self.emailField.text = @"big-test@crownandcaliber.com";
//    self.emailField.text = @"small-test@crownandcaliber.com";
//    self.emailField.text = @"rep-test@crownandcaliber.com";
//    self.passwordField.text = @"1234567890";

	

    NSMutableAttributedString* createAccountString = [[NSMutableAttributedString alloc] initWithString: self.createAccountLabel.text];
    NSRange hereRange = [self.createAccountLabel.text rangeOfString: @"here"];
    if (hereRange.location != NSNotFound)
    {
        [createAccountString addAttribute: NSUnderlineStyleAttributeName
                                    value: @(NSUnderlineStyleSingle)
                                    range: hereRange];
        self.createAccountLabel.attributedText = createAccountString;
    }

    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(onCreateAccount:)];
    [self.createAccountLabel addGestureRecognizer: tap];
    self.createAccountLabel.userInteractionEnabled = YES;

    NSBundle* bundle = [NSBundle mainBundle];
    NSString* moviePath = [bundle pathForResource: @"Background" ofType: @"mov"];
    NSURL* movieURL = [NSURL fileURLWithPath: moviePath];

    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL: movieURL];
    self.moviePlayer.controlStyle = MPMovieControlStyleNone;
    self.moviePlayer.scalingMode = MPMovieScalingModeAspectFill;
    self.moviePlayer.repeatMode = MPMovieRepeatModeOne;

    self.moviePlayer.view.frame = self.view.bounds;
    [self.view insertSubview: self.moviePlayer.view atIndex: 0];
    [self.moviePlayer play]; 
}

- (void) viewWillAppear:(BOOL)animated {
    self.emailField.text = @"";
    self.passwordField.text = @"";
}


- (void) displayAlert: (NSString*) message withTitle: (NSString*) title
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle: title
                                                    message: message
                                                   delegate: nil
                                          cancelButtonTitle: @"OK"
                                          otherButtonTitles: nil];
    [alert show];
}

#pragma mark - Events

- (IBAction) onLogin
{
    if (self.emailField.text.length == 0)
    {
        [self displayAlert: @"Email address is required" withTitle: @""];
        return;
    }

    if (self.passwordField.text.length == 0)
    {
        [self displayAlert: @"Password is required" withTitle: @""];
        return;
    }

	[CNCSettings setUserEmail: self.emailField.text];
    
    [MBProgressHUD showHUDAddedTo: self.parentViewController.view animated:YES];

    [[CNCApi sharedClient] loginWithEmail: self.emailField.text
                                 password: self.passwordField.text
                             successBlock: ^(NSString* userId, NSString* authToken, NSError* error, NSString *message)
     {
		 [MBProgressHUD hideAllHUDsForView: self.parentViewController.view animated: YES];
         if (message)
         {
             [self displayAlert: message withTitle: @"Error"];
         }
		 else if (error)
		 {
			 [self displayAlert: [error localizedDescription] withTitle: @"Error"];
		 }
		 else
		 {
			 [MBProgressHUD showHUDAddedTo: self.parentViewController.view animated:YES];

			 [[CNCApi sharedClient] upateAppInfoWithCompletionBlock:^(NSError *error, BOOL redirect) {
				 [MBProgressHUD hideAllHUDsForView: self.parentViewController.view animated: YES];
                 
                 if (redirect)
                 {
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"requestSignout" object:self];
                     return;
                 }

                 
				 if (!error)
				 {
					 [self.menuDelegate didSelectWelcomeItem];
#ifdef Retail
                     if ([CNCAppOptions sharedInstance].appSize == AppSizeRep)
                     {
                         [self.menuDelegate changeToRepRetailMenu];
                     } else {
                         [self.menuDelegate changeToNormalRetailMenu];
                     }
#endif
				 }
			 }];
		 }
     }];
}

- (void) onCreateAccount: (UIGestureRecognizer*) recognized
{
    if (recognized.state == UIGestureRecognizerStateRecognized)
    {
        NSURL* url = [NSURL URLWithString: @"http://google.com"];
        [[UIApplication sharedApplication] openURL: url];
    }
}

@end
