//
//  CNCHistoricalDataViewController.h
//  Crown&Caliber
//

#import <UIKit/UIKit.h>
#import "CNCBaseViewController.h"

@class CNCQuote;
@class CNCCase;

@interface CNCHistoricalDataViewController: CNCBaseViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView* historyTableView;

@property (nonatomic, strong) IBOutlet UILabel* brandLabel;
@property (nonatomic, strong) IBOutlet UILabel* modelLabel;
@property (nonatomic, strong) IBOutlet UILabel* modelNumLabel;
@property (nonatomic, strong) IBOutlet UILabel* nameLabel;
@property (nonatomic, strong) IBOutlet UILabel* phoneLabel;
@property (nonatomic, strong) IBOutlet UILabel* receivedLabel;
@property (nonatomic, strong) IBOutlet UILabel* caseIdLabel;
@property (nonatomic, strong) IBOutlet UILabel* associateNameLabel;
@property (nonatomic, strong) IBOutlet UITextView* notesTextView;
@property (nonatomic, strong) IBOutlet UIImageView* watchPhoto;
@property (nonatomic, strong) IBOutlet UILabel* footerEmailLabel;


@property (nonatomic, strong) IBOutlet UIButton* backToOfferButton;

@property (nonatomic, strong) CNCQuote* quote;
@property (nonatomic, strong) CNCCase* caseData;

- (IBAction) onBackToOffer;

@end
