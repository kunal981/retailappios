//
//  CNCRejectOfferViewController.m
//  Crown&Caliber
//

#import "CNCRejectOfferViewController.h"
#import "CNCAppOptions.h"
#import "CNCApi.h"
#import "UIColor+CNCColor.h"
#import "CNCQuote.h"
#import "MBProgressHUD.h"
#import "CNCPendingTableVC.h"
#import "UIImageView+AFNetworking.h"

@interface CNCRejectOfferViewController ()

@property (nonatomic, strong) NSArray* declineReasons;
@property (nonatomic, assign) NSInteger selectedCell;

@end

@implementation CNCRejectOfferViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.selectedCell = NSNotFound;
    
    self.declineReasons = [[CNCAppOptions sharedInstance] declineReasons];
    
    self.rejectButton.backgroundColor = [UIColor CNCOrangeColor];

    [self updateQuote];
}

- (void) viewDidAppear: (BOOL) animated
{
    [super viewDidAppear: animated];
    
    [self updateQuote];
}


- (void) viewWillAppear: (BOOL) animated
{
    [super viewWillAppear: animated];
    NSString *phoneMailString;
    if ([CNCAppOptions sharedInstance].appSize == AppSizeRep)
    {
        phoneMailString = @"Phone: (404) 812-9928";
    } else
    {
        phoneMailString = @"Phone: (800) 514-3750       |       Email: bnollner@crownandcaliber.com";
    }
    
    NSMutableAttributedString* phoneMailAttr = [[NSMutableAttributedString alloc] initWithString: phoneMailString];
    [phoneMailAttr addAttribute: NSFontAttributeName
                          value: [UIFont systemFontOfSize: 17.0f]
                          range: NSMakeRange(0, phoneMailString.length)];
    
    NSRange phoneRange = [phoneMailString rangeOfString: @"Phone:"];
    if (phoneRange.location != NSNotFound)
    {
        [phoneMailAttr addAttribute: NSFontAttributeName
                              value: [UIFont boldSystemFontOfSize: 17.0f]
                              range: phoneRange];
    }
    
    NSRange emailRange = [phoneMailString rangeOfString: @"Email:"];
    if (emailRange.location != NSNotFound)
    {
        [phoneMailAttr addAttribute: NSFontAttributeName
                              value: [UIFont boldSystemFontOfSize: 17.0f]
                              range: emailRange];
    }
    
    _footerEmailLabel.attributedText = phoneMailAttr;
}


- (void) setQuote: (CNCQuote*) aQuote
{
    _quote = aQuote;
    
    [self updateQuote];
}

- (void) updateQuote
{
    self.brandLabel.text = self.quote.brand;
    self.modelLabel.text = self.quote.modelName;
    self.modelNumLabel.text = self.quote.modelNumber;
    self.nameLabel.text = self.quote.name;
    self.phoneLabel.text = self.quote.phone;
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle: NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle: NSDateFormatterMediumStyle];
    
    self.receivedLabel.text = [dateFormatter stringFromDate: self.quote.createdAt];
    
    if (self.quote.imageURL.length > 0)
    {
        NSURL* watchURL = [NSURL URLWithString: self.quote.imageURL];
        [self.watchPhoto setImageWithURL: watchURL];
    }
}

- (void) displayAlert: (NSString*) message withTitle: (NSString*) title
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle: title
                                                    message: message
                                                   delegate: nil
                                          cancelButtonTitle: @"OK"
                                          otherButtonTitles: nil];
    [alert show];
}

#pragma mark - Events

- (IBAction) onReject
{
    [MBProgressHUD showHUDAddedTo: self.parentViewController.view animated: YES];
    
    [[CNCApi sharedClient] rejectCase: self.quote.caseId
                        declineReason: self.declineReasons[self.selectedCell]
                         successBlock: ^(NSError* error, BOOL redirect)
     {
         [MBProgressHUD hideAllHUDsForView: self.parentViewController.view animated: YES];
         
         if (redirect)
         {
             [[NSNotificationCenter defaultCenter] postNotificationName:@"requestSignout" object:self];
             return;
         }

         if (error == nil)
         {
             for (UIViewController* viewController in self.navigationController.viewControllers)
             {
                 if ([viewController isKindOfClass: [CNCPendingTableVC class]])
                 {
                     [self.navigationController popToViewController: viewController animated: YES];
                     break;
                 }
             }
         }
         else
         {
             [self displayAlert: [error localizedDescription] withTitle: @"Error"];
         }
     }];
}

#pragma mark - UITableViewDataSource

- (NSInteger) tableView: (UITableView*) tableView numberOfRowsInSection: (NSInteger) section
{
    return self.declineReasons.count;
}

- (UITableViewCell*) tableView: (UITableView*) tableView cellForRowAtIndexPath: (NSIndexPath*) indexPath
{
    static NSString* CellId = @"ReasonCellId";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: CellId];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault
                                      reuseIdentifier: CellId];
    }
    
    cell.textLabel.text = self.declineReasons[indexPath.row];
    cell.accessoryType = (self.selectedCell == indexPath.row) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void) tableView: (UITableView*) tableView didSelectRowAtIndexPath: (NSIndexPath*) indexPath
{
    [tableView deselectRowAtIndexPath: indexPath animated: NO];
    
    NSArray* rowsToReload = @[ indexPath ];
    
    if (self.selectedCell == indexPath.row)
    {
        self.rejectButton.enabled = NO;
        self.selectedCell = NSNotFound;
    }
    else
    {
        self.rejectButton.enabled = YES;
    
        rowsToReload = @[ indexPath ];
        if (self.selectedCell != NSNotFound)
        {
            rowsToReload = [rowsToReload arrayByAddingObject: [NSIndexPath indexPathForRow: self.selectedCell inSection: 0]];
        }
        
        self.selectedCell = indexPath.row;
    }

    [tableView reloadRowsAtIndexPaths: rowsToReload withRowAnimation: UITableViewRowAnimationAutomatic];
}

@end
