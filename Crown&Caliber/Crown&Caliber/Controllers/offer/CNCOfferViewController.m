//
//  CNCOfferViewController.m
//  Crown&Caliber
//

#import "CNCOfferViewController.h"
#import "CNCQuote.h"
#import "CNCHistoricalDataViewController.h"
#import "CNCOfferProcessVC.h"
#import "UIColor+CNCColor.h"
#import "CNCAppOptions.h"
#import "CNCHistoryInfoViewController.h"
#import "CNCApi.h"
#import "MBProgressHUD.h"
#import "CNCPendingTableVC.h"
#import "CNCRejectOfferViewController.h"
#import "UIImageView+AFNetworking.h"
#import "CNCConfig.h"
#import "CNCWebViewController.h"
#import "CNCCase.h"

@interface CNCOfferViewController () <UIAlertViewDelegate>

@property (nonatomic, strong) CNCCase* caseData;

@end

@implementation CNCOfferViewController

- (void) viewDidLoad
{
    [super viewDidLoad];

    [self.viewOfferButton setBackgroundColor: [UIColor CNCOrangeColor]];
    [self.viewHistoricalDataButton setBackgroundColor: [UIColor CNCOrangeColor]];
    [self.acceptOfferButton setBackgroundColor: [UIColor CNCOrangeColor]];
    [self.rejectOfferButton setBackgroundColor: [UIColor CNCOrangeColor]];
    [self.undecidedButton setBackgroundColor: [UIColor CNCOrangeColor]];
    
    [self updateQuote];
}

- (void) viewDidAppear: (BOOL) animated
{
    [super viewDidAppear: animated];
    
    [self updateQuote];

    if (self.caseData == nil)
    {
        [MBProgressHUD showHUDAddedTo: self.parentViewController.view animated: YES];

        [[CNCApi sharedClient] watchCaseInfo: self.quote.caseId
                                successBlock: ^(CNCCase* caseInfo, NSError* error, BOOL redirect)
         {
             [MBProgressHUD hideAllHUDsForView: self.parentViewController.view animated: YES];

             if (redirect)
             {
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"requestSignout" object:self];
                 return;
             }

             if (error == nil)
             {
                 self.caseData = caseInfo;
                 self.offerLabel.text = self.caseData.offerText;
             }
             else
             {
                 [self displayAlert: error.localizedDescription withTitle: @"Error"];
             }
         }];
    }
}

- (void) viewWillAppear: (BOOL) animated
{
    [super viewWillAppear: animated];
    NSString *phoneMailString;
    if ([CNCAppOptions sharedInstance].appSize == AppSizeRep)
    {
        phoneMailString = @"Phone: (404) 812-9928";
    } else
    {
        phoneMailString = @"Phone: (800) 514-3750       |       Email: bnollner@crownandcaliber.com";
    }
    
    NSMutableAttributedString* phoneMailAttr = [[NSMutableAttributedString alloc] initWithString: phoneMailString];
    [phoneMailAttr addAttribute: NSFontAttributeName
                          value: [UIFont systemFontOfSize: 17.0f]
                          range: NSMakeRange(0, phoneMailString.length)];
    
    NSRange phoneRange = [phoneMailString rangeOfString: @"Phone:"];
    if (phoneRange.location != NSNotFound)
    {
        [phoneMailAttr addAttribute: NSFontAttributeName
                              value: [UIFont boldSystemFontOfSize: 17.0f]
                              range: phoneRange];
    }
    
    NSRange emailRange = [phoneMailString rangeOfString: @"Email:"];
    if (emailRange.location != NSNotFound)
    {
        [phoneMailAttr addAttribute: NSFontAttributeName
                              value: [UIFont boldSystemFontOfSize: 17.0f]
                              range: emailRange];
    }
    
    _footerEmailLabel.attributedText = phoneMailAttr;
}


- (void) prepareForSegue: (UIStoryboardSegue*) segue sender: (id) sender
{
    if ([segue.destinationViewController isKindOfClass: [CNCHistoricalDataViewController class]])
    {
        CNCHistoricalDataViewController* historyController = (CNCHistoricalDataViewController*)segue.destinationViewController;
        historyController.quote = self.quote;
        historyController.caseData = self.caseData;
    }
    else if ([segue.destinationViewController isKindOfClass: [CNCOfferProcessVC class]])
    {
        CNCOfferProcessVC* offerProcessController = (CNCOfferProcessVC*)segue.destinationViewController;
        [offerProcessController setQuote: self.quote];
        [offerProcessController setCaseData: self.caseData];
    }
    else if ([segue.destinationViewController isKindOfClass: [CNCRejectOfferViewController class]])
    {
        CNCRejectOfferViewController* rejectController = (CNCRejectOfferViewController*)segue.destinationViewController;
        [rejectController setQuote: self.quote];
    }
}

- (void) setQuote: (CNCQuote*) aQuote
{
    _quote = aQuote;

    [self updateQuote];
}

- (void) updateQuote
{
    self.brandLabel.text = self.quote.brand;
    self.modelLabel.text = self.quote.modelName;
    self.modelNumLabel.text = self.quote.modelNumber;
    self.nameLabel.text = self.quote.name;
    self.phoneLabel.text = self.quote.phone;
    self.caseIdLabel.text = [self.quote.caseId description];
    self.associateNameLabel.text = self.quote.retailerId;

    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle: NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle: NSDateFormatterMediumStyle];

    self.receivedLabel.text = [dateFormatter stringFromDate: self.quote.createdAt];
    
    if (self.quote.imageURL.length > 0)
    {
        NSURL* watchURL = [NSURL URLWithString: self.quote.imageURL];
        [self.watchPhoto setImageWithURL: watchURL];
    }
    
    AppSizeGroup appSize = [CNCAppOptions sharedInstance].appSize;
    
    // Big
    self.viewOfferButton.hidden = YES;
    self.offerLabel.hidden = NO;
    self.acceptOfferButton.hidden = NO;
    self.rejectOfferButton.hidden = NO;
    self.undecidedButton.hidden = NO;
    self.termsAndConditionsButton.hidden = NO;
    self.privacyPolicyButton.hidden = NO;
    
    if (appSize == AppSizeSmall)
    {
        // Little
        self.viewOfferButton.hidden = NO;
        self.offerLabel.hidden = YES;
    }
    else if (appSize == AppSizeRep)
    {
        // Appraiser
        self.acceptOfferButton.hidden = YES;
        self.rejectOfferButton.hidden = YES;
        self.undecidedButton.hidden = YES;
        self.termsAndConditionsButton.hidden = YES;
        self.privacyPolicyButton.hidden = YES;
    }
}

- (void) presentWebVCWithLink: (NSString*) link
{
    UINavigationController* webNavigationController = [self.storyboard instantiateViewControllerWithIdentifier: @"webNavigationController"];
    CNCWebViewController* webViewController = (CNCWebViewController*) webNavigationController.topViewController;
    [webViewController loadPageFromString: link];
    
    UIBarButtonItem* doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemDone
                                                                              target: self
                                                                              action: @selector(donePressed)];
    webViewController.navigationItem.rightBarButtonItem = doneItem;
    [self presentViewController: webNavigationController animated: YES completion: nil];
    
}

- (void) presentWebVCWithHtml: (NSString*) html
{
    UINavigationController* webNavigationController = [self.storyboard instantiateViewControllerWithIdentifier: @"webNavigationController"];
    CNCWebViewController* webViewController = (CNCWebViewController*) webNavigationController.topViewController;
    [webViewController loadPageFromHtml: html];

    UIBarButtonItem* doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemDone
                                                                              target: self
                                                                              action: @selector(donePressed)];
    webViewController.navigationItem.rightBarButtonItem = doneItem;
    [self presentViewController: webNavigationController animated: YES completion: nil];

}

#pragma mark - Events

- (IBAction) onViewOffer
{
    if ([CNCAppOptions sharedInstance].appSize == AppSizeSmall)
    {
        [self displayAlert: self.caseData.offerText
                 withTitle: @"Offer"];
    }
}

- (IBAction) onAcceptOffer
{
    if (!([CNCAppOptions sharedInstance].appSize == AppSizeSmall))
    {
        [self performSegueWithIdentifier: @"acceptOfferSegue" sender: self];
    }
    else
    {
        [MBProgressHUD showHUDAddedTo: self.parentViewController.view animated: YES];
        
        [[CNCApi sharedClient] acceptCase: self.quote.caseId
                             successBlock: ^(NSString* thankYou, NSError* error, BOOL redirect)
         {
             [MBProgressHUD hideAllHUDsForView: self.parentViewController.view animated: YES];
             
             if (redirect)
             {
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"requestSignout" object:self];
                 return;
             }
             
             if (error == nil)
             {
                 [self displayAlert: thankYou
                          withTitle: @"Thank You"
                         completion: ^{
                             for (UIViewController* viewController in self.navigationController.viewControllers)
                             {
                                 if ([viewController isKindOfClass: [CNCPendingTableVC class]])
                                 {
                                     [self.navigationController popToViewController: viewController animated: YES];
                                     break;
                                 }
                             }
                             for (UIViewController* viewController in self.navigationController.viewControllers)
                             {
                                 if ([viewController isKindOfClass: [CNCHistoryInfoViewController class]])
                                 {
                                     [self.navigationController popToViewController: viewController animated: YES];
                                     break;
                                 }
                             }
                         }
                  ];
             }
             else
             {
                 [self displayAlert: [error localizedDescription] withTitle: @"Error"];
             }
         }];
    }
}

- (IBAction) onUndecided
{
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction) termsAndConditionsPressed: (UIButton*) sender
{
    [self presentWebVCWithHtml: [CNCAppOptions sharedInstance].terms];
}

- (IBAction) privacyPolicyPressed: (UIButton*) sender
{
    [self presentWebVCWithLink: kCNCPrivacyURL];
}

- (void) donePressed
{
    [self dismissViewControllerAnimated: YES completion: nil];
}

@end
