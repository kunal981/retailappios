//
//  CNCHistoricalDataViewController.m
//  Crown&Caliber
//

#import "CNCHistoricalDataViewController.h"
#import "CNCQuote.h"
#import "UIColor+CNCColor.h"
#import "UIImageView+AFNetworking.h"
#import "CNCApi.h"
#import "CNCCase.h"
#import "CNCHistoryCell.h"
#import "CNCAppOptions.h"

@interface CNCHistoricalDataViewController ()

@end

@implementation CNCHistoricalDataViewController

- (void) viewDidLoad
{
    [super viewDidLoad];

    [self.backToOfferButton setBackgroundColor: [UIColor CNCOrangeColor]];
    
    //Style the notes field
    [self.notesTextView.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [self.notesTextView.layer setBorderWidth:2.0];
    
    //The rounded corner part, where you specify your view's corner radius:
    self.notesTextView.layer.cornerRadius = 5;
    self.notesTextView.clipsToBounds = YES;
    
    self.notesTextView.hidden = YES;
    [self pullOutAndShowNotes];
    

    [self updateQuote];
}


- (void) viewWillAppear: (BOOL) animated
{
    [super viewWillAppear: animated];
    NSString *phoneMailString;
    if ([CNCAppOptions sharedInstance].appSize == AppSizeRep)
    {
        phoneMailString = @"Phone: (404) 812-9928";
    } else
    {
        phoneMailString = @"Phone: (800) 514-3750       |       Email: bnollner@crownandcaliber.com";
    }
    
    NSMutableAttributedString* phoneMailAttr = [[NSMutableAttributedString alloc] initWithString: phoneMailString];
    [phoneMailAttr addAttribute: NSFontAttributeName
                          value: [UIFont systemFontOfSize: 17.0f]
                          range: NSMakeRange(0, phoneMailString.length)];
    
    NSRange phoneRange = [phoneMailString rangeOfString: @"Phone:"];
    if (phoneRange.location != NSNotFound)
    {
        [phoneMailAttr addAttribute: NSFontAttributeName
                              value: [UIFont boldSystemFontOfSize: 17.0f]
                              range: phoneRange];
    }
    
    NSRange emailRange = [phoneMailString rangeOfString: @"Email:"];
    if (emailRange.location != NSNotFound)
    {
        [phoneMailAttr addAttribute: NSFontAttributeName
                              value: [UIFont boldSystemFontOfSize: 17.0f]
                              range: emailRange];
    }
    
    _footerEmailLabel.attributedText = phoneMailAttr;

}


- (void)pullOutAndShowNotes {
    
    if (self.caseData.notes) {
        NSString* notes = [NSString stringWithFormat:@"Valuation Notes:\n%@", self.caseData.notes];
        self.notesTextView.text = notes;
        self.notesTextView.hidden = NO;
    }
}


- (void) setQuote: (CNCQuote*) aQuote
{
    _quote = aQuote;

    [self updateQuote];
}

- (void) updateQuote
{
    self.brandLabel.text = self.quote.brand;
    self.modelLabel.text = self.quote.modelName;
    self.modelNumLabel.text = self.quote.modelNumber;
    self.nameLabel.text = self.quote.name;
    self.phoneLabel.text = self.quote.phone;
    self.caseIdLabel.text = [self.quote.caseId description];
    self.associateNameLabel.text = self.quote.retailerId;

    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle: NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle: NSDateFormatterMediumStyle];

    self.receivedLabel.text = [dateFormatter stringFromDate: self.quote.createdAt];
    
    if (self.quote.imageURL.length > 0)
    {
        NSURL* watchURL = [NSURL URLWithString: self.quote.imageURL];
        [self.watchPhoto setImageWithURL: watchURL];
    }
}


#pragma mark - Events

- (IBAction) onBackToOffer
{
    [self.navigationController popViewControllerAnimated: YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger) tableView: (UITableView*) tableView numberOfRowsInSection: (NSInteger) section
{
    return self.caseData.watchData.count;
}

- (UITableViewCell*) tableView: (UITableView*) tableView cellForRowAtIndexPath: (NSIndexPath*) indexPath
{
    NSDictionary* historyRecord = self.caseData.watchData[indexPath.row];

    CNCHistoryCell* cell = (CNCHistoryCell*)[tableView dequeueReusableCellWithIdentifier: @"HistoryCellId"
                                                                            forIndexPath: indexPath];

    cell.priceLabel.text = [NSString stringWithFormat: @"$%@", historyRecord[@"price"]];
    cell.dateOfSaleLabel.text = historyRecord[@"auction_at"];
    cell.brandLabel.text = historyRecord[@"brand"];
    cell.modelLabel.text = historyRecord[@"model_name"];
    cell.modelNumLabel.text = historyRecord[@"model_number"];
    cell.salesFormatLabel.text = historyRecord[@"source"];

    return cell;
}

@end
