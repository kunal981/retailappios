//
//  CNCOfferViewController.h
//  Crown&Caliber
//

#import <UIKit/UIKit.h>
#import "CNCBaseViewController.h"

@class CNCQuote;

@interface CNCOfferViewController: CNCBaseViewController

@property (nonatomic, strong) IBOutlet UILabel* offerLabel;

@property (nonatomic, strong) IBOutlet UILabel* brandLabel;
@property (nonatomic, strong) IBOutlet UILabel* modelLabel;
@property (nonatomic, strong) IBOutlet UILabel* modelNumLabel;
@property (nonatomic, strong) IBOutlet UILabel* nameLabel;
@property (nonatomic, strong) IBOutlet UILabel* phoneLabel;
@property (nonatomic, strong) IBOutlet UILabel* receivedLabel;
@property (nonatomic, strong) IBOutlet UILabel* caseIdLabel;
@property (nonatomic, strong) IBOutlet UILabel* associateNameLabel;
@property (nonatomic, strong) IBOutlet UIImageView* watchPhoto;

@property (nonatomic, strong) IBOutlet UIButton* viewOfferButton;
@property (nonatomic, strong) IBOutlet UIButton* viewHistoricalDataButton;
@property (nonatomic, strong) IBOutlet UIButton* acceptOfferButton;
@property (nonatomic, strong) IBOutlet UIButton* rejectOfferButton;
@property (nonatomic, strong) IBOutlet UIButton* undecidedButton;
@property (nonatomic, strong) IBOutlet UILabel* footerEmailLabel;
@property (nonatomic, strong) IBOutlet UIButton* termsAndConditionsButton;
@property (nonatomic, strong) IBOutlet UIButton* privacyPolicyButton;



@property (nonatomic, strong) CNCQuote* quote;

- (IBAction) onAcceptOffer;
- (IBAction) onUndecided;

- (IBAction) termsAndConditionsPressed: (UIButton*) sender;
- (IBAction) privacyPolicyPressed: (UIButton*) sender;


@end
