//
//  CNCRejectOfferViewController.h
//  Crown&Caliber
//

#import <UIKit/UIKit.h>
#import "CNCBaseViewController.h"

@class CNCQuote;

@interface CNCRejectOfferViewController: CNCBaseViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) IBOutlet UILabel* brandLabel;
@property (nonatomic, strong) IBOutlet UILabel* modelLabel;
@property (nonatomic, strong) IBOutlet UILabel* modelNumLabel;
@property (nonatomic, strong) IBOutlet UILabel* nameLabel;
@property (nonatomic, strong) IBOutlet UILabel* phoneLabel;
@property (nonatomic, strong) IBOutlet UILabel* receivedLabel;
@property (nonatomic, strong) IBOutlet UIImageView* watchPhoto;
@property (nonatomic, strong) IBOutlet UILabel* footerEmailLabel;


@property (nonatomic, strong) IBOutlet UIButton* rejectButton;
@property (nonatomic, strong) IBOutlet UITableView* reasonsTableView;

@property (nonatomic, strong) CNCQuote* quote;

- (IBAction) onReject;

@end
