//
//  CNCHowViewController.h
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNCMenuViewController.h"
#import "CNCBaseViewController.h"

@interface CNCHowViewController : CNCBaseViewController

@property (nonatomic, strong) id <CNCMenuDelegate> menuDelegate;

@end
