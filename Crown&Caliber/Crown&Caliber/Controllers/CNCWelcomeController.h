//
//  CNCWelcomeController.h
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNCMenuViewController.h"
#import "CNCQuote.h"
#import "CNCBaseViewController.h"

@interface CNCWelcomeController : CNCBaseViewController

@property (nonatomic, strong) id<CNCMenuDelegate> menuDelegate;
@property (nonatomic, strong) CNCQuote *currentQuote;

-(void)loadWelcomeScreen;
-(void)loadSuccessScreen;
-(void)loadFailScreen;

- (IBAction) onTryAgain;

@end
