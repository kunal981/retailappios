//
//  CNCPendingTableVC.m
//  Crown&Caliber
//
//  Created by Valeriy on 9/23/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCPendingTableVC.h"
#import "CNCPendingQuoteTVC.h"
#import "CNCQuote.h"
#import "CNCOfferProcessVC.h"
#import "CNCOfferViewController.h"
#import "CNCTimerLabel.h"
#import "CNCSettings.h"
#import "CNCApi.h"
#import "MBProgressHUD.h"
#import "UIImageView+AFNetworking.h"
#import "CNCFooterView.h"
#import "SHSPhoneNumberFormatter.h"
#import "SHSPhoneNumberFormatter+UserConfig.h"
#import "CNCImageViewerViewController.h"

static const NSTimeInterval Timeout = 20 * 60;

@interface CNCPendingTableVC () <CNCOfferProcessVC_Delegate, UIAlertViewDelegate>

@property (nonatomic, strong) NSArray* pendingQuotes;
@property (nonatomic, unsafe_unretained) NSInteger selectedIndex;
@property (nonatomic, strong) NSDateFormatter* dateFormatter;
@property (nonatomic, strong) NSDateFormatter* timeFormatter;
@property (nonatomic, strong) SHSPhoneNumberFormatter* phoneFormatter;
@property (nonatomic, strong) UIRefreshControl* refreshControl;
@property (nonatomic, strong) IBOutlet UILabel* headerLabel;
@property (nonatomic) BOOL returningFromSecondaryView;

@property (nonatomic, strong) NSDictionary *systemDict;
@property (nonatomic, strong) NSDictionary *systemBoldDict;
@property (nonatomic, strong) NSDictionary *systemBoldItalicDict;

@end

@implementation CNCPendingTableVC

- (id) initWithCoder: (NSCoder*) aDecoder
{
    self = [super initWithCoder: aDecoder];
    if (self)
    {
        // Custom initialization
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setDateStyle: NSDateFormatterShortStyle];
        
        _timeFormatter = [[NSDateFormatter alloc] init];
        [_timeFormatter setTimeStyle: NSDateFormatterShortStyle];
        
        _phoneFormatter = [[SHSPhoneNumberFormatter alloc] init];
        [_phoneFormatter setDefaultOutputPattern:@"+# (###) ###-####"];
    }
    
    return self;
}

- (void) dealloc
{
    _menuDelegate = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.pendingTableView addSubview: self.refreshControl];
    [self.refreshControl addTarget: self action: @selector(refreshTable) forControlEvents: UIControlEventValueChanged];
    
    NSString *labelText = @"Below is a comprehensive list of quote requests and associated statuses. Once a quote request has been completed by our valuation team, it becomes clickable for more information.";
    NSString *tableInstText = @"  Swipe down to automatically refresh screen.";
    
    UIFont *systemFontNormal = [UIFont systemFontOfSize:17.0];
    UIFont *systemFontBold = [UIFont boldSystemFontOfSize:17.0];
    UIFont *systemFontBoldItalic = [UIFont fontWithName:@"HelveticaNeue-BoldItalic" size:17.0];
    self.systemDict = [NSDictionary dictionaryWithObject: systemFontNormal forKey:NSFontAttributeName];
    self.systemBoldDict = [NSDictionary dictionaryWithObject:systemFontBold forKey:NSFontAttributeName];
    self.systemBoldItalicDict = [NSDictionary dictionaryWithObject:systemFontBoldItalic forKey:NSFontAttributeName];
    
    NSMutableAttributedString *lAttrString = [[NSMutableAttributedString alloc] initWithString:labelText attributes: self.systemDict];
    NSMutableAttributedString *iAttrString = [[NSMutableAttributedString alloc]initWithString: tableInstText attributes:self.systemBoldItalicDict];
    
    [lAttrString appendAttributedString:iAttrString];
    self.headerLabel.attributedText = lAttrString;
    
    self.returningFromSecondaryView = NO;
    
    [self.pendingTableView.layer setBorderWidth:1.0];
    [self.pendingTableView.layer setBorderColor:[[UIColor colorWithWhite: 0.8 alpha: 1.0] CGColor]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUserSignout:)
                                                 name:@"userSignout"
                                               object:nil];
}

#pragma mark - Notifications

- (void)handleUserSignout:(NSNotification *)note {
    self.pendingQuotes = nil;
    [self.pendingTableView reloadData];
}


- (void) refreshTable
{
    [MBProgressHUD showHUDAddedTo:self.parentViewController.view animated:YES];
    
    [[CNCApi sharedClient] pendingWatches: [CNCSettings userId]
                             successBlock: ^(NSArray *quotes, NSError *error, BOOL redirect)
     {
         [MBProgressHUD hideAllHUDsForView: self.parentViewController.view animated: YES];
         
         if (redirect)
         {
             [[NSNotificationCenter defaultCenter] postNotificationName:@"requestSignout" object:self];
             return;
         }

         
         self.pendingQuotes = quotes;
         
         [self.refreshControl endRefreshing];
         [self.pendingTableView reloadData];
         
         
         CGFloat rowsHeight = 0;
         for (int i=0; i< self.pendingQuotes.count; i++)
         {
             CNCQuote *quote = self.pendingQuotes[i];
             if (quote.offerReceived) {
                 rowsHeight += self.pendingTableView.rowHeight;
             } else {
                 rowsHeight += self.pendingTableView.rowHeight - 27;
             }
         }
         
         if (rowsHeight > CGRectGetHeight(self.pendingTableView.frame) - StaticFooterHeight)
         {
             const CGFloat footerHeight = StaticFooterHeight;
             CNCFooterView* footerView = [[CNCFooterView alloc] initWithFrame: CGRectMake(0.0f, 0.0f, 0.0f, footerHeight)];
             self.pendingTableView.tableFooterView = footerView;
             self.footerView.hidden = YES;
         }
         else
         {
             self.pendingTableView.tableFooterView = nil;
             self.footerView.hidden = NO;
         }
     }];
}

- (void) viewDidAppear: (BOOL) animated
{
    [super viewDidAppear: animated];
    if (self.returningFromSecondaryView)
    {
        self.returningFromSecondaryView = NO;
    } else {
        [self refreshTable];
    }
}

#pragma mark - Row Actions

- (void) prepareForSegue: (UIStoryboardSegue*) segue sender: (id) sender
{
    if ([segue.destinationViewController isKindOfClass: [CNCOfferProcessVC class]])
    {
        CNCOfferProcessVC* offerController = (CNCOfferProcessVC*)segue.destinationViewController;
        offerController.delegate = self;
        offerController.menuDelegate = self.menuDelegate;
        
        CNCQuote* quote = self.pendingQuotes[self.selectedIndex];
        [offerController setQuote: quote];
    }
    if ([segue.destinationViewController isKindOfClass: [CNCOfferViewController class]])
    {
        CNCOfferViewController* offerController = (CNCOfferViewController*)segue.destinationViewController;
        
        CNCQuote* quote = self.pendingQuotes[self.selectedIndex];
        [offerController setQuote: quote];
    }
}

- (void)onTimerTextPressed:(UIButton *)sender {
    
    
    const NSInteger selectedRow = sender.tag-1;
    if ((selectedRow >= 0) && (selectedRow < self.pendingQuotes.count))
    {
        CNCQuote* quote = self.pendingQuotes[selectedRow];
        
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle: nil
                                                        message: quote.timerText
                                                       delegate: nil
                                              cancelButtonTitle: @"OK"
                                              otherButtonTitles: nil];
        [alert show];
    }
    
    
}





#pragma mark - Table view data source

- (NSInteger) tableView: (UITableView*) tableView numberOfRowsInSection: (NSInteger) section
{
    // Return 1 for empty list
    return MAX(1, self.pendingQuotes.count);
}

- (NSAttributedString *) makeAttributedStringWithLabel: (NSString *)labelString value:(NSString *)valueString {
    
    NSMutableAttributedString *lAttrString;
    
    lAttrString = [[NSMutableAttributedString alloc] initWithString:labelString attributes: self.systemBoldDict];
    if (valueString) {
        [lAttrString appendAttributedString: [[NSAttributedString alloc] initWithString:valueString attributes:self.systemDict]];
    }
    return lAttrString;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.pendingQuotes.count==0)
    { // possible if we in the "None" cell and there are actually none
        return self.pendingTableView.rowHeight - 27;
    }
    
    CNCQuote* quote = self.pendingQuotes[indexPath.row];
    if (quote.offerReceived) {
        return self.pendingTableView.rowHeight;
    } else {
        return self.pendingTableView.rowHeight - 27;
    }
}


- (IBAction) onImagePressed: (UIButton*) sender
{
    const NSInteger selectedIndex = sender.tag;
    CNCQuote *quote = self.pendingQuotes[selectedIndex];
    
    if (quote.imageURL.length > 0)
    {
        CNCImageViewerViewController *imageVC = [[CNCImageViewerViewController alloc] init];
        [imageVC setImageUrl:quote.imageURL];
        self.returningFromSecondaryView = YES;
        [self presentViewController:imageVC animated:YES completion:nil];
    }
}

- (IBAction) onImage2Pressed: (UIButton*) sender
{
    const NSInteger selectedIndex = sender.tag;
    CNCQuote *quote = self.pendingQuotes[selectedIndex];
    
    if (quote.image2URL.length > 0)
    {
        CNCImageViewerViewController *imageVC = [[CNCImageViewerViewController alloc] init];
        [imageVC setImageUrl:quote.image2URL];
        self.returningFromSecondaryView = YES;
        [self presentViewController:imageVC animated:YES completion:nil];
    }
}



- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if ([alertView tag]== 1 ) {
        if (buttonIndex == 0) {
            // cancel
        } else if (buttonIndex == 1 ){
            //
            [self performSegueWithIdentifier: @"viewOffer" sender: self];
        }
    }
}




- (UITableViewCell*) tableView: (UITableView*) tableView cellForRowAtIndexPath: (NSIndexPath*) indexPath
{
    //	get cell from prototypes
    BOOL quotesExist = self.pendingQuotes.count > 0;
    NSString* identifier = quotesExist ?  @"pendingQuoteCell" : @"emptyCell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: identifier];
    
    // Configure the cell...
    if (quotesExist)
    {
        
        CNCQuote* quote = self.pendingQuotes[indexPath.row];
        CNCPendingQuoteTVC* quoteCell = (CNCPendingQuoteTVC*) cell;
        
        if (quote.offerReceived) {
            quoteCell.offerExpectedInLabel.hidden = YES;
            quoteCell.timerLabel.hidden = YES;
            quoteCell.expiresDateLabel.hidden = NO;
            quoteCell.receivedDateLabel.hidden = NO;
            CGRect rect = quoteCell.associateNameLabel.frame;
            rect.origin.y = CGRectGetMinY(quoteCell.expiresDateLabel.frame)+44;
            quoteCell.associateNameLabel.frame = rect;

        } else {
            quoteCell.offerExpectedInLabel.hidden = NO;
            quoteCell.timerLabel.hidden = NO;
            quoteCell.expiresDateLabel.hidden = YES;
            quoteCell.receivedDateLabel.hidden = YES;
            CGRect rect = quoteCell.associateNameLabel.frame;
            rect.origin.y = CGRectGetMinY(quoteCell.offerExpectedInLabel.frame)+44;
            quoteCell.associateNameLabel.frame = rect;
        }
        
        quoteCell.nameLabel.attributedText = [self makeAttributedStringWithLabel:@"Name: " value:quote.name];
        quoteCell.associateNameLabel.attributedText = [self makeAttributedStringWithLabel:@"Associate: " value:quote.retailerId];
        quoteCell.brandLabel.attributedText = [self makeAttributedStringWithLabel:@"Brand: " value:quote.brand];
        if (quote.phone)
        {
            NSDictionary *phoneDict = [self.phoneFormatter valuesForString:quote.phone];
            quoteCell.phoneLabel.attributedText = [self makeAttributedStringWithLabel:@"Phone: " value:phoneDict[@"text"]];
            
        } else {
            quoteCell.phoneLabel.attributedText = [self makeAttributedStringWithLabel:@"Phone: " value:nil];
        }
        quoteCell.caseIdLabel.attributedText = [self makeAttributedStringWithLabel:@"Case #: " value:quote.caseId];
        
        NSString* dateString;
        if (quote.createdAt) {
            dateString = [NSString stringWithFormat: @"%@ %@",
               [self.dateFormatter stringFromDate: quote.createdAt],
               [self.timeFormatter stringFromDate: quote.createdAt]];
        } else {
            dateString = nil;
        }
        quoteCell.createdDateLabel.attributedText = [self makeAttributedStringWithLabel:@"Case Created: " value:dateString];
        
        if (quote.receivedAt) {
            dateString = [NSString stringWithFormat: @"%@ %@",
                          [self.dateFormatter stringFromDate: quote.receivedAt],
                          [self.timeFormatter stringFromDate: quote.receivedAt]];
        } else {
            dateString = nil;
        }
        quoteCell.receivedDateLabel.attributedText = [self makeAttributedStringWithLabel:@"Offer Received: " value:dateString];

        if (quote.expiresDate) {
            dateString = [NSString stringWithFormat: @"%@ %@",
                          [self.dateFormatter stringFromDate: quote.expiresDate],
                          [self.timeFormatter stringFromDate: quote.expiresDate]];
        } else {
            dateString = nil;
        }
        quoteCell.expiresDateLabel.attributedText = [self makeAttributedStringWithLabel:@"Offer Expires: " value:dateString];
        
        quoteCell.statusLabel.attributedText = [self makeAttributedStringWithLabel:@"Status: " value:quote.status];
        quoteCell.timerLabel.hidden = !quote.hasTimer;
        quoteCell.timerTextButton.hidden = YES;
        
        if (quote.hasTimer)
        {
            quoteCell.timerLabel.startDate = quote.createdAt;
            quoteCell.timerLabel.timeout = Timeout;
        } else {
            if ((quote.timerText) && !quote.offerReceived) {
                [quoteCell.timerTextButton setBackgroundImage: [UIImage imageNamed: @"alert_gray.png"] forState: UIControlStateNormal];
                quoteCell.timerTextButton.hidden = NO;
                quoteCell.timerTextButton.tag=indexPath.row+1;
                [quoteCell.timerTextButton addTarget: self action: @selector(onTimerTextPressed:) forControlEvents: UIControlEventTouchUpInside];
                
            }
        }
        
        quoteCell.watchPhotoButton.hidden = YES;
        quoteCell.watchPhoto.hidden = YES;
        if (quote.imageURL.length > 0)
        {
            NSURL* watchURL = [NSURL URLWithString: quote.imageURL];
            [quoteCell.watchPhoto setImage:nil];
            [quoteCell.watchPhoto setImageWithURL: watchURL];
            quoteCell.watchPhotoButton.tag=indexPath.row;
            quoteCell.watchPhotoButton.hidden = NO;
            quoteCell.watchPhoto.hidden = NO;
        }
        
        quoteCell.watchPhoto2Button.hidden = YES;
        quoteCell.watchPhoto2.hidden = YES;
        if (quote.imageURL.length > 0)
        {
            NSURL* watchURL = [NSURL URLWithString: quote.image2URL];
            [quoteCell.watchPhoto2 setImage:nil];
            [quoteCell.watchPhoto2 setImageWithURL: watchURL];
            quoteCell.watchPhoto2Button.tag=indexPath.row;
            quoteCell.watchPhoto2Button.hidden = NO;
            quoteCell.watchPhoto2.hidden = NO;
        }
        

        
        quoteCell.accessoryType = quote.offerReceived ? UITableViewCellAccessoryDisclosureIndicator : UITableViewCellAccessoryNone;
    }
    
    return cell;
}


- (void) tableView: (UITableView*) tableView didSelectRowAtIndexPath: (NSIndexPath*) indexPath
{
    [tableView deselectRowAtIndexPath: indexPath  animated: YES];
    
    if (indexPath.row < self.pendingQuotes.count)
    {
        CNCQuote* quote = (CNCQuote*) self.pendingQuotes[indexPath.row];
        
        if ([quote.status isEqualToString:@"Additional Info Required"]) {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle: @"Additional Info Required"
                                                            message: quote.notes
                                                           delegate: nil
                                                  cancelButtonTitle: @"OK"
                                                  otherButtonTitles: nil];
            [alert show];
            
        } else if (quote.offerReceived)
        {
            self.selectedIndex = indexPath.row;
            
            [self performSegueWithIdentifier: @"viewOffer" sender: self];
        }
    }
}

- (void) openOfferPage
{
    CNCQuote* quote = (CNCQuote*) self.pendingQuotes[self.selectedIndex];
    CNCOfferProcessVC* offerProcess = (CNCOfferProcessVC*) [self.storyboard instantiateViewControllerWithIdentifier: @"offerProcessViewController"];
    offerProcess.delegate = self;
    offerProcess.menuDelegate = self.menuDelegate;
    
    [offerProcess setQuote: quote];
    [self.navigationController pushViewController: offerProcess animated: YES];
}

#pragma mark UIAlertViewDelegate protocol

- (void) alertView: (UIAlertView*) alertView willDismissWithButtonIndex: (NSInteger) buttonIndex
{
    if (buttonIndex == 1)
    {
        NSString* emailString = [alertView textFieldAtIndex: 0].text;
        CNCQuote* quote = (CNCQuote*) self.pendingQuotes[self.selectedIndex];
        
        if ([emailString.lowercaseString isEqualToString: quote.email.lowercaseString])
        {
            [self openOfferPage];
            
        }
        else
        {
            UIAlertView* alertView =  [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Wrong email address", @"")
                                                                 message: nil
                                                                delegate: nil
                                                       cancelButtonTitle: NSLocalizedString(@"Ok", @"")
                                                       otherButtonTitles: nil];
            [alertView show];
        }
    }
}



#pragma mark CNCOfferProcessVC_Delegate protocol

- (void) offerProcessViewControllerEnd: (CNCOfferProcessVC*) offerVC
{
    //	CNCQuote* quote = (CNCQuote*) self.pendingQuotes[self.selectedIndex];
    //	[quote removeQuoteFromStorage];
    //	[self.pendingQuotesArray removeObjectAtIndex: self.selectedIndex];
    //	
    //	[self.tableView reloadData];
    //	
    //	if (_menuDelegate)
    //	{
    //		[_menuDelegate didSelectWelcomeItem];
    //	}
    //	
    //	[self.navigationController popToViewController: self
    //										  animated: NO];
}

@end
