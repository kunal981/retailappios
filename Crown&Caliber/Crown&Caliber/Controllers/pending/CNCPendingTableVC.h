//
//  CNCPendingTableVC.h
//  Crown&Caliber
//
//  Created by Valeriy on 9/23/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNCMenuViewController.h"
#import "CNCBaseViewController.h"

@interface CNCPendingTableVC : CNCBaseViewController

@property (nonatomic, strong) id<CNCMenuDelegate> menuDelegate;
@property (nonatomic, strong) IBOutlet UITableView* pendingTableView;
@property (nonatomic, strong) IBOutlet UIView* footerView;

@end
