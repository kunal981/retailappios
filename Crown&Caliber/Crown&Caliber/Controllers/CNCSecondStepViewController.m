//
//  CNCSecondStepViewController.m
//  Crown&Caliber
//
//  Created by valerio8 on 09.07.14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCSecondStepViewController.h"
#import "CNCTextField.h"
#import <ActionSheetPicker.h>
#import "UIColor+CNCColor.h"

@interface CNCSecondStepViewController () <UITextFieldDelegate, UIScrollViewDelegate, UITextFieldDelegate>


@property (nonatomic,weak) IBOutlet CNCTextField *customerNameTextfield;
@property (nonatomic,weak) IBOutlet CNCTextField *emailTextField;
@property (nonatomic,weak) IBOutlet CNCTextField *phoneTextField;

@property (nonatomic,weak) IBOutlet CNCTextField *watchBrandTextfield;
@property (nonatomic,weak) IBOutlet CNCTextField *watchModelTextfield;
@property (nonatomic,weak) IBOutlet CNCTextField *modelNumberTextfield;

@property (nonatomic,weak) IBOutlet UISegmentedControl *domesticSegmentedControl;
@property (nonatomic,weak) IBOutlet UISegmentedControl *modelNumberSegmentedControl;
@property (nonatomic,weak) IBOutlet UISegmentedControl *boxPapersSegmentedControl;
@property (nonatomic,weak) IBOutlet UISegmentedControl *sellSegmentedControl;

@property (weak, nonatomic) IBOutlet UILabel *sellLabel;

@property (nonatomic,weak) IBOutlet UIButton *nextButton;


@end

@implementation CNCSecondStepViewController
typedef NS_ENUM(NSUInteger, CNCSecondStepField){
    CNCCustomerName = 1,
    CNCCustomerEmail,
    CNCCustomerPhone,
    CNCWhatchBrand,
    CNCWhatchModel,
    CNCWhatchModelNumber,
};


- (void) viewDidLoad
{
    [super viewDidLoad];
    self.currentQuote.isDomestic = YES;
	self.currentQuote.isFullQuote = YES;
	self.currentQuote.extraInfo = @"";
}

- (void) viewDidAppear: (BOOL) animated
{
	NSDictionary* userInfo = [[NSUserDefaults standardUserDefaults] objectForKey: UserInfoKey];
	if ([userInfo isKindOfClass: [NSDictionary class]])
	{
		self.currentQuote.name = _customerNameTextfield.text =
		[userInfo[UserNameKey] isKindOfClass: [NSString class]] ? userInfo[UserNameKey] : @"";
		
		self.currentQuote.email = _emailTextField.text =
		[userInfo[UserEmailKey] isKindOfClass: [NSString class]] ? userInfo[UserEmailKey] : @"";
		
		self.currentQuote.phone = _phoneTextField.text =
		[userInfo[UserPhoneNumberKey] isKindOfClass: [NSString class]] ? userInfo[UserPhoneNumberKey] : @"";
	}
	[super viewDidAppear: animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction) nextButtonPressed:(id)sender
{
    [self.delegate goToThirdStep];
    [self.view endEditing:YES];
}

-(IBAction)segmentedControlerChanged:(id)sender
{
    
    UISegmentedControl *segmentedControl = sender;
    
    switch (segmentedControl.selectedSegmentIndex) {
        case 0:
            self.watchModelTextfield.returnKeyType = UIReturnKeyNext;
            self.currentQuote.hasModelNumber = YES;
            [self animateSubmitButtonVerticallyDirectionUp:NO];
            
            break;
        case 1:
            self.watchModelTextfield.returnKeyType = UIReturnKeyDone;
            self.currentQuote.hasModelNumber = NO;
            [self animateSubmitButtonVerticallyDirectionUp:YES];
            break;
            
        default:
            break;
    }
    [self checkNextButtonAvailability: self.currentQuote];
    
}

-(void) animateSubmitButtonVerticallyDirectionUp:(BOOL)directionUp
{
    CGRect frame = self.nextButton.frame;
    CGRect segmentedControlFrame = self.boxPapersSegmentedControl.frame;
	CGRect sellLabelFrame = self.sellLabel.frame;
	CGRect sellSegmentedControlFrame = self.sellSegmentedControl.frame;
    
    float moveHeight = 60;

    frame.origin.y += (directionUp)? - moveHeight : moveHeight;
    segmentedControlFrame.origin.y += (directionUp)? -moveHeight:moveHeight;
	sellLabelFrame.origin.y += (directionUp)? -moveHeight:moveHeight;
	sellSegmentedControlFrame.origin.y += (directionUp)? -moveHeight:moveHeight;
    [self.view.layer removeAllAnimations];

    
    
    [self.modelNumberTextfield setHidden:YES];

    [UIView animateWithDuration:0.5 animations:^{

        [self.modelNumberTextfield setHidden:YES];
        
        self.nextButton.frame = frame;
        self.boxPapersSegmentedControl.frame = segmentedControlFrame;
		self.sellLabel.frame = sellLabelFrame;
		self.sellSegmentedControl.frame = sellSegmentedControlFrame;
    }
    completion:^(BOOL finished) {
        if (!directionUp)
        {
            [self.modelNumberTextfield setHidden:NO];
        }
    }];
}

-(IBAction)domesticSegmentedControlChanged :(id)sender
{
    UISegmentedControl *segmentedControl = sender;
    
    switch (segmentedControl.selectedSegmentIndex)
    {
        case 0:
            self.currentQuote.isDomestic = YES;
            
        case 1:
            self.currentQuote.isDomestic = NO;
    }
}

-(IBAction)sellSegmentedControlChanged :(id)sender
{
    UISegmentedControl *segmentedControl = sender;

    switch (segmentedControl.selectedSegmentIndex)
    {
        case 0:
            self.currentQuote.isFullQuote = YES;

        case 1:
            self.currentQuote.isFullQuote = NO;
    }
}

- (IBAction) boxPapersSegmentedControlerChanged: (UISegmentedControl*) sender
{
	NSString* documentsParam = nil;
	switch (sender.selectedSegmentIndex)
	{
		case 0:
			documentsParam = @"Box Only";
			break;
		case 1:
			documentsParam = @"Papers Only";
			break;
		case 2:
			documentsParam = @"Box & Papers";
			break;
		case 3:
			documentsParam = @"Neither";
			break;

		default:
			break;
	}
    self.currentQuote.documents = documentsParam;
	[self checkNextButtonAvailability: self.currentQuote];
}

- (IBAction)textChanged:(JVFloatLabeledTextField *)sender {
  switch (sender.tag) {
    case CNCCustomerName:
      self.currentQuote.name = sender.text;
      break;
    case CNCCustomerEmail:
      self.currentQuote.email = sender.text;
      break;
    case CNCCustomerPhone: {
      sender.text = sender.text.length > 15? [sender.text substringToIndex:15]:sender.text;
      self.currentQuote.phone = sender.text;
      break;
    }
    case CNCWhatchBrand:
      self.currentQuote.brand = sender.text;
      break;
    case CNCWhatchModel:
      self.currentQuote.modelName = sender.text;
      break;
    case CNCWhatchModelNumber:
      self.currentQuote.modelNumber = sender.text;
      
      break;
      
    default:
      break;
  }
  
  [self checkNextButtonAvailability: self.currentQuote];
}

- (IBAction)textFieldDidEndEditing:(JVFloatLabeledTextField *)sender {
    switch (sender.tag) {
        case CNCCustomerName:
            sender.layer.borderColor = [self validateName:sender.text]? [UIColor CNCGrayColor].CGColor : [UIColor redColor].CGColor;
            break;
        case CNCCustomerEmail:
            sender.layer.borderColor = [self validateEmail:sender.text]? [UIColor CNCGrayColor].CGColor : [UIColor redColor].CGColor;
            break;
        case CNCCustomerPhone:
            sender.layer.borderColor = [self validatePhone:sender.text]? [UIColor CNCGrayColor].CGColor : [UIColor redColor].CGColor;
            break;
        case CNCWhatchModelNumber:
            sender.layer.borderColor = [self validateModelNumber:sender.text]? [UIColor CNCGrayColor].CGColor : [UIColor redColor].CGColor;
            break;
            
        default:
            break;
    }

}


- (void)checkNextButtonAvailability:(CNCQuote*)quote {
	BOOL condition =  self.boxPapersSegmentedControl.selectedSegmentIndex != UISegmentedControlNoSegment && ([self validateName:quote.name] && [self validateEmail:quote.email] && [self validatePhone:quote.phone] && quote.brand.length > 0 && (quote.hasModelNumber? [self validateModelNumber:quote.modelNumber]:YES));
	
    if (condition) {
		NSDictionary* userInfo = @{UserNameKey : quote.name, UserEmailKey : quote.email, UserPhoneNumberKey : quote.phone};
		[[NSUserDefaults standardUserDefaults] setObject: userInfo forKey: UserInfoKey];
		
        self.nextButton.enabled = YES;
        [self.delegate enableSubmitButton];
    }
    else
    {
        self.nextButton.enabled = NO;
        [self.delegate disableSubmitButton];
    }
}

-(void) deleteUserData
{
    self.customerNameTextfield.text = nil;
    self.emailTextField.text = nil;
    self.phoneTextField.text = nil;
    self.watchBrandTextfield.text = nil;
    self.modelNumberTextfield.text = nil;
    self.watchModelTextfield.text = nil;
    self.domesticSegmentedControl.selectedSegmentIndex = 0;
    self.nextButton.enabled = NO;
    
    for (CNCTextField *textField in self.view.subviews) {
        textField.layer.borderColor = [UIColor CNCGrayColor].CGColor;
    }
    
    if (self.modelNumberSegmentedControl.selectedSegmentIndex == 1) {
        [self animateSubmitButtonVerticallyDirectionUp:NO];
        
    }
    self.modelNumberSegmentedControl.selectedSegmentIndex = 0;
    self.boxPapersSegmentedControl.selectedSegmentIndex = UISegmentedControlNoSegment;
}

-(BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];    //  return 0;
    return ([emailTest evaluateWithObject:candidate] && candidate.length>3 && candidate.length<255);
}

- (IBAction)showBrandPicker:(UIButton*)sender {
    
    NSString *pickerResourcePath = [[NSBundle mainBundle]pathForResource: @"WatchesBrand" ofType:@"plist"];
    NSArray* watchBrandes = [[NSArray alloc]initWithContentsOfFile:pickerResourcePath];
    
    __weak CNCSecondStepViewController *weakSelf = self;
    ActionSheetStringPicker* picker = [[ActionSheetStringPicker alloc] initWithTitle:nil rows:watchBrandes initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        self.currentQuote.brand = watchBrandes[selectedIndex];
        self.watchBrandTextfield.text = self.currentQuote.brand;
        [weakSelf checkNextButtonAvailability:self.currentQuote];
        [self.watchModelTextfield becomeFirstResponder];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        //
    } origin:[self.delegate view]];
    
    [picker showActionSheetPicker];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    switch (textField.tag) {
        case CNCCustomerName:
            [self.emailTextField becomeFirstResponder];
            break;
        case CNCCustomerEmail:
            [self.phoneTextField becomeFirstResponder];
            break;
        case CNCCustomerPhone:
            [self showBrandPicker:nil];
            break;

        case CNCWhatchModel:
            if (self.modelNumberSegmentedControl.selectedSegmentIndex == 0) {
                [self.modelNumberTextfield becomeFirstResponder];
            }
            else {
                [self.view endEditing:YES];
            }
            
            break;
        case CNCWhatchModelNumber:
            [self.view endEditing:YES];

            break;
        default:
            break;
    }
    
    return YES;
}

#define PHONEACCEPTEDSYMBOLS @"1234567890+-()"

- (BOOL) validatePhone:(NSString*) phone
{
    NSCharacterSet *unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:PHONEACCEPTEDSYMBOLS] invertedSet];
    
    NSRange range = [phone rangeOfCharacterFromSet:unacceptedInput];
    
    return (phone.length >= 10 && phone.length <= 15 && range.location == NSNotFound);
}

- (BOOL) validateName:(NSString*) name
{
    return (name.length>0&& name.length<256);
}

- (BOOL) validateModelNumber:(NSString*) modelNumber
{
    return (modelNumber.length>2 && modelNumber.length<31);
}

- (BOOL) validateModelName:(NSString*) modelName
{
  return YES;
}

@end
