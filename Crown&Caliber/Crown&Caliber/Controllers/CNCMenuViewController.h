//
//  CNCMenuViewController.h
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNCQuote.h"

@protocol CNCMenuDelegate <NSObject>

- (void)didSelectWelcomeItem;
- (void)didSelectNewQuoteItem;
- (void)didSelectBlogItem;
- (void)didSelectHowItem;
- (void)didSelectPendingQuotesItem;
- (void)didSelectWatchesToShipItem;
- (void)didSelectHistoryInfoItem;
- (void)didSelectSignOutItem;
- (void)didSelectTermsAndConditionsItem;
- (void)didSelectPrivacyPolicyItem;
- (void)didSelectThankYouItem;
- (void)didSelectContactsItem;
- (void)didSelectFailItemWithQuote:(CNCQuote*)quote;
- (void)allowSwipes:(BOOL)allow;
- (void)loadWelcomeScreen;
- (void)changeToNormalRetailMenu;
- (void)changeToRepRetailMenu;

@end

@interface CNCMenuViewController : UITableViewController

@property (nonatomic, weak) id<CNCMenuDelegate> menuDelegate;

- (void)changeToNormalRetailMenu;
- (void)changeToRepRetailMenu;

@end
