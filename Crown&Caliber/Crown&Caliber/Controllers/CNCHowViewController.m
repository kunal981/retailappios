//
//  CNCHowViewController.m
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCHowViewController.h"
#import <MediaPlayer/MPMoviePlayerController.h>
#import <MediaPlayer/MPMoviePlayerViewController.h>
#import "Config.h"

#import "CNCTabBarController.h"

@interface CNCHowViewController () <UIGestureRecognizerDelegate>

@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
@property (nonatomic, weak) IBOutlet UIImageView *movieView;
@property (nonatomic, strong) UITapGestureRecognizer *movieTap;
@property (nonatomic, strong) NSString *videoUrl;

@property (nonatomic, strong) UIScrollView *scrollView;


@end

@implementation CNCHowViewController


- (void)viewDidLoad
{
    [super viewDidLoad];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale == 2.0)) {
            self.videoUrl = VIDEO3_LINK;
            // Retina display
        } else {
            self.videoUrl = VIDEO2_LINK;
            // non-Retina display
        }
        self.movieView.image = [UIImage imageNamed:@"video_screenshot_ipadretina.png"];
    }
    else
    {
        self.videoUrl = VIDEO1_LINK;
        self.movieView.image = [UIImage imageNamed:@"video_screenshot_iphone.png"];
    }
    
    
//    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:url]];
//    
//    
//    self.moviePlayer.view.frame = CGRectMake(0, 0, self.movieView.frame.size.width, self.movieView.frame.size.height);
//    [self.movieView addSubview:self.moviePlayer.view];
    //[self.moviePlayer prepareToPlay];
    
    self.movieTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(movieTapAction)];
    self.movieTap.numberOfTapsRequired = 1;
    self.movieTap.delegate = self;
    [self.movieView addGestureRecognizer:self.movieTap];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    NSLog(@"bounds: %@", NSStringFromCGRect(self.view.bounds));
    NSLog(@"frame: %@", NSStringFromCGRect(self.view.frame));
    
    CNCTabBarController *tabController = (CNCTabBarController*)self.tabBarController;
    NSLog(@"tab bar frame: %@", NSStringFromCGRect(tabController.tabBar.frame));
    
    
    if ( nil == self.scrollView ) {
        
        CGRect frame = self.view.bounds;
        frame.size.height -= tabController.tabBar.frame.size.height;
        
        NSLog(@"scroll view frame: %@", NSStringFromCGRect(frame));
        
        CGFloat contentHeight = frame.size.height;
        self.scrollView = [[UIScrollView alloc] initWithFrame: frame];
        self.scrollView.alwaysBounceVertical = YES;
        [self.view insertSubview: self.scrollView atIndex: 0];
        
        for ( UIView *view in self.view.subviews ) {
            
            if ( view != self.scrollView ) {
                [view removeFromSuperview];
                [self.scrollView addSubview: view];
                
                if ( contentHeight < view.frame.origin.y + view.frame.size.height ) {
                    contentHeight = view.frame.origin.y + view.frame.size.height;
                }
            }
        }
        
        self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, contentHeight);
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) movieTapAction
{
    MPMoviePlayerViewController *movieController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:self.videoUrl]];
    [movieController.moviePlayer prepareToPlay];
    [self presentMoviePlayerViewControllerAnimated:movieController];
    
}

@end
