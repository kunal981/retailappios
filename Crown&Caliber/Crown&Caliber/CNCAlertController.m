//
//  CNCAlertController.m
//  CrownAndCaliber
//
//  Created by User on 13.10.15.
//  Copyright © 2015 SROST Studio. All rights reserved.
//

#import "CNCAlertController.h"

@implementation CNCAlertController

- (void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    //set this to whatever color you like...
    self.view.tintColor = [UIColor blackColor];
}

@end
