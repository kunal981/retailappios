//
//  CNCQuoteProcessor.h
//  Crown&Caliber
//
//  Created by Valeriy on 9/23/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CNCQuote;

@interface CNCQuoteProcessor : NSObject

+ (void) saveQuoteInfo: (NSDictionary*) quoteInfo;
+ (void) deleteQuoteWIthIndex: (NSInteger) index;
+ (NSArray*) quotesInfo;

+ (void) saveDriverLicense: (UIImage*) driverLicenseImage
                  forQuote: (NSString*) caseId;
+ (UIImage*) quoteDriverLicenseWithQuoteId: (NSString*) caseId;


+ (void) deleteQuote: (CNCQuote*) quote;

@end
