//
//  CNCQuoteProcessor.m
//  Crown&Caliber
//
//  Created by Valeriy on 9/23/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCQuoteProcessor.h"
#import "CNCQuote.h"

static NSString* QuotesDirectoryName = @"quotes";

static NSString* QuotesContainerName = @"quotesContainer";

@implementation CNCQuoteProcessor

+ (void) createQuotesDirectoryIfNeed
{
	
	NSFileManager* defaultManager = [NSFileManager defaultManager];
	
	NSString* filePath = [self quotesDirectoryPath];
	if (![defaultManager fileExistsAtPath: filePath])
	{
		[defaultManager createDirectoryAtPath: filePath
				  withIntermediateDirectories: NO
								   attributes: nil
										error: nil];
	}
}

+ (void) saveQuoteInfo: (NSDictionary*) quoteInfo
{
	NSMutableArray* quotes = nil;
	
	NSFileManager* defaultManager = [NSFileManager defaultManager];
	
	NSString* filePath = [self quotesFilePath];
	if ([defaultManager fileExistsAtPath: filePath])
	{
		quotes = [[[NSArray alloc] initWithContentsOfFile: filePath] mutableCopy];
		[quotes addObject: quoteInfo];
	}
	else
	{
		[self createQuotesDirectoryIfNeed];
		quotes = [[NSMutableArray alloc] initWithObjects: quoteInfo, nil];
	}
	
	[quotes writeToFile: filePath atomically: YES];
}

+ (void) deleteQuote: (CNCQuote*) quote
{
	NSMutableArray* quotes = nil;
	
	NSFileManager* defaultManager = [NSFileManager defaultManager];
	
	NSString* filePath = [self quotesFilePath];
	if ([defaultManager fileExistsAtPath: filePath])
	{
		quotes = [[[NSArray alloc] initWithContentsOfFile: filePath] mutableCopy];
		NSDictionary* quoteForRemove = nil;
		for (NSDictionary* savedQuote in quotes)
		{
			NSString* savedCaseId = savedQuote[QuoteCaseIdKey];
			if ([savedCaseId isEqualToString: quote.caseId])
			{
				quoteForRemove = savedQuote;
			}
		}
		if (quoteForRemove)
		{
			[self deleteDriverLicenseForQuote: quoteForRemove[QuoteCaseIdKey]];
			[quotes removeObject: quoteForRemove];
			[quotes writeToFile: filePath atomically: YES];
		}
	}
}

+ (void) deleteQuoteWIthIndex: (NSInteger) index
{
	NSMutableArray* quotes = nil;
	
	NSFileManager* defaultManager = [NSFileManager defaultManager];
	
	NSString* filePath = [self quotesFilePath];
	if ([defaultManager fileExistsAtPath: filePath])
	{
		quotes = [[[NSArray alloc] initWithContentsOfFile: filePath] mutableCopy];
		if (index < quotes.count)
		{
			CNCQuote* quote = quotes[index];
            [self deleteDriverLicenseForQuote: quote.caseId];
			[quotes removeObjectAtIndex: index];
		}
		[quotes writeToFile: filePath atomically: YES];
	}
}

+ (NSString*) quotesDirectoryPath
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
														 NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString* directoryPaath = [documentsDirectory stringByAppendingPathComponent: QuotesDirectoryName];
	return directoryPaath;
}

+ (NSString*) quotesFilePath
{
	return [[self quotesDirectoryPath] stringByAppendingPathComponent: QuotesContainerName];
}

+ (NSArray*) quotesInfo
{
	NSMutableArray* quotes = nil;
	
	NSFileManager* defaultManager = [NSFileManager defaultManager];
	
	NSString* filePath = [self quotesFilePath];
	if ([defaultManager fileExistsAtPath: filePath])
	{
		quotes = [[[NSArray alloc] initWithContentsOfFile: filePath] mutableCopy];
	}
	else
	{
		quotes = [[NSMutableArray alloc] init];
	}
	
	return quotes;
}


+ (NSString*) quoteDriverLicensePathWithQuoteId: (NSString*) caseId
{
    return [[self quotesDirectoryPath] stringByAppendingPathComponent: caseId];
}

+ (void) saveDriverLicense: (UIImage*) driverLicenseImage forQuote: (NSString*) caseId
{
    NSString* drivelLicensePath = [self quoteDriverLicensePathWithQuoteId: caseId];
    
    [self createQuotesDirectoryIfNeed];
    
    NSData* imageData = UIImageJPEGRepresentation(driverLicenseImage, 1.0);
    
    [imageData writeToFile: drivelLicensePath atomically: YES];
}

+ (UIImage*) quoteDriverLicenseWithQuoteId: (NSString*) caseId
{
    NSString* drivelLicensePath = [self quoteDriverLicensePathWithQuoteId: caseId];
    
    UIImage* driverLicenseImage = [UIImage imageWithData: [NSData dataWithContentsOfFile: drivelLicensePath]];
    
    return driverLicenseImage;
}

+ (void) deleteDriverLicenseForQuote:  (NSString*) caseId
{
    NSString* drivelLicensePath = [self quoteDriverLicensePathWithQuoteId: caseId];
    
    NSFileManager* defaultManager = [NSFileManager defaultManager];
    if ([defaultManager fileExistsAtPath: drivelLicensePath])
    {
        [defaultManager removeItemAtPath: drivelLicensePath error: nil];
    }
}

@end
