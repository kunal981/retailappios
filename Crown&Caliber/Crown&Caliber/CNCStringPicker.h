//
//  CNCStringPicker.h
//  Crown&Caliber
//

#import "AbstractActionSheetPicker.h"

@class CNCStringPicker;
typedef void(^CNCStringPickerDoneBlock)(CNCStringPicker *picker, NSInteger selectedIndex, id selectedValue);
typedef void(^CNCStringPickerCancelBlock)(CNCStringPicker *picker);

@interface CNCStringPicker : AbstractActionSheetPicker

+ (id)showPickerWithTitle:(NSString *)title rows:(NSArray *)strings initialSelection:(NSInteger)index doneBlock:(CNCStringPickerDoneBlock)doneBlock cancelBlock:(CNCStringPickerCancelBlock)cancelBlock origin:(id)origin;

- (id)initWithTitle:(NSString *)title rows:(NSArray *)strings initialSelection:(NSInteger)index doneBlock:(CNCStringPickerDoneBlock)doneBlock cancelBlock:(CNCStringPickerCancelBlock)cancelBlockOrNil origin:(id)origin;

@property (nonatomic, copy) CNCStringPickerDoneBlock onCNCStringPickerDone;
@property (nonatomic, copy) CNCStringPickerCancelBlock onCNCStringPickerCancel;

@end
