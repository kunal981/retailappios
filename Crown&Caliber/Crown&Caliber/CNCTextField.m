//
//  CNCTextField.m
//  Crown&Caliber
//
//  Created by valerio8 on 11.07.14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCTextField.h"
#import "UIColor+CNCColor.h"

@implementation CNCTextField

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.layer.cornerRadius = 0.0f;
    self.layer.masksToBounds = YES;
    [self.layer setBorderColor:[[UIColor CNCGrayColor] CGColor]];
    
    self.tintColor = [UIColor blackColor];
    
    self.layer.borderWidth = 1.0f;
}


- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 10 , 0 );
}

-(CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 10 , 0 );
}

@end


static NSString* const DefaultPhoneNumberFormat = @"(###) ###-####";
static NSInteger const MaxPhoneLengthForFormatting = 11;

@interface CNCPhoneTextField ()

@property (strong, nonatomic) NSCharacterSet* phoneNumberSymbols;

@property (strong, nonatomic) SHSPhoneNumberFormatter* formatter;

@property (strong, nonatomic) NSCharacterSet* availableCharacters;

@end

@implementation CNCPhoneTextField

- (void) awakeFromNib
{
    [super awakeFromNib];
    self.layer.cornerRadius = 0.0f;
    self.layer.masksToBounds = YES;
    [self.layer setBorderColor:[[UIColor CNCGrayColor] CGColor]];

    self.tintColor = [UIColor blackColor];

    self.layer.borderWidth = 1.0f;
    
    self.phoneNumberSymbols  = [NSCharacterSet characterSetWithCharactersInString: @"#*+"];
    self.availableCharacters = [NSCharacterSet characterSetWithCharactersInString: @"+*#0123456789"];
    
    self.formatter = [[SHSPhoneNumberFormatter alloc] init];
    [self.formatter setDefaultOutputPattern: DefaultPhoneNumberFormat];
    
    
    [self addTarget: self
             action: @selector(editingChanged:)
   forControlEvents: UIControlEventEditingChanged];
}

- (void) dealloc {
    
    [self removeTarget: self
                action: @selector(editingChanged:)
      forControlEvents: UIControlEventEditingChanged];
    
    _phoneNumberSymbols  = nil;
    _availableCharacters = nil;
    _formatter = nil;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 10 , 0 );
}

-(CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 10 , 0 );
}

- (void) editingChanged: (CNCPhoneTextField*) sender {
    
    if (sender == self) {
        
        NSString* text = self.text;
        NSString* didgitsonly = [self.formatter digitOnlyString: text];
        
        BOOL isFormatting = (didgitsonly.length < MaxPhoneLengthForFormatting) &&
                            ([text rangeOfCharacterFromSet: self.phoneNumberSymbols].location == NSNotFound);
        
        if (isFormatting) {
            
            NSDictionary* result = [self.formatter valuesForString: text];
            self.text = result[@"text"];
        } else {
            
            text = [[text componentsSeparatedByCharactersInSet: [self.availableCharacters invertedSet]] componentsJoinedByString: @""];
            
            self.text = text;
        }
    }
}

@end
