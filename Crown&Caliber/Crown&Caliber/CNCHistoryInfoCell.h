//
//  CNHistoryInfoCell.h
//  Crown&Caliber
//

#import <UIKit/UIKit.h>

@interface CNCHistoryInfoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel* nameLabel;
@property (weak, nonatomic) IBOutlet UILabel* phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel* brandLabel;
@property (weak, nonatomic) IBOutlet UIImageView* watchPhoto;
@property (weak, nonatomic) IBOutlet UIButton* watchPhotoButton;
@property (weak, nonatomic) IBOutlet UIImageView* watchPhoto2;
@property (weak, nonatomic) IBOutlet UIButton* watchPhoto2Button;

@property (weak, nonatomic) IBOutlet UILabel* caseIdLabel;
@property (weak, nonatomic) IBOutlet UILabel* createdDateLabel;

@property (weak, nonatomic) IBOutlet UILabel* statusLabel;

@property (weak, nonatomic) IBOutlet UILabel* expiresDateLabel;

@property (weak, nonatomic) IBOutlet UILabel* acceptedDateLabel;
@property (weak, nonatomic) IBOutlet UILabel* amountLabel;

@property (weak, nonatomic) IBOutlet UILabel* markedShippedLabel;
@property (weak, nonatomic) IBOutlet UILabel* trackingNumberLabel;

@property (weak, nonatomic) IBOutlet UILabel* shippedDateLabel;
@property (weak, nonatomic) IBOutlet UIButton* shippedButton;

@property (weak, nonatomic) IBOutlet UILabel* associateNameLabel;

@end
