//
//  CNCAppOptions.h
//  Crown&Caliber
//
//  Created by Oleg Lavrentyev on 12/1/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CNCApi.h"

@interface CNCAppOptions : NSObject

@property (nonatomic, assign) AppSizeGroup appSize;
@property (nonatomic, assign) BOOL managersApproval;
@property (nonatomic, assign) BOOL showDLOnQuote;
@property (nonatomic, strong) NSString* logoURL;
@property (nonatomic, strong) NSString* screensaverURL;
@property (nonatomic, strong) NSDictionary* brands;
@property (nonatomic, strong) NSArray* declineReasons;
@property (nonatomic, strong) NSDictionary* modelsDescription;
@property (nonatomic, strong) NSArray* reps;
@property (nonatomic, strong) NSString* terms;

+ (CNCAppOptions*) sharedInstance;

@end
