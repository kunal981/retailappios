//
//  menuTableViewCell.h
//  CrownNCaliber
//
//  Created by Admin on 07.07.14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNCMenuTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *menuImage;
@property (weak, nonatomic) IBOutlet UIView *selectedStateView;
@property (weak, nonatomic) IBOutlet UIView *separationView;

- (void)configureCellWithDictionary:(NSDictionary*)dictionary isSelected:(BOOL)isSelected isFirstItem:(BOOL)isFirstItem;
@end
