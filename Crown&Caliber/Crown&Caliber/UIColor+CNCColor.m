//
//  UIColor+CNCColor.m
//  Crown&Caliber
//
//  Created by valerio8 on 09.07.14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "UIColor+CNCColor.h"

@implementation UIColor (CNCColor)

+ (UIColor *)CNCGrayColor
{
    static UIColor *CNCGrayColor = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        CNCGrayColor = [UIColor colorWithRed:.74 green:.74 blue:.74 alpha:1];
    });
    return CNCGrayColor;
}

+ (UIColor *)CNCOrangeColor
{
    static UIColor *CNCOrangeColor = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        CNCOrangeColor = [UIColor colorWithRed:1 green:.63 blue:.15 alpha:1];
    });
    return CNCOrangeColor;
}

@end
