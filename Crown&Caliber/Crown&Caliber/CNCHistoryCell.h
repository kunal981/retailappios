//
//  CNCHistoryCell.h
//  Crown&Caliber
//

#import <UIKit/UIKit.h>

@interface CNCHistoryCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel* priceLabel;
@property (nonatomic, strong) IBOutlet UILabel* dateOfSaleLabel;
@property (nonatomic, strong) IBOutlet UILabel* brandLabel;
@property (nonatomic, strong) IBOutlet UILabel* modelLabel;
@property (nonatomic, strong) IBOutlet UILabel* modelNumLabel;
@property (nonatomic, strong) IBOutlet UILabel* salesFormatLabel;


@end
