//
//  CNCHistoryInfoViewController.m
//  Crown&Caliber
//

#import "CNCHistoryInfoViewController.h"
#import "CNCHistoryInfoCell.h"
#import "CNCOfferViewController.h"
#import "MBProgressHUD.h"
#import "CNCApi.h"
#import "CNCQuote.h"
#import "CNCSettings.h"
#import "CNCFooterView.h"
#import "SHSPhoneNumberFormatter.h"
#import "SHSPhoneNumberFormatter+UserConfig.h"
#import "CNCImageViewerViewController.h"
#import "UIImageView+AFNetworking.h"


@interface CNCHistoryInfoViewController()

@property (nonatomic, strong) NSArray* historyInfo;

@property (nonatomic, strong) NSDateFormatter* dateFormatter;
@property (nonatomic, strong) NSDateFormatter* timeFormatter;
@property (nonatomic, strong) SHSPhoneNumberFormatter* phoneFormatter;
@property (nonatomic, strong) UIRefreshControl* refreshControl;
@property (nonatomic, strong) IBOutlet UILabel* headerLabel;
@property (nonatomic, unsafe_unretained) NSInteger selectedIndex;
@property (nonatomic) BOOL returningFromSecondaryView;

@property (nonatomic, strong) NSDictionary *systemDict;
@property (nonatomic, strong) NSDictionary *systemBoldDict;
@property (nonatomic, strong) NSDictionary *systemBoldItalicDict;

typedef NS_ENUM(NSInteger, watchStatusType) {
    StatusShipped,
    StatusToBeShipped,
    StatusDeclinedByCustomer,
    StatusExpired,
    StatusReceived,
    StatusUndecided,
    StatusWillNotQuote,
    StatusQuoteReceived,
    StatusAdditionalInfoRequired,
    StatusUnknown
};

@end

@implementation CNCHistoryInfoViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        // Custom initialization
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setDateStyle: NSDateFormatterShortStyle];
        
        self.timeFormatter = [[NSDateFormatter alloc] init];
        [self.timeFormatter setTimeStyle: NSDateFormatterShortStyle];

        self.phoneFormatter = [[SHSPhoneNumberFormatter alloc] init];
        [self.phoneFormatter setDefaultOutputPattern:@"+# (###) ###-####"];
    }
    return self;
}


- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.historyInfoTableView addSubview: self.refreshControl];
    [self.refreshControl addTarget: self action: @selector(refreshTable) forControlEvents: UIControlEventValueChanged];
    
    NSString *labelText = @"Below is a list of all offers within the last 30 days that were either declined, expired or have been received by Crown & Caliber.";

    NSString *tableInstText = @"  Swipe down to automatically refresh screen.";
    
    UIFont *systemFontNormal = [UIFont systemFontOfSize:17.0];
    UIFont *systemFontBold = [UIFont boldSystemFontOfSize:17.0];
    UIFont *systemFontBoldItalic = [UIFont fontWithName:@"HelveticaNeue-BoldItalic" size:17.0];
    self.systemDict = [NSDictionary dictionaryWithObject: systemFontNormal forKey:NSFontAttributeName];
    self.systemBoldDict = [NSDictionary dictionaryWithObject:systemFontBold forKey:NSFontAttributeName];
    self.systemBoldItalicDict = [NSDictionary dictionaryWithObject:systemFontBoldItalic forKey:NSFontAttributeName];

    NSMutableAttributedString *lAttrString = [[NSMutableAttributedString alloc] initWithString:labelText attributes: self.systemDict];
    NSMutableAttributedString *iAttrString = [[NSMutableAttributedString alloc]initWithString: tableInstText attributes:self.systemBoldItalicDict];
    
    [lAttrString appendAttributedString:iAttrString];
    self.headerLabel.attributedText = lAttrString;

    self.returningFromSecondaryView = NO;
    
    [self.historyInfoTableView.layer setBorderWidth:1.0];
    [self.historyInfoTableView.layer setBorderColor:[[UIColor colorWithWhite: 0.8 alpha: 1.0] CGColor]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUserSignout:)
                                                 name:@"userSignout"
                                               object:nil];
}


#pragma mark - Notifications

- (void)handleUserSignout:(NSNotification *)note {
    self.historyInfo = nil;
    [self.historyInfoTableView reloadData];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void) refreshTable
{
    [MBProgressHUD showHUDAddedTo:self.parentViewController.view animated:YES];
    
    [[CNCApi sharedClient] historyInfo: [CNCSettings userId]
                            successBlock: ^(NSArray *historyInfo, NSError *error, BOOL redirect)
     {
         [MBProgressHUD hideAllHUDsForView: self.parentViewController.view animated: YES];
         
         if (redirect)
         {
             [[NSNotificationCenter defaultCenter] postNotificationName:@"requestSignout" object:self];
             return;
         }
         
         
         self.historyInfo = historyInfo;
         
         [self.refreshControl endRefreshing];
         [self.historyInfoTableView reloadData];
         
         
         CGFloat rowsHeight = 0;
         for (int i=0; i< self.historyInfo.count; i++)
         {
             CNCQuote *watch = self.historyInfo[i];
             switch ([self getStatusTypeForWatch:watch])
             {
                 case StatusShipped:
                 case StatusToBeShipped:
                 case StatusReceived:
                     rowsHeight += self.historyInfoTableView.rowHeight;
                     break;
                     
                 case StatusExpired:
                 case StatusDeclinedByCustomer:
                 case StatusUndecided:
                 case StatusQuoteReceived:
                 case StatusWillNotQuote:
                 case StatusAdditionalInfoRequired:
                 default:
                     rowsHeight += self.historyInfoTableView.rowHeight - 85;
                     break;
             }
         }
         

         if (rowsHeight > CGRectGetHeight(self.historyInfoTableView.frame) - StaticFooterHeight)
         {
             const CGFloat footerHeight = (rowsHeight > CGRectGetHeight(self.historyInfoTableView.frame)) ? 100.0f : CGRectGetHeight(self.historyInfoTableView.frame) - rowsHeight;
             CNCFooterView* footerView = [[CNCFooterView alloc] initWithFrame: CGRectMake(0.0f, 0.0f, 0.0f, footerHeight)];
             self.historyInfoTableView.tableFooterView = footerView;
             self.footerView.hidden = YES;
         }
         else
         {
             self.historyInfoTableView.tableFooterView = nil;
             self.footerView.hidden = NO;
         }
     }];
}

- (void) viewDidAppear: (BOOL) animated
{
    [super viewDidAppear: animated];
    if (self.returningFromSecondaryView)
    {
        self.returningFromSecondaryView = NO;
    } else {
        [self refreshTable];
    }
}

- (void) displayAlert: (NSString*) message withTitle: (NSString*) title
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle: title
                                                    message: message
                                                   delegate: nil
                                          cancelButtonTitle: @"OK"
                                          otherButtonTitles: nil];
    [alert setTag: 0];
    [alert show];
}

- (NSAttributedString *) makeAttributedStringWithLabel: (NSString *)labelString value:(NSString *)valueString {
    
    NSMutableAttributedString *lAttrString;
    
    lAttrString = [[NSMutableAttributedString alloc] initWithString:labelString attributes: self.systemBoldDict];
    if (valueString) {
        [lAttrString appendAttributedString: [[NSAttributedString alloc] initWithString:valueString attributes:self.systemDict]];
    }
    return lAttrString;
}

#pragma mark - Events

- (void) onShipPressed: (UIButton*) sender
{
    const NSInteger selectedRow = sender.tag - 1;
    if ((selectedRow >= 0) && (selectedRow < self.historyInfo.count))
    {
        CNCQuote* watch = self.historyInfo[selectedRow];
        
        [MBProgressHUD showHUDAddedTo:self.parentViewController.view animated:YES];
        
        [[CNCApi sharedClient] shipWatch: watch.caseId
                            successBlock: ^(NSError* error, BOOL redirect)
         {
             [MBProgressHUD hideAllHUDsForView: self.parentViewController.view animated: YES];

             if (redirect)
             {
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"requestSignout" object:self];
                 return;
             } else if (error == nil)
             {
                 [self refreshTable];
             }
             else
             {
                 [self displayAlert: error.localizedDescription withTitle: @"Error"];
             }
         }];
    }
}

- (IBAction) onImagePressed: (UIButton*) sender
{
    const NSInteger selectedIndex = sender.tag;
    CNCQuote *quote = self.historyInfo[selectedIndex];
    
    if (quote.imageURL.length > 0)
    {
        CNCImageViewerViewController *imageVC = [[CNCImageViewerViewController alloc] init];
        [imageVC setImageUrl:quote.imageURL];
        self.returningFromSecondaryView = YES;
        [self presentViewController:imageVC animated:YES completion:nil];
    }
}

- (IBAction) onImage2Pressed: (UIButton*) sender
{
    const NSInteger selectedIndex = sender.tag;
    CNCQuote *quote = self.historyInfo[selectedIndex];
    
    if (quote.image2URL.length > 0)
    {
        CNCImageViewerViewController *imageVC = [[CNCImageViewerViewController alloc] init];
        [imageVC setImageUrl:quote.image2URL];
        self.returningFromSecondaryView = YES;
        [self presentViewController:imageVC animated:YES completion:nil];
    }
}

- (watchStatusType) getStatusTypeForWatch:(CNCQuote *)watch {
    if ([watch.status isEqualToString:@"To Be Shipped"])
    {
        return StatusToBeShipped;
    }
    if ([watch.status isEqualToString:@"Shipped"])
    {
        return StatusShipped;
    }
    if ([watch.status isEqualToString:@"Offer Declined"])
    {
        NSDate *now = [NSDate date];
        switch ([watch.expiresDate compare:now]){
            case NSOrderedAscending:
                return StatusExpired;
                break;
            case NSOrderedSame:
                return StatusExpired;
                break;
            case NSOrderedDescending:
                return StatusDeclinedByCustomer;
                break;
            }
    }
    if ([watch.status isEqualToString:@"Customer Undecided"] || [watch.status isEqualToString:@"Undecided"])
    {
        NSDate *now = [NSDate date];
        switch ([watch.expiresDate compare:now]){
            case NSOrderedAscending:
                return StatusExpired;
                break;
            case NSOrderedSame:
                return StatusExpired;
                break;
            case NSOrderedDescending:
                return StatusUndecided;
                break;
        }
    }
    if ([watch.status isEqualToString:@"Watch Received"])
    {
        return StatusReceived;
    }
    if ([watch.status isEqualToString:@"Additional Info Required"])
    {
        return StatusAdditionalInfoRequired;
    }
    if ([watch.status isEqualToString:@"Unable to Purchase"])
    {
        return StatusWillNotQuote;
    }
    if ([watch.status isEqualToString:@"Quote Received"])
    {
        NSDate *now = [NSDate date];
        switch ([watch.expiresDate compare:now]){
            case NSOrderedAscending:
                return StatusExpired;
                break;
            case NSOrderedSame:
                return StatusExpired;
                break;
            case NSOrderedDescending:
                return StatusQuoteReceived;
                break;
        }
    }
    
    return StatusUnknown;
}


#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.historyInfo.count==0)
    { // possible if we in the "None" cell and there are actually none
        return self.historyInfoTableView.rowHeight - 85;
    }

    CNCQuote* watch = self.historyInfo[indexPath.row];
    switch ([self getStatusTypeForWatch:watch]) {
        case StatusShipped:
        case StatusToBeShipped:
        case StatusReceived:
            return self.historyInfoTableView.rowHeight;
            break;
            
        case StatusExpired:
        case StatusDeclinedByCustomer:
        case StatusUndecided:
        case StatusQuoteReceived:
        case StatusWillNotQuote:
        case StatusAdditionalInfoRequired:
        default:
            return self.historyInfoTableView.rowHeight - 85;
            break;

    }

}


- (NSInteger) tableView: (UITableView*) tableView numberOfRowsInSection: (NSInteger) section
{
    // Return 1 for empty list
    return MAX(1, self.historyInfo.count);
}

- (UITableViewCell*) tableView: (UITableView*) tableView cellForRowAtIndexPath: (NSIndexPath*) indexPath
{
    const BOOL historyInfoExists = self.historyInfo.count > 0;
    NSString* identifier = historyInfoExists ?  @"watch" : @"emptyCell";
    UITableViewCell* cellGeneral = [tableView dequeueReusableCellWithIdentifier: identifier];
    
    if (historyInfoExists)
    {
        CNCHistoryInfoCell* cell = (CNCHistoryInfoCell*)cellGeneral;
        CNCQuote* watch = self.historyInfo[indexPath.row];

        
        NSString *displayStatus = @"";
        NSString *expirationLabel = @"";
        watchStatusType watchStatusType = [self getStatusTypeForWatch:watch];

        switch (watchStatusType) {
            case StatusShipped:
                displayStatus = @"Accepted & Shipped";
                break;
            case StatusToBeShipped:
                displayStatus = @"Accepted & To Be Shipped";
                break;
            case StatusExpired:
                displayStatus = @"Expired";
                break;
            case StatusUndecided:
                displayStatus = @"Undecided";
                break;
            case StatusDeclinedByCustomer:
                displayStatus = @"Offer Declined";
                break;
            case StatusQuoteReceived:
                displayStatus = @"Quote Received";
                break;
            case StatusReceived:
                displayStatus = @"Watch Received";
                break;
            case StatusAdditionalInfoRequired:
                displayStatus = @"Additional Info Required";
                break;
            case StatusWillNotQuote:
                displayStatus = @"No Offer Made";
                break;
            case StatusUnknown:
                displayStatus = [NSString stringWithFormat:@"Unknown Status Code: %@", watch.status];
                break;
                
            default:
                break;
        }
    
        cell.markedShippedLabel.hidden = YES;
        cell.shippedButton.hidden = YES;
        cell.shippedDateLabel.hidden = YES;
        cell.trackingNumberLabel.hidden =YES;
        cell.expiresDateLabel.hidden = YES;
        cell.amountLabel.hidden = YES;
        cell.acceptedDateLabel.hidden = YES;
        cell.associateNameLabel.text = @"";
        switch (watchStatusType) {
            case StatusShipped:
            case StatusReceived:
                cell.shippedDateLabel.hidden = NO;
                cell.trackingNumberLabel.hidden = NO;
                cell.acceptedDateLabel.hidden = NO;
                cell.amountLabel.hidden = NO;
                [cell.shippedButton setBackgroundImage: [UIImage imageNamed: @"checked-checkbox.png"] forState: UIControlStateNormal];
                CGRect rect = cell.associateNameLabel.frame;
                rect.origin.y = CGRectGetMinY(cell.trackingNumberLabel.frame)+44;
                cell.associateNameLabel.frame = rect;
                break;
            case StatusToBeShipped:
                cell.acceptedDateLabel.hidden = NO;
                cell.amountLabel.hidden = NO;
                cell.markedShippedLabel.hidden = NO;
                cell.shippedButton.hidden = NO;
                [cell.shippedButton setBackgroundImage: [UIImage imageNamed: @"unchecked-checkbox.png"] forState: UIControlStateNormal];
                rect = cell.associateNameLabel.frame;
                rect.origin.y = CGRectGetMinY(cell.trackingNumberLabel.frame)+44;
                cell.associateNameLabel.frame = rect;

                break;
            case StatusExpired:
                cell.expiresDateLabel.hidden = NO;
                rect = cell.associateNameLabel.frame;
                rect.origin.y = CGRectGetMinY(cell.expiresDateLabel.frame)+44;
                cell.associateNameLabel.frame = rect;
                expirationLabel = @"Offer Expired At: ";
                break;
            case StatusDeclinedByCustomer:
            case StatusUndecided:
            case StatusQuoteReceived:
                cell.expiresDateLabel.hidden = NO;
                rect = cell.associateNameLabel.frame;
                rect.origin.y = CGRectGetMinY(cell.expiresDateLabel.frame)+44;
                cell.associateNameLabel.frame = rect;
                expirationLabel = @"Offer Expires At: ";
                break;
                
            case StatusUnknown:
            case StatusWillNotQuote:
            case StatusAdditionalInfoRequired:
                rect = cell.associateNameLabel.frame;
                rect.origin.y = CGRectGetMinY(cell.statusLabel.frame)+44;
                cell.associateNameLabel.frame = rect;
                break;
                
            default:
                break;
        }
        
        cell.nameLabel.attributedText = [self makeAttributedStringWithLabel:@"Name: " value:watch.name];
        cell.associateNameLabel.attributedText = [self makeAttributedStringWithLabel:@"Associate: " value:watch.retailerId];
        cell.brandLabel.attributedText = [self makeAttributedStringWithLabel:@"Brand: " value:watch.brand];
        cell.markedShippedLabel.attributedText = [self makeAttributedStringWithLabel:@"Mark Shipped: " value:nil];
        cell.statusLabel.attributedText = [self makeAttributedStringWithLabel:@"Status: " value:displayStatus];
        cell.trackingNumberLabel.attributedText = [self makeAttributedStringWithLabel:@"Tracking Number: " value:watch.shippingLabel];
        if (watch.phone)
        {
            NSDictionary *phoneDict = [self.phoneFormatter valuesForString:watch.phone];
            cell.phoneLabel.attributedText = [self makeAttributedStringWithLabel:@"Phone: " value:phoneDict[@"text"]];
            
        } else {
            cell.phoneLabel.attributedText = [self makeAttributedStringWithLabel:@"Phone: " value:nil];
        }
        cell.caseIdLabel.attributedText = [self makeAttributedStringWithLabel:@"Case #: " value:watch.caseId];
        
        
        if (![watch.cashPrice isEqual: [NSNull null]])
        {
            cell.amountLabel.attributedText = [self makeAttributedStringWithLabel:@"Insured Amount: " value:[NSString stringWithFormat: @"$%@", [watch.cashPrice stringValue]]];
        }
        else
        {
            cell.amountLabel.attributedText = [self makeAttributedStringWithLabel:@"Insured Amount: " value:@"-"];
        }
        
        
        NSString* dateString;
        if (watch.expiresDate) {
            dateString = [NSString stringWithFormat: @"%@ %@",
                          [self.dateFormatter stringFromDate: watch.expiresDate],
                          [self.timeFormatter stringFromDate: watch.expiresDate]];
        } else {
            dateString = nil;
        }
        cell.expiresDateLabel.attributedText = [self makeAttributedStringWithLabel:expirationLabel value:dateString];
        
        if (watch.createdAt) {
            dateString = [NSString stringWithFormat: @"%@ %@",
                          [self.dateFormatter stringFromDate: watch.createdAt],
                          [self.timeFormatter stringFromDate: watch.createdAt]];
        } else {
            dateString = nil;
        }
        cell.createdDateLabel.attributedText = [self makeAttributedStringWithLabel:@"Case Created: " value:dateString];
        
        if (watch.acceptedAt) {
            dateString = [NSString stringWithFormat: @"%@ %@",
                          [self.dateFormatter stringFromDate: watch.acceptedAt],
                          [self.timeFormatter stringFromDate: watch.acceptedAt]];
        } else {
            dateString = nil;
        }
        cell.acceptedDateLabel.attributedText = [self makeAttributedStringWithLabel:@"Offer Accepted: " value:dateString];
        
        if (watch.shippedAt) {
            dateString = [NSString stringWithFormat: @"%@ %@",
                          [self.dateFormatter stringFromDate: watch.shippedAt],
                          [self.timeFormatter stringFromDate: watch.shippedAt]];
        } else {
            dateString = nil;
        }
        cell.shippedDateLabel.attributedText = [self makeAttributedStringWithLabel:@"Shipped At: " value:dateString];
        
        cell.shippedButton.tag = indexPath.row + 1;
        [cell.shippedButton addTarget: self action: @selector(onShipPressed:) forControlEvents: UIControlEventTouchUpInside];        
        
        cell.watchPhotoButton.hidden = YES;
        cell.watchPhoto.hidden = YES;
        if (watch.imageURL.length > 0)
        {
            NSURL* watchURL = [NSURL URLWithString: watch.imageURL];
            [cell.watchPhoto setImage:nil];
            [cell.watchPhoto setImageWithURL: watchURL];
            cell.watchPhotoButton.tag=indexPath.row;
            cell.watchPhotoButton.hidden = NO;
            cell.watchPhoto.hidden = NO;
        }
        
        cell.watchPhoto2Button.hidden = YES;
        cell.watchPhoto2.hidden = YES;
        if (watch.imageURL.length > 0)
        {
            NSURL* watchURL = [NSURL URLWithString: watch.image2URL];
            [cell.watchPhoto2 setImage:nil];
            [cell.watchPhoto2 setImageWithURL: watchURL];
            cell.watchPhoto2Button.tag=indexPath.row;
            cell.watchPhoto2Button.hidden = NO;
            cell.watchPhoto2.hidden = NO;
        }

        cell.accessoryType = [self allowDrillDownForWatch:watch] ? UITableViewCellAccessoryDisclosureIndicator : UITableViewCellAccessoryNone;

        
    }
    return cellGeneral;
}

-(BOOL) allowDrillDownForWatch: (CNCQuote *)watch
{
    watchStatusType watchStatusType = [self getStatusTypeForWatch:watch];
    return watch.isOpenCase && ((watchStatusType == StatusUndecided)||
                          (watchStatusType == StatusDeclinedByCustomer)||
                                (watchStatusType == StatusQuoteReceived));
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedIndex = indexPath.row;
    CNCQuote *watch = self.historyInfo[self.selectedIndex];
    
    if ([self getStatusTypeForWatch:watch] == StatusAdditionalInfoRequired) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle: @"Additional Info Required"
                                                        message: watch.notes
                                                       delegate: nil
                                              cancelButtonTitle: @"OK"
                                              otherButtonTitles: nil];
        [alert show];
    } else if ([self allowDrillDownForWatch:watch])
    {
        watchStatusType status = [self getStatusTypeForWatch:watch];
        if (status == StatusDeclinedByCustomer) {
            [self changedMind];
        } else {
            [self performSegueWithIdentifier: @"viewOffer" sender: self];
        }
    }
}



- (void)changedMind
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:@"Change Your Mind? Click OK to view the offer again."
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:nil];
    [alert addButtonWithTitle:@"OK"];
    [alert setTag: 1];
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ([alertView tag]== 1 ) {
        if (buttonIndex == 0) {
            // cancel
        } else if (buttonIndex == 1 ){
            //
            [self performSegueWithIdentifier: @"viewOffer" sender: self];
        }
    }
}

- (void) prepareForSegue: (UIStoryboardSegue*) segue sender: (id) sender
{
    if ([segue.destinationViewController isKindOfClass: [CNCOfferViewController class]])
    {
        CNCOfferViewController* offerController = (CNCOfferViewController*)segue.destinationViewController;
        
        CNCQuote* quote = self.historyInfo[self.selectedIndex];
        [offerController setQuote: quote];

    }
        

}



@end
