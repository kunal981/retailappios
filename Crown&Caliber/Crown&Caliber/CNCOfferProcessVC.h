//
//  CNCOfferProcessVC.h
//  Crown&Caliber
//
//  Created by Valeriy on 9/23/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNCMenuViewController.h"
#import "CNCBaseViewController.h"

@class CNCQuote;
@class CNCCase;
@class CNCOfferProcessVC;


@protocol CNCOfferProcessVC_Delegate <NSObject>

- (void) offerProcessViewControllerEnd: (CNCOfferProcessVC*) offerVC;

@end

@interface CNCOfferProcessVC : CNCBaseViewController

@property (nonatomic, assign) id<CNCOfferProcessVC_Delegate> delegate;
@property (nonatomic, strong) id<CNCMenuDelegate> menuDelegate;

@property (nonatomic, strong) IBOutlet UILabel* offerLabel;

@property (nonatomic, strong) IBOutlet UILabel* brandLabel;
@property (nonatomic, strong) IBOutlet UILabel* modelLabel;
@property (nonatomic, strong) IBOutlet UILabel* modelNumLabel;
@property (nonatomic, strong) IBOutlet UILabel* nameLabel;
@property (nonatomic, strong) IBOutlet UILabel* phoneLabel;
@property (nonatomic, strong) IBOutlet UILabel* receivedLabel;
@property (nonatomic, strong) IBOutlet UILabel* caseIdLabel;
@property (nonatomic, strong) IBOutlet UILabel* associateNameLabel;
@property (nonatomic, strong) IBOutlet UIImageView* watchPhoto;
@property (nonatomic, strong) IBOutlet UISwitch* ageSwitch;

@property (nonatomic, strong) CNCQuote* quote;
@property (nonatomic, strong) CNCCase* caseData;

- (IBAction) ageSwitchChanged: (id) sender;

@end
