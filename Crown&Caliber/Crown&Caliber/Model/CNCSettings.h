//
//  CNCSettings.h
//  Crown&Caliber
//

#import <Foundation/Foundation.h>

@interface CNCSettings: NSObject

+ (NSString*) loginToken;
+ (void) setLoginToken: (NSString*) loginToken;

+ (NSString*) userId;
+ (void) setUserId: (NSString*) userId;

+ (NSString*) userEmail;
+ (void) setUserEmail: (NSString*) userId;

@end
