//
//  CNCQuote.m
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCQuote.h"

@implementation CNCQuote

- (instancetype)initWithBaseURL:(NSURL *)url {
    self = [super init];
    if (!self) {
        return nil;
        
    }
    
    self.extraInfo = @"";
    self.isTermsSelected = YES;
    self.isDomestic = YES;
    return self;
}

- (void)setImageFront:(UIImage *)imageFront {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    _imageFront = [self resizeImage:imageFront];
  });
}


- (void)setImageBack:(UIImage *)imageBack {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    _imageBack = [self resizeImage:imageBack];
  });
}

- (void) setDriverLicenseImage: (UIImage*) driverLicenseImage
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        _driverLicenseImage = [self resizeImage: driverLicenseImage];
    });
}

- (void) setSignatureImage:(UIImage *)signatureImage {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        _signatureImage = [self resizeImage: signatureImage];
    });
}

#pragma mark - Image resizing

- (UIImage*) resizeImage:(UIImage*) imageToResize{
    CGSize  dimensions = [imageToResize size];
    if (dimensions.width > 1024){
        int resizeImageWidth = 1024;
        int resizedImageHeight = (resizeImageWidth * dimensions.height) / dimensions.width;
        UIGraphicsBeginImageContext(CGSizeMake(resizeImageWidth, resizedImageHeight));
        [imageToResize drawInRect: CGRectMake(0, 0, resizeImageWidth, resizedImageHeight)];
        UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return resizedImage;
    }
    return imageToResize;
}

- (void) setMainInfo: (NSDictionary*) mainInfo
{
	self.name = mainInfo[UserNameKey];
	self.email = mainInfo[UserEmailKey];
	self.phone = mainInfo[UserPhoneNumberKey];
	self.brand = mainInfo[QuoteBrandKey];
	self.caseId = [NSString stringWithFormat: @"%@", mainInfo[QuoteCaseIdKey]];
	
    self.driverLicenseImage = [CNCQuoteProcessor quoteDriverLicenseWithQuoteId: self.caseId];
	
	self.storeName = ([mainInfo[QuoteStoreNameKey] isKindOfClass: [NSString class]]) ? mainInfo[QuoteStoreNameKey] : @"Store 1" ;
	self.retailerId = ([mainInfo[QuoteRetailerIdKey] isKindOfClass: [NSString class]]) ? mainInfo[QuoteRetailerIdKey] : @"Store 1" ;
}

- (NSMutableDictionary*) details
{
	NSMutableDictionary* detailsContainer = [[NSMutableDictionary alloc] init];
	
	[detailsContainer setObject: self.name forKey: UserNameKey];
	[detailsContainer setObject: self.email forKey: UserEmailKey];
	[detailsContainer setObject: self.phone forKey: UserPhoneNumberKey];
	[detailsContainer setObject: self.brand forKey: QuoteBrandKey];
	[detailsContainer setObject: [self.caseId isKindOfClass: [NSObject class]] ? [NSString stringWithFormat: @"%@", self.caseId] : @""
						 forKey: QuoteCaseIdKey];

	[detailsContainer setObject: [self.retailerId isKindOfClass: [NSString class]] ? self.retailerId : @""
						 forKey: QuoteRetailerIdKey];
	
	return detailsContainer;
}

- (void) saveQuoteToStorage
{
	NSMutableDictionary* details = [self details];
	
    if (self.driverLicenseImage)
    {
        [CNCQuoteProcessor saveDriverLicense: self.driverLicenseImage
                                    forQuote: self.caseId];
    }
	
	[CNCQuoteProcessor saveQuoteInfo: details];
}

- (void) removeQuoteFromStorage
{
	[CNCQuoteProcessor deleteQuote: self];
}

@end
