//
//  CNCApi.m
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCApi.h"
#import <AFNetworking/AFNetworking.h>
#import "CNCQuote.h"
#import "CNCWatch.h"
#import "CNCSettings.h"
#import "CNCAppOptions.h"
#import "CNCCase.h"


#define API_HOST @"https://portal.crownandcaliber.com/api/external/v2/"
//#define API_HOST @"https://crown.sandbox.rietta.com/api/external/v2/"


//#define API_HOST @"https://portal.crownandcaliber.com/api/external"
//#define API_HOST @"https://portal.crownandcaliber.com/api/v1/sessions"

//curl -v -H 'Content-Type: application/json' -H 'Accept: application/json' -X POST https://portal.crownandcaliber.com/api/v1/sessions -d "{\"user\":{\"email\":\”user@domain.com\",\"password\":\”password here\"}}”

#define API_TOKEN @"b48e7e6f69cb4f2bad36d38cbfcded7c:491ce05faded67ac6c4b04e044d2503da2df6b8fde8113082a96eefe9fb9ff6861e1af155e7c0a258172a9e898cbb6ee19b82e4b7f66970d"
#define API_USERAGENT @"iphone"
#define API_IP @"127.0.0.1"
#define API_SOURCE @"ios"


@implementation CNCApi

+(instancetype)sharedClient {
     static CNCApi *_sharedClient = nil;
     static dispatch_once_t onceToken;
     dispatch_once(&onceToken, ^{
          _sharedClient = [[self alloc] init];
     });
     
     return _sharedClient;
}


- (instancetype)init
{
     self = [super init];
     if (self) {
          self.dateFormatter = [[NSDateFormatter alloc] init];
          [self.dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
          [self.dateFormatter setDateFormat: @"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'"];
     }
     return self;
}


-(void)createQuote:(CNCQuote *)quote withSuccess:(void (^)(CNCQuote *quote, NSString* submissionText, BOOL retailTimer, NSError *error, BOOL redirect, NSString* message))success {
     AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
     manager.requestSerializer = [AFJSONRequestSerializer serializer];
     [manager.requestSerializer setValue: @"application/json" forHTTPHeaderField: @"Accept"];
     [manager.requestSerializer setValue: [CNCSettings loginToken] forHTTPHeaderField: @"X-AUTH-TOKEN"];
     
     
     NSMutableDictionary* params = [NSMutableDictionary dictionaryWithDictionary:@{@"name" : quote.name, \
                                                                                   @"email" : quote.email, \
                                                                                   @"brand" : quote.brand, \
                                                                                   @"domestic" : quote.isDomestic?@"true":@"false", \
                                                                                   @"no_model_number" : quote.hasModelNumber?@"false":@"true", \
                                                                                   @"retailer" : ([CNCAppOptions sharedInstance].appSize==AppSizeRep)? @"false" : @"true", \
                                                                                   @"business_development_manager" : ([CNCAppOptions sharedInstance].appSize==AppSizeRep)? @"true" : @"false", \
                                                                                   @"expedited" : quote.isExpedited?@"true":@"false", \
                                                                                   @"trade_in": quote.isTradeIn?@"true":@"false", \
                                                                                   @"full_quote" : quote.isFullQuote?@"true":@"false",\
                                                                                   @"store_id" : [CNCSettings userId], \
                                                                                   @"terms" : @"true", \
                                                                                   @"originating_ip" : API_IP, \
                                                                                   @"originating_source" : API_SOURCE, \
                                                                                   @"originating_user_agent" : API_USERAGENT, \
                                                                                   @"originating_campaign" : @"retailer app"}];

     if (quote.retailerId != nil)
     {
          [params setObject: quote.retailerId forKey: @"associate_name"];
     }
     
     if (quote.phone != nil)
     {
          [params setObject: quote.phone forKey: @"phone"];
     }
     
     if (quote.extraInfo.length > 0)
     {
          [params setObject: quote.extraInfo forKey: @"extra_info"];
     }
     
     if (quote.salesAssociateId != nil)
     {
          [params setObject: [quote.salesAssociateId stringValue] forKey: @"sales_associate_id"];
     }
     
     if (quote.documents.length > 0)
     {
          [params setObject: quote.documents forKey: @"documents"];
     }
     else
     {
          [params setObject: @"neither" forKey: @"documents"];
     }
     
     if (quote.modelName.length > 0) {
          [params setObject:quote.modelName forKey:@"model_name"];
     }
     
     if (quote.modelNumber.length > 0) {
          [params setObject:quote.modelNumber forKey:@"model_number"];
     }
     
     if (quote.retailerName.length > 0) {
          [params setObject:quote.retailerName forKey:@"retailer_name"];
     }
     
     
     [manager POST: [API_HOST stringByAppendingPathComponent: @"quote_requests.json"]
        parameters: params
           success: ^(AFHTTPRequestOperation *operation, id responseObject) {
                if ([responseObject isKindOfClass:[NSDictionary class]] && responseObject[@"success"]) {
                     quote.imagesPostURLString = responseObject[@"attachments_post_url"];
                     quote.caseId = [NSString stringWithFormat: @"%@", responseObject[@"case_id"]];
                     quote.sessionId = responseObject[@"session_id"];
                     NSString* submissionText = responseObject[@"submission_text"];
                     BOOL retailTimer = [(NSNumber *)responseObject[@"retail_timer"] boolValue];
                     [self uploadImage:quote.imageFront toQuote:quote withSuccess:^(CNCQuote *quote, NSError *error) {
                          [self uploadImage:quote.imageBack toQuote:quote withSuccess:^(CNCQuote *quote, NSError *error) {
                               // if needed upload DL
                               [self uploadDriversLicenseImage:quote.driverLicenseImage toCaseID:quote.caseId forStoreID:[CNCSettings userId] withSuccess:^(NSError *error) {
                                    // if needed upload DL
                                    [self uploadSignatureImage:quote.signatureImage toCaseID:quote.caseId forStoreID:[CNCSettings userId] withSuccess:^(NSError *error) {
                                         if (success) {
                                              success(quote, submissionText, retailTimer, nil, NO, nil);
                                         }
                                         
                                    }];
                                    
                               }];
                          }];
                     }];
                }
                
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                if (success) {
                     long statusCode = [[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
                     NSMutableString *message = [NSMutableString new];
                     if (statusCode==401)
                     {
                          success(quote, nil, NO, error, YES, message);
                     } else if (statusCode==422)
                     {
                          if ([operation.responseObject isKindOfClass:[NSDictionary class]])
                          {
                               NSDictionary *errors = operation.responseObject[@"errors"];
                               for( NSString *aField in [errors allKeys] )
                               {
                                    for (NSString *errorType in errors[aField])
                                    {
                                         [message appendFormat:@"%@ %@\n",aField, errorType];
                                         
                                    }
                               }
                          }
                          if (message.length==0) message=nil;
                          success(quote, nil, NO, error, NO, message);

                     } else
                     {
                          success(quote, nil, NO, error, NO, nil);
                     }
                }
           }];
}



- (void) loginWithEmail: (NSString*) email
               password: (NSString*) password
           successBlock: (void (^) (NSString* userId, NSString* authToken, NSError *error, NSString *message))success
{
     AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
     [manager.requestSerializer setValue: @"application/json" forHTTPHeaderField: @"Accept"];
     
     NSDictionary* params = @{@"email": email, @"password": password};
     
     [manager POST: [API_HOST stringByAppendingPathComponent: @"authenticate"]
        parameters: params
           success: ^(AFHTTPRequestOperation *operation, id responseObject) {
                if (success)
                {
                     [CNCSettings setUserId: responseObject[@"user_id"]];
                     [CNCSettings setLoginToken: responseObject[@"authentication_token"]];
                     
                     success(responseObject[@"user_id"], responseObject[@"authentication_token"], nil, nil);
                }
           }
           failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
                long statusCode = [[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
                NSString *message;
                switch (statusCode)
                {
                     case 422:
                          if ([operation.responseObject isKindOfClass:[NSDictionary class]])
                          {
                               message = operation.responseObject[@"error"];
                          }
                          success(nil, nil, error, message);
                          break;
                     default:
                          success(nil, nil, error, nil);
                          break;
                }
                
           }];
}



- (void) upateAppInfoWithCompletionBlock: (void (^)(NSError *error, BOOL redirect)) success
{
     AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
     [manager.requestSerializer setValue: @"application/json" forHTTPHeaderField: @"Accept"];
     [manager.requestSerializer setValue: [CNCSettings loginToken] forHTTPHeaderField: @"X-AUTH-TOKEN"];
     
     [manager GET: [API_HOST stringByAppendingPathComponent: [NSString stringWithFormat: @"retail_stores/%@", [CNCSettings userId]]]
       parameters: nil
          success: ^(AFHTTPRequestOperation *operation, id responseObject) {
               CNCAppOptions* options = [CNCAppOptions sharedInstance];
               
               options.appSize = AppSizeUnknown;
               if (![responseObject[@"assets"][@"appsize"] isEqual: [NSNull null]])
               {
                    if ([responseObject[@"assets"][@"appsize"] isEqualToString: @"small"])
                    {
                         options.appSize= AppSizeSmall;
                    }
                    if ([responseObject[@"assets"][@"appsize"] isEqualToString: @"big"])
                    {
                         options.appSize= AppSizeBig;
                    }
                    if ([responseObject[@"assets"][@"appsize"] isEqualToString: @"rep"])
                    {
                         options.appSize= AppSizeRep;
                    }
               }
               
               
               options.showDLOnQuote = NO;
               if (![responseObject[@"assets"][@"quote_form_dl"] isEqual: [NSNull null]] &&
                   [responseObject[@"assets"][@"quote_form_dl"] isEqual: [NSNumber numberWithInt: 1]])
               {
                    options.showDLOnQuote = YES;
               }
               
               options.managersApproval = NO;
               if (![responseObject[@"assets"][@"managers_approval"] isEqual: [NSNull null]] &&
                   [responseObject[@"assets"][@"managers_approval"] isEqual: [NSNumber numberWithInt: 1]])
               {
                    options.managersApproval = YES;
               }
               
               options.logoURL = responseObject[@"assets"][@"logo"];
               options.screensaverURL = responseObject[@"assets"][@"screensaver"];
               options.brands = responseObject[@"options"][@"brands"];
               options.declineReasons = responseObject[@"options"][@"decline_reason"];

               options.modelsDescription = responseObject[@"options"][@"model_location"];
               
               options.reps = responseObject[@"reps"];
               
               if (![responseObject[@"assets"][@"terms"] isEqual: [NSNull null]])
               {
                    options.terms = responseObject[@"assets"][@"terms"];
               }
               success(nil, NO);
          }
      
          failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
               if (success) {
                    long statusCode = [[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
                    if (statusCode==401)
                    {
                         success(nil, YES);
                    } else
                    {
                         success(nil, NO);
                    }
               }
          }];
     
}


- (CNCQuote*) watchFromDictionary: (NSDictionary*) watchDict
{
     CNCQuote* watch = [CNCQuote new];
     watch.brand = watchDict[@"brand"];
     
     NSString* dateString = watchDict[@"accepted_at"];
     if (dateString && ![dateString isEqual: [NSNull null]])
     {
          watch.acceptedAt = [self.dateFormatter dateFromString: dateString];
     }
     
     dateString = watchDict[@"offer_submitted_at"];
     if (dateString && ![dateString isEqual: [NSNull null]])
     {
          watch.createdAt = [self.dateFormatter dateFromString: dateString];
     }
     
     // watches to ship entries use created date instead of offer_submitted_at ??
     dateString = watchDict[@"created_at"];
     if (dateString && ![dateString isEqual: [NSNull null]])
     {
          watch.createdAt = [self.dateFormatter dateFromString: dateString];
     }
     
     dateString = watchDict[@"offer_expires_at"];
     if (dateString && ![dateString isEqual: [NSNull null]])
     {
          watch.expiresDate = [self.dateFormatter dateFromString: dateString];
     }
     
     dateString = watchDict[@"offer_recieved_at"];
     if (dateString && ![dateString isEqual: [NSNull null]])
     {
          watch.receivedAt = [self.dateFormatter dateFromString: dateString];
     }
     
     dateString = watchDict[@"shipped_at"];
     if (dateString && ![dateString isEqual: [NSNull null]])
     {
          watch.shippedAt = [self.dateFormatter dateFromString: dateString];
     }
     
     watch.retailerId = watchDict[@"associate_name"];
     
     watch.status = watchDict[@"status"];
     
     if ([watch.status isEqualToString: @"Waiting For Quote"])
     {
          watch.offerReceived = NO;
     }
     else if ([watch.status isEqualToString: @"Quote Received"])
     {
          watch.offerReceived = YES;
     }
     else if ([watch.status isEqualToString: @"Undecided"])
     {
          watch.offerReceived = YES;
     }
     else if ([watch.status isEqualToString: @"Customer Undecided"])
     {
          watch.offerReceived = YES;
     }
     
     
     
     
     watch.email = watchDict[@"email"];
     watch.caseId = [((NSNumber *)watchDict[@"id"]) stringValue];
     watch.imageURL = watchDict[@"image"];
     watch.image2URL = watchDict[@"image_2"];
     watch.modelName = watchDict[@"model_name"];
     watch.modelNumber = watchDict[@"model_number"];
     watch.name = watchDict[@"name"];
     watch.phone = watchDict[@"phone"];
     watch.shippingLabel = watchDict[@"shipping_label"];
     
     if ([watchDict[@"timer"] isEqualToValue: @YES])
     {
          watch.hasTimer = YES;
     }
     watch.timerText = watchDict[@"timer_text"];
     watch.notes = watchDict[@"notes"];
     
     if ([watchDict[@"shipped"] isEqualToValue: @YES])
     {
          watch.shipped = YES;
     }
     
     if ([watchDict[@"open"] isEqualToValue: @YES])
     {
          watch.isOpenCase = YES;
     } else
     {
          watch.isOpenCase = NO;
     }
     
     
     watch.cashPrice = watchDict[@"cash_price"];
     
     return watch;
}


- (void) pendingWatches: (NSString*) userId
           successBlock: (void (^)(NSArray* watches, NSError *error, BOOL redirect)) success
{
     AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
     [manager.requestSerializer setValue: @"application/json" forHTTPHeaderField: @"Accept"];
     [manager.requestSerializer setValue: [CNCSettings loginToken] forHTTPHeaderField: @"X-AUTH-TOKEN"];
     
     [manager GET: [API_HOST stringByAppendingPathComponent: [NSString stringWithFormat: @"retail_stores/%@/retail_cases", [CNCSettings userId]]]
       parameters: nil
          success: ^(AFHTTPRequestOperation *operation, id responseObject) {
               NSMutableArray* resultArray = [NSMutableArray arrayWithCapacity: ((NSArray*)responseObject[@"pending_cases"]).count];
               
               for (NSDictionary* watchDict in responseObject[@"pending_cases"])
               {
                    CNCQuote* watch = [self watchFromDictionary: [self dictionaryByRemovingNulls: watchDict]];
                    [resultArray addObject: watch];
               }
               
               success(resultArray, nil, NO);
          }
      
          failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
               if (success) {
                    long statusCode = [[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
                    if (statusCode==401)
                    {
                         success(nil, error, YES);
                    } else
                    {
                         success(nil, error, NO);
                    }
               }

          }];
}



- (void) quotedWatches: (NSString*) userId
          successBlock: (void (^)(NSArray* watches, NSError *error, BOOL redirect)) success
{
     AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
     [manager.requestSerializer setValue: @"application/json" forHTTPHeaderField: @"Accept"];
     [manager.requestSerializer setValue: [CNCSettings loginToken] forHTTPHeaderField: @"X-AUTH-TOKEN"];
     
     [manager GET: [API_HOST stringByAppendingPathComponent: [NSString stringWithFormat: @"retail_stores/%@/retail_cases", [CNCSettings userId]]]
       parameters: nil
          success: ^(AFHTTPRequestOperation *operation, id responseObject) {
               NSMutableArray* resultArray = [NSMutableArray arrayWithCapacity: ((NSArray*)responseObject[@"quoted_cases"]).count];
               
               for (NSDictionary* watchDict in responseObject[@"quoted_cases"])
               {
                    CNCQuote* watch = [self watchFromDictionary: [self dictionaryByRemovingNulls: watchDict]];
                    [resultArray addObject: watch];
               }
               
               success(resultArray, nil, NO);
          }
      
          failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
               if (success) {
                    long statusCode = [[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
                    if (statusCode==401)
                    {
                         success(nil, error, YES);
                    } else
                    {
                         success(nil, error, NO);
                    }
               }
          }];
}



- (void) watchesToShip: (NSString*) userId
          successBlock: (void (^)(NSArray* watches, NSError *error, BOOL redirect)) success
{
     AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
     [manager.requestSerializer setValue: @"application/json" forHTTPHeaderField: @"Accept"];
     [manager.requestSerializer setValue: [CNCSettings loginToken] forHTTPHeaderField: @"X-AUTH-TOKEN"];
     
     [manager GET: [API_HOST stringByAppendingPathComponent: [NSString stringWithFormat: @"retail_stores/%@/retail_cases", [CNCSettings userId]]]
       parameters: nil
          success: ^(AFHTTPRequestOperation *operation, id responseObject) {
               NSMutableArray* resultArray = [NSMutableArray arrayWithCapacity: ((NSArray*)responseObject[@"to_be_shipped_cases"]).count];
               
               for (NSDictionary* watchDict in responseObject[@"to_be_shipped_cases"])
               {
                    CNCQuote* watch = [self watchFromDictionary: [self dictionaryByRemovingNulls: watchDict]];
                    [resultArray addObject: watch];
               }
               
               success(resultArray, nil, NO);
          }
      
          failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
               if (success) {
                    long statusCode = [[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
                    if (statusCode==401)
                    {
                         success(nil, error, YES);
                    } else
                    {
                         success(nil, error, NO);
                    }
               }
          }];
}


- (void) historyInfo: (NSString*) userId
        successBlock: (void (^)(NSArray* watches, NSError *error, BOOL redirect)) success
{
     AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
     [manager.requestSerializer setValue: @"application/json" forHTTPHeaderField: @"Accept"];
     [manager.requestSerializer setValue: [CNCSettings loginToken] forHTTPHeaderField: @"X-AUTH-TOKEN"];
     
     [manager GET: [API_HOST stringByAppendingPathComponent: [NSString stringWithFormat: @"retail_stores/%@/retail_cases", [CNCSettings userId]]]
       parameters: nil
          success: ^(AFHTTPRequestOperation *operation, id responseObject) {
               NSMutableArray* resultArray = [NSMutableArray arrayWithCapacity: ((NSArray*)responseObject[@"historical_cases"]).count];
               
               for (NSDictionary* watchDict in responseObject[@"historical_cases"])
               {
                    CNCQuote* watch = [self watchFromDictionary: [self dictionaryByRemovingNulls: watchDict]];
                    [resultArray addObject: watch];
               }
               
               success(resultArray, nil, NO);
          }
      
          failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
               if (success) {
                    long statusCode = [[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
                    if (statusCode==401)
                    {
                         success(nil, error, YES);
                    } else
                    {
                         success(nil, error, NO);
                    }
               }
          }];
     
}


- (void) watchCaseInfo: (NSString*) caseId
          successBlock: (void (^)(CNCCase* aCase, NSError *error, BOOL redirect)) success
{
     AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
     [manager.requestSerializer setValue: @"application/json" forHTTPHeaderField: @"Accept"];
     [manager.requestSerializer setValue: [CNCSettings loginToken] forHTTPHeaderField: @"X-AUTH-TOKEN"];
     
     [manager GET: [API_HOST stringByAppendingPathComponent: [NSString stringWithFormat: @"retail_stores/%@/retail_cases/%@", [CNCSettings userId], caseId]]
       parameters: nil
          success: ^(AFHTTPRequestOperation *operation, id response) {
               NSDictionary* responseObject = [self dictionaryByRemovingNulls: response];
               CNCCase* resultCase = [CNCCase new];
               resultCase.brand = responseObject[@"brand"];
               resultCase.caseId = responseObject[@"case_id"];
               resultCase.email = responseObject[@"email"];
               resultCase.imageURL = responseObject[@"image"];
               resultCase.modelName = responseObject[@"model_name"];
               resultCase.modelNumber = responseObject[@"model_number"];
               resultCase.offerText = responseObject[@"offer_text"];
               resultCase.phone = responseObject[@"phone"];
               resultCase.seller = responseObject[@"seller"];
               resultCase.notes = responseObject[@"notes"];
               resultCase.quoteRequest = responseObject[@"quote_request"];
               resultCase.signatureOnFile = NO;
               if ([responseObject[@"customer_agreement_signature_on_file"] isEqualToValue: @YES])
               {
                    resultCase.signatureOnFile = YES;
               }
               resultCase.driversLicenseOnFile = NO;
               if ([responseObject[@"drivers_license_on_file"] isEqualToValue: @YES])
               {
                    resultCase.driversLicenseOnFile = YES;
               }
               resultCase.watchData = [self arrayByRemovingNulls: responseObject[@"watch_data"]];
               
               success(resultCase, nil, NO);
          }
      
          failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
               if (success) {
                    long statusCode = [[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
                    if (statusCode==401)
                    {
                         success(nil, error, YES);
                    } else
                    {
                         success(nil, error, NO);
                    }
               }
          }];
}


- (void) acceptCase: (NSString*) caseId
       successBlock: (void (^)(NSString* thankYou, NSError *error, BOOL redirect)) success
{
     AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
     [manager.requestSerializer setValue: @"application/json" forHTTPHeaderField: @"Accept"];
     [manager.requestSerializer setValue: [CNCSettings loginToken] forHTTPHeaderField: @"X-AUTH-TOKEN"];
     
//     NSDictionary* params = @{ @"authentication": [CNCSettings loginToken],
//                               @"api_token": [CNCSettings loginToken]
//                               };
     
     [manager PUT: [API_HOST stringByAppendingString: [NSString stringWithFormat: @"/retail_stores/%@/retail_cases/%@/accept.json", [CNCSettings userId], caseId]]
       parameters: nil
          success: ^(AFHTTPRequestOperation *operation, id responseObject) {
               //             {
               //                 "accepted_at" = "2014-12-02T23:51:44.675Z";
               //                 "case_id" = 9414;
               //                 id = 20468;
               //                 "session_id" = "<null>";
               //                 success = 1;
               //                 "thank_you" = "Thank You Text here.";
               //             }
               success(responseObject[@"thank_you"], nil, NO);
          }
      
          failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
               if (success) {
                    long statusCode = [[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
                    if (statusCode==401)
                    {
                         success(nil, error, YES);
                    } else
                    {
                         success(nil, error, NO);
                    }
               }
          }];
}


- (void) rejectCase: (NSString*) caseId
      declineReason: (NSString*) declineReason
       successBlock: (void (^)(NSError *error, BOOL redirect)) success
{
     AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
     [manager.requestSerializer setValue: @"application/json" forHTTPHeaderField: @"Accept"];
     [manager.requestSerializer setValue: [CNCSettings loginToken] forHTTPHeaderField: @"X-AUTH-TOKEN"];
     
//     NSDictionary* params = @{ @"authentication": [CNCSettings loginToken],
//                               @"api_token": [CNCSettings loginToken],
//                               @"decline_reason": declineReason
//                               };
     NSDictionary* params = @{ @"decline_reason": declineReason
                               };
     
     [manager PUT: [API_HOST stringByAppendingString: [NSString stringWithFormat: @"/retail_stores/%@/retail_cases/%@/reject", [CNCSettings userId], caseId]]
       parameters: params
          success: ^(AFHTTPRequestOperation *operation, id responseObject) {
               success(nil, NO);
          }
      
          failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
               if (success) {
                    long statusCode = [[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
                    if (statusCode==401)
                    {
                         success(error, YES);
                    } else
                    {
                         success(error, NO);
                    }
               }
          }];
}

//- (void) undecideCase: (NSString*) caseId
//         successBlock: (void (^)(NSError *error)) success
//{
//    AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
//    [manager.requestSerializer setValue: @"application/json" forHTTPHeaderField: @"Accept"];
//    [manager.requestSerializer setValue: [CNCSettings loginToken] forHTTPHeaderField: @"X-AUTH-TOKEN"];
//
//    NSDictionary* params = @{ @"authentication": [CNCSettings loginToken],
//                              @"api_token": [CNCSettings loginToken],
//                              };
//
//    [manager PUT: [API_HOST stringByAppendingString: [NSString stringWithFormat: @"/retail_stores/%@/retail_cases/%@/undecided", [CNCSettings userId], caseId]]
//      parameters: params
//         success: ^(AFHTTPRequestOperation *operation, id responseObject) {
//             success(nil);
//         }
//
//         failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
//             success(error);
//         }];
//}

- (void) shipWatch: (NSString*) caseId
      successBlock: (void (^)(NSError *error, BOOL redirect)) success
{
     AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
     [manager.requestSerializer setValue: @"application/json" forHTTPHeaderField: @"Accept"];
     [manager.requestSerializer setValue: [CNCSettings loginToken] forHTTPHeaderField: @"X-AUTH-TOKEN"];
     
//     NSDictionary* params = @{ @"authentication": [CNCSettings loginToken],
//                               @"api_token": [CNCSettings loginToken],
//                               };
     
     [manager PUT: [API_HOST stringByAppendingString: [NSString stringWithFormat: @"/retail_stores/%@/retail_cases/%@/ship", [CNCSettings userId], caseId]]
       parameters: nil
          success: ^(AFHTTPRequestOperation *operation, id responseObject) {
               //             "accepted_at" = "2014-12-03T13:48:38.784Z";
               //             "case_id" = 9436;
               //             id = 20512;
               //             "session_id" = "<null>";
               //             success = 1;
               //             "thank_you" = "Thank You Text here.";
               success(nil, NO);
          }
      
          failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
               if (success) {
                    long statusCode = [[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
                    if (statusCode==401)
                    {
                         success(error, YES);
                    } else
                    {
                         success(error, NO);
                    }
               }
          }];
}

//- (void) uploadImage: (UIImage*) image
//            toCaseID: (NSString*) caseID
//         withSuccess: (void (^)(CNCCase *caseData, NSError *error))success
//{
//     NSData *imageData = UIImageJPEGRepresentation(image, 0.8);
//     
//     AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
//     
//     NSError* error;
//     
//     NSString* urlString = [API_HOST stringByAppendingFormat: @"quote_requests/%@/attachments", caseID];
//     
//     NSMutableURLRequest *request = [serializer multipartFormRequestWithMethod:@"POST" URLString:urlString  parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//          [formData appendPartWithFormData:[[[CNCSettings loginToken]  stringByAppendingString:@";type=text"] dataUsingEncoding:NSUTF8StringEncoding] name:@"api_token"];
//          [formData appendPartWithFileData:imageData name:@"attachment[data]" fileName:@"myimage.jpg" mimeType:@"image/jpeg"];
//     } error:&error];
//     
//     [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//     request.timeoutInterval = 180;
//     
//     
//     AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//     AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request
//                                                                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                                                                               NSLog(@"Success %@", responseObject);
//                                                                               
//                                                                               if (success) {
//                                                                                    success(caseQuote, nil);
//                                                                               }
//                                                                          }
//                                                                          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                                                                               NSLog(@"Failure %@", error.description);
//                                                                               
//                                                                               if (success) {
//                                                                                    success(caseQuote, error);
//                                                                               }
//                                                                          }];
//     
//     [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
//                                         long long totalBytesWritten,
//                                         long long totalBytesExpectedToWrite) {
//          NSLog(@"Wrote %lld/%lld", totalBytesWritten, totalBytesExpectedToWrite);
//     }];
//     
//     [operation start];
//}
//
- (void) uploadImage: (UIImage*) image
             toQuote: (CNCQuote*) quote
         withSuccess: (void (^)(CNCQuote *quote, NSError *error))success
{
     NSData *imageData = UIImageJPEGRepresentation(image, 0.8);
     
     AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
     
     NSError* error;
     
     NSMutableURLRequest *request = [serializer multipartFormRequestWithMethod:@"POST" URLString:quote.imagesPostURLString  parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
          [formData appendPartWithFormData:[[quote.sessionId  stringByAppendingString:@";type=text"] dataUsingEncoding:NSUTF8StringEncoding] name:@"session_id"];
          [formData appendPartWithFormData:[[[CNCSettings loginToken]  stringByAppendingString:@";type=text"] dataUsingEncoding:NSUTF8StringEncoding] name:@"api_token"];
          [formData appendPartWithFileData:imageData name:@"attachment[data]" fileName:@"myimage.jpg" mimeType:@"image/jpeg"];
     } error:&error];
     
     [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
     [request setValue: [CNCSettings loginToken] forHTTPHeaderField: @"X-AUTH-TOKEN"];
     request.timeoutInterval = 180;
     
     
     AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
     AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                               NSLog(@"Success %@", responseObject);
                                                                               
                                                                               if (success) {
                                                                                    success(quote, nil);
                                                                               }
                                                                          }
                                                                          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                               NSLog(@"Failure %@", error.description);
                                                                               
                                                                               if (success) {
                                                                                    success(quote, error);
                                                                               }
                                                                          }];
     
     [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
                                         long long totalBytesWritten,
                                         long long totalBytesExpectedToWrite) {
          NSLog(@"Wrote %lld/%lld", totalBytesWritten, totalBytesExpectedToWrite);
     }];
     
     [operation start];
}

- (void) uploadSignatureImage: (UIImage*) image
                          toCaseID: (NSString*) caseID
                        forStoreID: (NSString*) storeID
                       withSuccess: (void (^)(NSError *error))success
{
     NSData *imageData = UIImageJPEGRepresentation(image, 0.8);
     
     if (imageData == nil) {
          success(nil);
          return;
     }
     
     AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
     
     NSError* error;
     
     NSString* urlString = [API_HOST stringByAppendingFormat: @"retail_stores/%@/retail_cases/%@/customer_agreement_signatures", storeID, caseID];
     
     
     NSMutableURLRequest *request = [serializer multipartFormRequestWithMethod:@"POST" URLString:urlString  parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
          [formData appendPartWithFormData:[[[CNCSettings loginToken]  stringByAppendingString:@";type=text"] dataUsingEncoding:NSUTF8StringEncoding] name:@"api_token"];
          [formData appendPartWithFileData:imageData name:@"attachment[data]" fileName:@"myimage.jpg" mimeType:@"image/jpeg"];
     } error:&error];
     
     [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
     [request setValue: [CNCSettings loginToken] forHTTPHeaderField: @"X-AUTH-TOKEN"];
     request.timeoutInterval = 180;
     
     
     AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
     AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                               NSLog(@"Success %@", responseObject);
                                                                               
                                                                               if (success) {
                                                                                    success(nil);
                                                                               }
                                                                          }
                                                                          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                               NSLog(@"Failure %@", error.description);
                                                                               
                                                                               if (success) {
                                                                                    success(error);
                                                                               }
                                                                          }];
     
     [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
                                         long long totalBytesWritten,
                                         long long totalBytesExpectedToWrite) {
          NSLog(@"Wrote %lld/%lld", totalBytesWritten, totalBytesExpectedToWrite);
     }];
     
     [operation start];
     
}

- (void) uploadDriversLicenseImage: (UIImage*) image
                          toCaseID: (NSString*) caseID
                        forStoreID: (NSString*) storeID
                       withSuccess: (void (^)(NSError *error))success
{
     NSData *imageData = UIImageJPEGRepresentation(image, 0.8);
     
     if (imageData == nil) {
          success(nil);
          return;
     }
     
     AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
     
     NSError* error;
     
     NSString* urlString = [API_HOST stringByAppendingFormat: @"retail_stores/%@/retail_cases/%@/drivers_licenses", storeID, caseID];
     

     NSMutableURLRequest *request = [serializer multipartFormRequestWithMethod:@"POST" URLString:urlString  parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
          [formData appendPartWithFormData:[[[CNCSettings loginToken]  stringByAppendingString:@";type=text"] dataUsingEncoding:NSUTF8StringEncoding] name:@"api_token"];
          [formData appendPartWithFileData:imageData name:@"attachment[data]" fileName:@"myimage.jpg" mimeType:@"image/jpeg"];
     } error:&error];

     [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
     [request setValue: [CNCSettings loginToken] forHTTPHeaderField: @"X-AUTH-TOKEN"];
     request.timeoutInterval = 180;


     AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
     AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                               NSLog(@"Success %@", responseObject);

                                                                               if (success) {
                                                                                    success(nil);
                                                                               }
                                                                          }
                                                                          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                               NSLog(@"Failure %@", error.description);

                                                                               if (success) {
                                                                                    success(error);
                                                                               }
                                                                          }];

     [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
                                         long long totalBytesWritten,
                                         long long totalBytesExpectedToWrite) {
          NSLog(@"Wrote %lld/%lld", totalBytesWritten, totalBytesExpectedToWrite);
     }];

     [operation start];
}


- (NSDictionary*) dictionaryByRemovingNulls: (NSDictionary*) dict
{
     __block NSMutableDictionary* retVal = [NSMutableDictionary dictionaryWithCapacity: dict.count];
     [dict enumerateKeysAndObjectsUsingBlock: ^(id key, id obj, BOOL* stop)
      {
           if (![obj isKindOfClass: [NSNull class]])
           {
                retVal[key] = obj;
           }
      }];
     
     return [NSDictionary dictionaryWithDictionary: retVal];
}

- (NSArray*) arrayByRemovingNulls: (NSArray*) list
{
     __block NSMutableArray* retVal = [NSMutableArray arrayWithCapacity: list.count];
     [list enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop)
      {
           if ([obj isKindOfClass: [NSDictionary class]])
           {
                [retVal addObject: [self dictionaryByRemovingNulls: obj]];
           }
           else if (![obj isKindOfClass: [NSNull class]])
           {
                [retVal addObject: obj];
           }
      }];
     
     return [NSArray arrayWithArray: retVal];
}


@end
