//
//  CNCSettings.m
//  Crown&Caliber
//

#import "CNCSettings.h"

static NSString* const LoginToken = @"LoginToken";
static NSString* const UserId = @"UserId";
static NSString* const UserEmail = @"UserEmail";

@implementation CNCSettings

+ (NSString*) loginToken
{
    return [[NSUserDefaults standardUserDefaults] objectForKey: LoginToken];
}

+ (void) setLoginToken: (NSString*) loginToken
{
    [[NSUserDefaults standardUserDefaults] setObject: loginToken forKey: LoginToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString*) userId
{
    return [[NSUserDefaults standardUserDefaults] objectForKey: UserId];
}

+ (void) setUserId: (NSString*) userId
{
    [[NSUserDefaults standardUserDefaults] setObject: userId forKey: UserId];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


+ (NSString*) userEmail
{
    return [[NSUserDefaults standardUserDefaults] objectForKey: UserEmail];
}

+ (void) setUserEmail: (NSString*) userEmail
{
    [[NSUserDefaults standardUserDefaults] setObject: userEmail forKey: UserEmail];
    [[NSUserDefaults standardUserDefaults] synchronize];
}



@end
