//
//  CNCApi.h
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CNCQuote.h"

@class CNCCase;

typedef NS_ENUM(NSInteger, AppSizeGroup) {
    AppSizeSmall,
    AppSizeBig,
    AppSizeRep,
    AppSizeUnknown
};


@interface CNCApi : NSObject

@property (nonatomic, strong) NSDateFormatter* dateFormatter;


+ (instancetype)sharedClient;


- (void) loginWithEmail: (NSString*) email
			   password: (NSString*) password
		   successBlock:(void (^)(NSString* userId, NSString* authToken, NSError *error, NSString* message))success;


- (void) createQuote:(CNCQuote*)quote withSuccess:(void (^)(CNCQuote *quote, NSString* submissionText, BOOL retailTimer, NSError* error, BOOL redirect, NSString*message))success;

//- (void) uploadImage: (UIImage*) image
//              toCase: (CNCCase*) caseQuote
//         withSuccess: (void (^)(CNCCase *caseData, NSError *error))success;

- (void) uploadDriversLicenseImage: (UIImage*) image
                          toCaseID: (NSString*) caseID
                        forStoreID: (NSString*) storeID
                       withSuccess: (void (^)(NSError *error))success;

- (void) uploadSignatureImage: (UIImage*) image
                     toCaseID: (NSString*) caseID
                     forStoreID: (NSString*) storeID
                  withSuccess: (void (^)(NSError *error))success;


- (void) upateAppInfoWithCompletionBlock: (void (^)(NSError *error, BOOL redirect)) success;


- (void) pendingWatches: (NSString*) userId
		   successBlock: (void (^)(NSArray* watches, NSError *error, BOOL redirect)) success;

- (void) quotedWatches: (NSString*) userId
		  successBlock: (void (^)(NSArray* watches, NSError *error, BOOL redirect)) success;

- (void) watchesToShip: (NSString*) userId
          successBlock: (void (^)(NSArray* watches, NSError *error, BOOL redirect)) success;

- (void) historyInfo: (NSString*) userId
          successBlock: (void (^)(NSArray* watches, NSError *error, BOOL redirect)) success;

- (void) watchCaseInfo: (NSString*) caseId
		  successBlock: (void (^)(CNCCase* aCase, NSError *error, BOOL redirect)) success;

- (void) acceptCase: (NSString*) caseId
       successBlock: (void (^)(NSString* thankYou, NSError *error, BOOL redirect)) success;

- (void) rejectCase: (NSString*) caseId
      declineReason: (NSString*) declineReason
	   successBlock: (void (^)(NSError *error, BOOL redirect)) success;

//- (void) undecideCase: (NSString*) caseId
//         successBlock: (void (^)(NSError *error, BOOL redirect)) success;

- (void) shipWatch: (NSString*) caseId
      successBlock: (void (^)(NSError *error, BOOL redirect)) success;

@end
