//
//  CNCWatch.h
//  Crown&Caliber
//

#import <Foundation/Foundation.h>

@interface CNCWatch: NSObject

@property (nonatomic, strong) NSString* brand;
@property (nonatomic, strong) NSDate* createdAt;
@property (nonatomic, strong) NSDate* acceptedAt;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* caseId;
@property (nonatomic, strong) NSString* imageURL;
@property (nonatomic, strong) NSString* modelName;
@property (nonatomic, strong) NSString* modelNumber;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* phone;
@property (nonatomic, strong) NSString* status;
@property (nonatomic, strong) NSString* cashPrice;


@end
