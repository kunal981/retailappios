//
//  CNCQuote.h
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CNCQuoteProcessor.h"

static NSString* const UserInfoKey = @"userInfo";
static NSString* const UserNameKey = @"userName";
static NSString* const UserEmailKey = @"userEmail";
static NSString* const UserPhoneNumberKey = @"userPhoneNumber";
static NSString* const QuoteCaseIdKey = @"quoteCaseId";
static NSString* const QuoteBrandKey = @"quoteBrand";
static NSString* const QuoteStoreNameKey = @"quoteStoreName";
static NSString* const QuoteRetailerIdKey = @"quoteRetailerId";

@interface CNCQuote : NSObject

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* phone;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* brand;
@property (nonatomic, strong) NSString* modelName;
@property (nonatomic, strong) NSString* modelNumber;
@property (nonatomic, strong) NSString* documents;
@property (nonatomic, strong) NSString* extraInfo;
@property (nonatomic, strong) NSString* retailerName;
@property (nonatomic, strong) NSString* retailerAssociateName;

@property (nonatomic, strong) NSString* storeName;
@property (nonatomic, assign) int storeId;
@property (nonatomic, strong) NSString* retailerId;
@property (nonatomic, strong) NSNumber* salesAssociateId;

@property (nonatomic, strong) NSString* caseId;
@property (nonatomic, strong) NSString* sessionId;
@property (nonatomic, strong) NSString* imagesPostURLString;

@property (nonatomic, strong) UIImage* imageFront;
@property (nonatomic, strong) UIImage* imageBack;
@property (nonatomic, strong) UIImage* driverLicenseImage;
@property (nonatomic, strong) UIImage* signatureImage;

@property (nonatomic, strong) NSDate* expiresDate;
@property (nonatomic, copy) NSString* status;

@property (nonatomic) BOOL isOpenCase;
@property (nonatomic) BOOL isDomestic;
@property (nonatomic) BOOL hasModelNumber;
@property (nonatomic) BOOL hasRetailer;
@property (nonatomic) BOOL isTermsSelected;
@property (nonatomic) BOOL isFullQuote;
@property (nonatomic, assign) BOOL hasTimer;
@property (nonatomic, assign) BOOL shipped;
@property (nonatomic, assign) BOOL offerReceived;
@property (nonatomic, assign) BOOL isTradeIn;
@property (nonatomic, assign) BOOL isExpedited;

@property (nonatomic, strong) NSNumber* cashPrice;
@property (nonatomic, strong) NSDate* createdAt;
@property (nonatomic, strong) NSDate* acceptedAt;
@property (nonatomic, strong) NSDate* receivedAt;
@property (nonatomic, strong) NSDate* shippedAt;
@property (nonatomic, strong) NSString* imageURL;
@property (nonatomic, strong) NSString* image2URL;
@property (nonatomic, strong) NSString* shippingLabel;


@property (nonatomic, strong) NSString* offerText;
@property (nonatomic, strong) NSString* timerText;
@property (nonatomic, strong) NSString* notes;



// set mamin info: name, email, phone, brand, caseId
//
- (void) setMainInfo: (NSDictionary*) mainInfo;

// details include mamin info
//
- (NSMutableDictionary*) details;

// save only quote mamin info on disk
//
- (void) saveQuoteToStorage;
- (void) removeQuoteFromStorage;

@end
