//
//  CNCNewQuoteController.h
//  Crown&Caliber
//
//  Created by Admin on 10.07.14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNCMenuViewController.h"
#import "CNCBaseViewController.h"

@interface CNCNewQuoteController : CNCBaseViewController

@property (nonatomic, strong) id<CNCMenuDelegate> menuDelegate;

-(void) resetUserData;

@end
