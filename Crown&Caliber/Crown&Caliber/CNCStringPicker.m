//
//  CNCStringPicker.m
//  Crown&Caliber
//

#import "CNCStringPicker.h"

static const CGFloat PreferredHeight = 480.0f;

@interface CNCStringPicker() <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,strong) NSArray *data;
@property (nonatomic,assign) NSInteger selectedIndex;
@end

@implementation CNCStringPicker

+ (id)showPickerWithTitle:(NSString *)title rows:(NSArray *)strings initialSelection:(NSInteger)index doneBlock:(CNCStringPickerDoneBlock)doneBlock cancelBlock:(CNCStringPickerCancelBlock)cancelBlockOrNil origin:(id)origin {
    CNCStringPicker * picker = [[CNCStringPicker alloc] initWithTitle:title rows:strings initialSelection:index doneBlock:doneBlock cancelBlock:cancelBlockOrNil origin:origin];
    [picker showActionSheetPicker];
    return picker;
}

- (id)initWithTitle:(NSString *)title rows:(NSArray *)strings initialSelection:(NSInteger)index doneBlock:(CNCStringPickerDoneBlock)doneBlock cancelBlock:(CNCStringPickerCancelBlock)cancelBlockOrNil origin:(id)origin {
    self = [self initWithTitle:title rows:strings initialSelection:index target:nil successAction:nil cancelAction:nil origin:origin];
    if (self) {
        self.onCNCStringPickerDone = doneBlock;
        self.onCNCStringPickerCancel = cancelBlockOrNil;
    }
    return self;
}

+ (id)showPickerWithTitle:(NSString *)title rows:(NSArray *)data initialSelection:(NSInteger)index target:(id)target successAction:(SEL)successAction cancelAction:(SEL)cancelActionOrNil origin:(id)origin {
    CNCStringPicker *picker = [[CNCStringPicker alloc] initWithTitle:title rows:data initialSelection:index target:target successAction:successAction cancelAction:cancelActionOrNil origin:origin];
    [picker showActionSheetPicker];
    return picker;
}

- (id)initWithTitle:(NSString *)title rows:(NSArray *)data initialSelection:(NSInteger)index target:(id)target successAction:(SEL)successAction cancelAction:(SEL)cancelActionOrNil origin:(id)origin {
    self = [self initWithTarget:target successAction:successAction cancelAction:cancelActionOrNil origin:origin];
    if (self) {
        self.data = data;
        self.selectedIndex = index;
        self.title = title;
    }
    return self;
}


- (UIView *)configuredPickerView {
    if (!self.data)
        return nil;

    UITableView* dataTableView = [[UITableView alloc] initWithFrame: CGRectMake(0.0f, 0.0f, self.viewSize.width, PreferredHeight)
                                                      style: UITableViewStylePlain];
    dataTableView.dataSource = self;
    dataTableView.delegate = self;
    self.pickerView = dataTableView;

    return dataTableView;
}

- (void)notifyTarget:(id)target didSucceedWithAction:(SEL)successAction origin:(id)origin {
    if (self.onCNCStringPickerDone) {
        _onCNCStringPickerDone(self, self.selectedIndex, [self.data objectAtIndex:self.selectedIndex]);
        return;
    }
    else if (target && [target respondsToSelector:successAction]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [target performSelector:successAction withObject:@(self.selectedIndex) withObject:origin];
#pragma clang diagnostic pop
        return;
    }
    NSLog(@"Invalid target/action ( %s / %s ) combination used for ActionSheetPicker", object_getClassName(target), sel_getName(successAction));
}

- (void)notifyTarget:(id)target didCancelWithAction:(SEL)cancelAction origin:(id)origin {
    if (self.onCNCStringPickerCancel) {
        _onCNCStringPickerCancel(self);
        return;
    }
    else if (target && cancelAction && [target respondsToSelector:cancelAction]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [target performSelector:cancelAction withObject:origin];
#pragma clang diagnostic pop
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger) tableView: (UITableView*) tableView numberOfRowsInSection: (NSInteger) section
{
    return self.data.count;
}

- (UITableViewCell*) tableView: (UITableView*) tableView cellForRowAtIndexPath: (NSIndexPath*) indexPath
{
    static NSString* CellId = @"CellId";

    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: CellId];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: CellId];
    }

    cell.textLabel.text = self.data[indexPath.row];
    cell.accessoryType = (indexPath.row == self.selectedIndex) ?
        UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;

    return cell;
}


#pragma mark - UITableViewDelegate

- (void) tableView: (UITableView*) tableView didSelectRowAtIndexPath: (NSIndexPath*) indexPath
{
    [tableView deselectRowAtIndexPath: indexPath animated: YES];

    self.selectedIndex = indexPath.row;

    if (self.onCNCStringPickerDone)
    {
        _onCNCStringPickerDone(self, self.selectedIndex, [self.data objectAtIndex:self.selectedIndex]);
    }

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    if ([self respondsToSelector: @selector(dismissPicker)])
    {
        [self performSelector: @selector(dismissPicker) withObject: nil afterDelay: 0.1];
    }
#pragma clang diagnostic pop
}

@end
