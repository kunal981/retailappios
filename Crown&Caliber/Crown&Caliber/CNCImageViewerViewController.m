//
//  CNCImageViewerViewController.m
//  Crown&Caliber
//

#import "CNCImageViewerViewController.h"
#import "UIImageView+AFNetworking.h"


@interface CNCImageViewerViewController ()

@end

@implementation CNCImageViewerViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.closeButton.layer.cornerRadius = 10;
    self.closeButton.layer.borderWidth = 1;
    self.closeButton.layer.borderColor = (__bridge CGColorRef)self.view.tintColor;
    
   }

- (BOOL)prefersStatusBarHidden {
    return YES;
}

-(void)viewWillAppear:(BOOL)animated {
    NSURL* watchURL = [NSURL URLWithString:self.imageUrlString];
    [self.imageView setImageWithURL:watchURL];
    
}

- (void)setImageUrl:(NSString *)imageUrlString {
    self.imageUrlString = imageUrlString;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Private methods

- (IBAction)handleClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end

