//
//  CNCHistoryInfoViewController.h
//  Crown&Caliber
//

#import <UIKit/UIKit.h>
#import "CNCMenuViewController.h"
#import "CNCBaseViewController.h"

@interface CNCHistoryInfoViewController: CNCBaseViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) id<CNCMenuDelegate> menuDelegate;
@property (nonatomic, strong) IBOutlet UITableView* historyInfoTableView;
@property (nonatomic, strong) IBOutlet UIView* footerView;

@end
