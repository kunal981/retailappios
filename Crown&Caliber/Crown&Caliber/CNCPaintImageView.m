//
//  CNCPaintImageView.m
//  Crown&Caliber
//
//  Created by Valeriy on 9/26/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCPaintImageView.h"

@interface CNCPaintImageView ()
{
	UIImage *mainImg;
	UIImageView *mainImageView;
	
	BOOL _isErasing;
	BOOL activate;
	CGPoint lastPoint;
	CGFloat red ;
	CGFloat green;
	CGFloat blue ;
	CGFloat brush;
	CGFloat opacity;
	BOOL mouseSwiped;
}

@end

@implementation CNCPaintImageView



- (id) initWithFrame: (CGRect) frame
{
    self = [super initWithFrame: frame];
    if (self)
	{
        // Initialization code
		
		_isErasing = NO;
		activate = NO;
		red = 0.0/255.0;
		green = 0.0/255.0;
		blue = 0.0/255.0;
		brush = 5.0;
		opacity = 1.0;
    }
    return self;
}


- (void) startDrawing
{
    activate = YES;
    self.userInteractionEnabled = YES;
    mainImg = self.image;
    mainImageView = [[UIImageView alloc] initWithFrame: self.bounds];
    [self addSubview:mainImageView];
	lastPoint = CGPointMake(-1, -1);
}

- (void) stopDrawing
{
    activate = NO;
}

-(void) resetImage
{
	_isErasing = NO;
    [mainImageView removeFromSuperview];
    mainImageView = nil;
    mainImageView = [[UIImageView alloc] initWithFrame: self.bounds];
    [self addSubview: mainImageView];
    lastPoint = CGPointMake(-1, -1);
}

-(void)setBrush:(CGFloat) b{
    
    brush = b;
    
}

-(void) setColor:(UIColor *) color{
    
	_isErasing = NO;
    [color getRed:&red green:&green blue:&blue alpha:&opacity];
}

- (UIImage*)imageByCombiningImage:(UIImage*)firstImage withImage:(UIImage*)secondImage {
    UIImage *image = nil;
    
    CGSize newImageSize = CGSizeMake(MAX(firstImage.size.width, secondImage.size.width), MAX(firstImage.size.height, secondImage.size.height));
    if (UIGraphicsBeginImageContextWithOptions != NULL) {
        UIGraphicsBeginImageContextWithOptions(newImageSize, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(newImageSize);
    }
    [firstImage drawAtPoint:CGPointMake(roundf((newImageSize.width-firstImage.size.width)/2),
                                        roundf((newImageSize.height-firstImage.size.height)/2))];
    [secondImage drawAtPoint:CGPointMake(roundf((newImageSize.width-secondImage.size.width)/2),
                                         roundf((newImageSize.height-secondImage.size.height)/2))];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


-(void) save
{
    UIImage *imageToSave = [self drawedImage];
    UIImageWriteToSavedPhotosAlbum(imageToSave, self,@selector(image:didFinishSavingWithError:contextInfo:), nil);
}

- (UIImage*) drawedImage
{
	if (CGPointEqualToPoint(lastPoint, CGPointMake(-1, -1)))
	{
		return nil;
	}
	
	UIImage *imageToSave = [self imageByCombiningImage: mainImg withImage: mainImageView.image];
    
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO,0.0);
    [imageToSave drawInRect:CGRectMake(0, 0, mainImageView.frame.size.width, mainImageView.frame.size.height)];
    UIImage *SaveImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
	return SaveImage;
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    
    if (error != NULL)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Image could not be saved.Please try again"  delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Close", nil];
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Image was successfully saved in photoalbum"  delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Close", nil];
        [alert show];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
    if(activate){
        mouseSwiped = NO;
        UITouch *touch = [touches anyObject];
        lastPoint = [touch locationInView:self];
        if ([self.delegate respondsToSelector:@selector(paintImageViewDidBegin)]) {
            [self.delegate paintImageViewDidBegin];
        }
    }
    
}

-(void)selectRubber{
    
    _isErasing = YES;
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if(activate){
        mouseSwiped = YES;
        UITouch *touch = [touches anyObject];
        CGPoint currentPoint = [touch locationInView:self];
        
        UIGraphicsBeginImageContext(self.frame.size);
        [mainImageView.image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
        CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
        CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
        CGContextSetLineWidth(UIGraphicsGetCurrentContext(), brush );
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), red, green, blue, 1.0);
        CGContextSetBlendMode(UIGraphicsGetCurrentContext(),kCGBlendModeNormal);
        
        if (_isErasing) {
            CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeClear);
        }
        else {
            CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), red, green, blue, 1.0);
            CGContextSetBlendMode(UIGraphicsGetCurrentContext(),kCGBlendModeNormal);
        }
        
        CGContextStrokePath(UIGraphicsGetCurrentContext());
        mainImageView.image = UIGraphicsGetImageFromCurrentImageContext();
        [mainImageView setAlpha:opacity];
        UIGraphicsEndImageContext();
        
        lastPoint = currentPoint;

        [self.delegate paintImageViewDidChange: self];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if(activate){
        if(!mouseSwiped) {
            UIGraphicsBeginImageContext(self.frame.size);
            [mainImageView.image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
            CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
            CGContextSetLineWidth(UIGraphicsGetCurrentContext(), brush);
            CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), red, green, blue, opacity);
            CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
            CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
            CGContextStrokePath(UIGraphicsGetCurrentContext());
            CGContextFlush(UIGraphicsGetCurrentContext());
            mainImageView.image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();

            [self.delegate paintImageViewDidChange: self];
        }
        if ([self.delegate respondsToSelector:@selector(paintImageViewDidEnd)]) {
            [self.delegate paintImageViewDidEnd];
        }

    }
}

@end
