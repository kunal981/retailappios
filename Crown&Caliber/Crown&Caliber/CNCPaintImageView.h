//
//  CNCPaintImageView.h
//  Crown&Caliber
//
//  Created by Valeriy on 9/26/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CNCPaintImageView;

@protocol CNCPaintImageViewDelegate <NSObject>

- (void) paintImageViewDidChange: (CNCPaintImageView*) paintImageView;
@optional
- (void) paintImageViewDidBegin;
- (void) paintImageViewDidEnd;

@end

@interface CNCPaintImageView : UIImageView

@property (nonatomic, weak) id<CNCPaintImageViewDelegate> delegate;

- (void)startDrawing;
-(void)stopDrawing;
-(void)resetImage;
-(void)setBrush:(CGFloat) b;
-(void) setColor:(UIColor *) color;
- (UIImage*)imageByCombiningImage:(UIImage*)firstImage withImage:(UIImage*)secondImage;
- (UIImage*) drawedImage;

-(void) save;
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
-(void)selectRubber;


@end
