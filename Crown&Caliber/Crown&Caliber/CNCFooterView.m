//
//  CNCFooterView.m
//  Crown&Caliber
//

#import "CNCFooterView.h"
#import "CNCAppOptions.h"

const CGFloat StaticFooterHeight = 94.0f;

@interface CNCFooterView()

@property (nonatomic, strong) UIView* containerView;

@end

@implementation CNCFooterView

- (id) initWithFrame: (CGRect) frame
{
    self = [super initWithFrame: frame];
    
    if (self)
    {
        [self doInit];
    }
    
    return self;
}

- (id) initWithCoder: (NSCoder*) aDecoder
{
    self = [super initWithCoder: aDecoder];
    
    if (self)
    {
        [self doInit];
    }
    
    return self;
}


- (void) doInit
{
    self.backgroundColor = [UIColor whiteColor];

    _containerView = [[UIView alloc] initWithFrame: CGRectZero];
    _containerView.backgroundColor = [UIColor colorWithWhite: 220.0f / 255.0f alpha: 1.0f];
    [self addSubview: _containerView];
    
    UILabel* titleLabel = [[UILabel alloc] initWithFrame: CGRectMake(8.0f, 8.0f, 0.0f, 0.0f)];
    titleLabel.text = @"Contact Crown & Caliber";
    titleLabel.font = [UIFont boldSystemFontOfSize: 17.0f];
    [titleLabel sizeToFit];
    [_containerView addSubview: titleLabel];
    
    
    NSString* phoneMailString;
    if ([CNCAppOptions sharedInstance].appSize == AppSizeRep)
    {
        phoneMailString = @"Phone: (404) 812-9928";
    } else {
        phoneMailString = @"Phone: (800) 514-3750       |       Email: bnollner@crownandcaliber.com";
    }

    NSMutableAttributedString* phoneMailAttr = [[NSMutableAttributedString alloc] initWithString: phoneMailString];
    [phoneMailAttr addAttribute: NSFontAttributeName
                          value: [UIFont systemFontOfSize: 17.0f]
                          range: NSMakeRange(0, phoneMailString.length)];

    NSRange phoneRange = [phoneMailString rangeOfString: @"Phone:"];
    if (phoneRange.location != NSNotFound)
    {
        [phoneMailAttr addAttribute: NSFontAttributeName
                              value: [UIFont boldSystemFontOfSize: 17.0f]
                              range: phoneRange];
    }
    
    NSRange emailRange = [phoneMailString rangeOfString: @"Email:"];
    if (emailRange.location != NSNotFound)
    {
        [phoneMailAttr addAttribute: NSFontAttributeName
                              value: [UIFont boldSystemFontOfSize: 17.0f]
                              range: emailRange];
    }
    
    UILabel* phoneMailLabel = [[UILabel alloc] initWithFrame: CGRectMake(8.0f, 49.0f, 0.0f, 0.0f)];
    phoneMailLabel.attributedText = phoneMailAttr;
    [phoneMailLabel sizeToFit];
    [_containerView addSubview: phoneMailLabel];
    
    
    
    NSString* webString = @"Website: www.crownandcaliber.com";
    NSMutableAttributedString* webAttr = [[NSMutableAttributedString alloc] initWithString: webString];
    [webAttr addAttribute: NSFontAttributeName
                    value: [UIFont systemFontOfSize: 17.0f]
                    range: NSMakeRange(0, webString.length)];
    
    NSRange webRange = [webString rangeOfString: @"Website:"];
    if (webRange.location != NSNotFound)
    {
        [webAttr addAttribute: NSFontAttributeName
                        value: [UIFont boldSystemFontOfSize: 17.0f]
                        range: webRange];
    }
    
    UILabel* webLabel = [[UILabel alloc] initWithFrame: CGRectMake(8.0f, 69.0f, 0.0f, 0.0f)];
    webLabel.attributedText = webAttr;
    [webLabel sizeToFit];
    [_containerView addSubview: webLabel];
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    _containerView.frame = CGRectMake(0.0f, CGRectGetHeight(self.frame) - StaticFooterHeight, CGRectGetWidth(self.frame), StaticFooterHeight);
}

@end
