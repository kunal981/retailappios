//
//  CNCPendingQuoteTVC.h
//  Crown&Caliber
//
//  Created by Valeriy on 9/23/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CNCTimerLabel;

@interface CNCPendingQuoteTVC : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel* nameLabel;
@property (weak, nonatomic) IBOutlet UILabel* associateNameLabel;
@property (weak, nonatomic) IBOutlet UILabel* brandLabel;
@property (weak, nonatomic) IBOutlet UILabel* phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel* caseIdLabel;

@property (weak, nonatomic) IBOutlet UILabel* createdDateLabel;
@property (weak, nonatomic) IBOutlet UILabel* receivedDateLabel;
@property (weak, nonatomic) IBOutlet UILabel* expiresDateLabel;
@property (weak, nonatomic) IBOutlet UILabel* offerExpectedInLabel;

@property (weak, nonatomic) IBOutlet UIImageView* watchPhoto;
@property (weak, nonatomic) IBOutlet UIButton* watchPhotoButton;
@property (weak, nonatomic) IBOutlet UIImageView* watchPhoto2;
@property (weak, nonatomic) IBOutlet UIButton* watchPhoto2Button;


@property (weak, nonatomic) IBOutlet UILabel* statusLabel;
@property (weak, nonatomic) IBOutlet CNCTimerLabel* timerLabel;
@property (weak, nonatomic) IBOutlet UIButton* timerTextButton;

@end
