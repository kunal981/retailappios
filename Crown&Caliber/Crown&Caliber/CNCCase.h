//
//  CNCCase.h
//  Crown&Caliber
//
//  Created by Oleg Lavrentyev on 12/1/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CNCCase : NSObject

@property (nonatomic, strong) NSString* brand;
@property (nonatomic, strong) NSString* caseId;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* imageURL;
@property (nonatomic, strong) NSString* modelName;
@property (nonatomic, strong) NSString* modelNumber;
@property (nonatomic, strong) NSString* offerText;
@property (nonatomic, strong) NSString* phone;
@property (nonatomic, strong) NSString* seller;
@property (nonatomic, strong) NSString* notes;
@property (nonatomic) BOOL signatureOnFile;
@property (nonatomic) BOOL driversLicenseOnFile;
@property (nonatomic, strong) NSArray* watchData;
@property (nonatomic, copy) NSString* quoteRequest;

@end
